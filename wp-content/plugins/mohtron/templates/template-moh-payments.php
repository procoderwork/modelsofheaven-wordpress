<?php
/**
 * Template Name: MOH Payments
 */

?>
<?php
  get_header();
?>


<?php 
	$bname = get_query_var('bname');
    $pid = get_query_var('packageid'); 
  
    $bloggerObj = get_user_by( 'slug', $bname );
    $package = mohtron_get_packages($pid);
    $paymentsData = get_option('mohtron_payments');
    $currency = get_option('mohtron_settings')['mohtron_currency'];

    

    switch( $currency  ){
        case html_entity_decode('kr'):
            $currency_code = 'DKK';
            break;
            
        case html_entity_decode('&#36;'):
            $currency_code = 'USD';
            break;

        case html_entity_decode('&euro;'):
            $currency_code = 'EUR';
            break;

        case html_entity_decode('&pound;'):
            $currency_code = 'GBP';
            break;
    }
?>

<?php //show if user is logged in and subscription status is not active
    if( is_user_logged_in() && checkUserSubscriptionStatus( $bloggerObj->ID , get_current_user_id()) != "active" && isset($_SERVER['HTTP_REFERER'] )){
?>	

<section class="container-wrap main-color">
	<div id="main-container" class="container">
		<div class="row">
                <?php 
                    echo "<div class='col-md-12 subscribe'><div class='checkout-pckg'>";
                    echo "<h3>" . __('Selected package details',MOHTRON_TEXT) ."</h3>";
                    echo '<h4>'.$package['package_name'] ."</h4>";
                    echo '<span class="pckg-desc-text">'.$package['package_description'] .'</span>';
                    echo '<span class="pckf-price">' . $currency . $package['billing_details'] . '</span>';
                    echo "<span class='exp-text'>" . __('Expires in',MOHTRON_TEXT) ." :  ".$package['expiration'] .  __('days',MOHTRON_TEXT) ."</span>";
                    echo "<div id='paypal-button'></div></div>";
                    echo "</div>";
                ?>
            <div class="ajax-loader" style="display:none;"></div>
            <div id="paypal-button"></div>
		</div><!--end .row-->		
	</div><!--end .container-->  
</section>


<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>

  jQuery(()=>{

        <?php if( 'sandbox' == $paymentsData['environment']){ ?>

                var cid = {sandbox: '<?php echo $paymentsData['sandbox_client_id']; ?>'};
                var mohtronenv = "<?php echo $paymentsData['environment'];?>";

        <?php  } ?>

        <?php if( 'production' == $paymentsData['environment']){ ?>

                var cid = {production: '<?php echo $paymentsData['production_client_id']; ?>' };
                var mohtronenv = "<?php echo $paymentsData['environment'];?>";

        <?php  } ?>      
        
        paypal.Button.render({
            // Configure environment
            env: mohtronenv, // sandbox | production
            // Show the buyer a 'Pay Now' button in the checkout flow 
            commit: true,
            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: cid,
            // Customize button (optional)
            // locale: 'en_US',
            style: {
              size: 'medium',
              color: 'black',
              shape: 'pill',
            },

            validate: function(actions){
               //check if user is not subscribed          
              jQuery.ajax({
                            method: "POST",
                            url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                            data: { 'action': 'chksub', 'bname': '<?php echo $bname;  ?>', 'packageid': '<?php echo $pid;  ?>'  },
                        })
                        .done(function( data ) {
                          
                          var res = JSON.parse(data);
                          if(!res.result){                           
                              alert(res.msg);
                              actions.disable();
                              window.location.href = '/';
                              return false;
                          }                           
                        })
                        .fail(function(data) {
                          console.log( data );
                        })
            },
            onClick: function() {
               
            },    
            //  payment() is called when the button is clicked
            payment: function (data, actions) {

               // Make a call to the REST api to create the payment
               return actions.payment.create({
                              transactions: [{
                                amount: {
                                  total: '50.00',
                                  currency: '<?php echo $currency_code;?>'
                                }
                              }],
                              redirect_urls: {
                                  return_url: 'https://moh.2311563.xyz/',
                                  cancel_url: 'https://moh.2311563.xyz/'
                              }
                    });
              
            },
            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function (data, actions) {     

                // Get the payment details
                return actions.payment.get()
                .then(function (paymentDetails) {
                
                      // Make a call to the REST api to execute the payment
                      return actions.payment.execute()
                        .then(function (res) {

                          // console.log(res); 

                          // return false;   
                        
                          jQuery('#paypal-button').hide();
                          jQuery('.ajax-loader').show();      
                                              
                          jQuery.ajax({
                              method: "POST",
                              url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                              data: { 'action': 'add_sub', 'data': res, 'bname': '<?php echo $bname;  ?>', 'packageid': '<?php echo $pid;  ?>'  },
                          })
                          .done(function( data ) {
                              var data =JSON.parse(data);
                              // console.log(data);
                              if(data.success){
                                  
                                  // alert(data.msg);
                                  if(data.redirect){
                                    
                                      window.location.href = data.redirect;
                                  }
                              }else{
                                  alert(data.msg);
                                  if(data.redirect){
                                      // window.location.href = data.redirect;
                                  }
                                  jQuery('#paypal-button').show();
                                  jQuery('.ajax-loader').hide(); 
                                  return false;
                              }
                              
                            
                          });
                        
                          // actions.redirect();              
                      }); 
              })
            },
            onCancel: function(data, actions) {
              // actions.redirect();
            },
            onError: function (err) {
              // Show an error page here, when an error occurs

              console.log(err);
            }
      }, '#paypal-button');

  })

</script>

<?php  } else{ ?>    
   
  <div class="container">
        <div class="row registration-custom">
            <?php if(!is_user_logged_in()){ ?>
                <div class="alert "><p><?php echo __( 'Please', MOHTRON_TEXT ); ?> <a href="/moh-login"><?php echo __( 'Login', MOHTRON_TEXT ); ?></a> <?php echo __( 'or', MOHTRON_TEXT ); ?> <a href="/moh-register"><?php echo __( 'Register', MOHTRON_TEXT ); ?></a> <?php echo __( 'to subscribe to an angel !', MOHTRON_TEXT ); ?></p></div>
            <?php } else {?>
                <div class="alert "><p>
					<?php echo __( 'Please first select an angel from', MOHTRON_TEXT ); ?><a href="/angels"> <?php echo __( 'Angels', MOHTRON_TEXT ); ?> </a> <?php echo __( 'page to subscribe.', MOHTRON_TEXT ); ?></p>
				</div>
            <?php }?>
        </div>
    </div>


<?php } ?>

<?php
  get_footer();
?>
