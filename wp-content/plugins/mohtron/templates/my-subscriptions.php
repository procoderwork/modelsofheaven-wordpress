<?php

$mohtron_crud = new Mohtron_CRUD;

$userid = get_current_user_id();

$usersActiveSubs = $mohtron_crud->mohtron_get_user_subscriptions( $userid ) ;

// pr( $userid );
// pr($usersActiveSubs);   
?>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <table class="responsive-table table">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?php echo __('Subscribed To',MOHTRON_TEXT); ?></th>                    
                    <th><?php echo __('Package name',MOHTRON_TEXT); ?></th>
                    <th><?php echo __('Time of Subscription',MOHTRON_TEXT); ?></th>
                    <th><?php echo __('Expires on',MOHTRON_TEXT); ?></th>
                </tr> 
                </thead>
                <tbody>
                <?php
                    $count = 1;                    
                    foreach($usersActiveSubs as $key=>$data){
                       
                        $blogger = get_user_by('id',$data['blogger_id']);

                        $package = mohtron_get_packages($data['product_id']);
                ?>
                    <tr>
                        <td>(<?php echo $count;?>)</td>
                        <td><a href="/members/<?php echo  $blogger->data->user_login; ?>"><?php echo $blogger->data->display_name;?></a></td>
                        <td><?php echo $package['package_name'];?></td>
                        <td><?php echo $data['starts_on'];?></td>
                        <td><?php echo $data['ends_on'];?></td>                     
                    </tr>

                    <?php $count++;} ?>

                </tbody>
            </table>
        </div>
    </div>   
   
</div>