<?php
/**
 * Template Name: MOH Registration
 */

?>

<?php  get_header(); ?>

<!--chek if user is logged in or landed directly tothis page -->
<?php  if ( !isset($_SERVER['HTTP_REFERER'] ) ||  is_user_logged_in() ){  ?> 
   
   <div class="container">
        <div class="row registration-custom">
            <?php if(!is_user_logged_in()){ ?>
                <div class="alert "><p><?php echo __( 'Please', MOHTRON_TEXT ); ?> <a href="/moh-login"><?php echo __( 'Login', MOHTRON_TEXT ); ?></a> <?php echo __( 'or', MOHTRON_TEXT ); ?> <a href="/moh-register"><?php echo __( 'Register', MOHTRON_TEXT ); ?></a> <?php echo __( 'to subscribe to an angel !', MOHTRON_TEXT ); ?></p></div>
            <?php } else {?>
                <div class="alert "><p>
					<?php echo __( 'Please first select an angel from', MOHTRON_TEXT ); ?><a href="/angels"> <?php echo __( 'Angels', MOHTRON_TEXT ); ?> </a> <?php echo __( 'page to subscribe.', MOHTRON_TEXT ); ?></p>
				</div>
            <?php }?>
        </div>
    </div>



<?php    
    }else{

        $bname = get_query_var('bname');
        $pid = get_query_var('packageid');

?>

<div class="container">
    <div class="row padd-row">

     <div class="alert alert-danger" style="display:none;"  id="error-msg" role="alert">            
    </div>
    
    <div class="col-md-6 col-sm-6 column-form column-login">
        <h3><?php echo __('Login if you are a registered member.',MOHTRON_TEXT); ?></h3>       

        <form name="loginform" id="loginform" action="#" method="post">
            <!-- <input type="hidden" name="bid" value="<?php //echo $_POST['pid'];?>">
            <input type="hidden" name="pid" value="<?php //echo $_POST['bid'];?>"> -->

			<p class="login-username">
				<label for="user_login"><?php echo __('Username or Email Address',MOHTRON_TEXT); ?></label>
				<input type="text" name="log" id="user_login" class="input" value="" size="20">
			</p>
			<p class="login-password">
				<label for="user_pass"><?php echo __('Password',MOHTRON_TEXT); ?></label>
				<input type="password" name="pwd" id="user_pass" class="input" value="" size="20">
			</p>
			
			<p class="login-remember"><label><input name="remember" type="checkbox" id="remember" value="forever"> <?php echo __('Remember Me',MOHTRON_TEXT); ?></label></p>
			<p class="login-submit">
				<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In">
				<input type="hidden" name="redirect_to" value="https://moh.2311563.xyz/moh-registration/">
			</p>
			
		</form>
       
    </div>
    
   <div class="col-md-6 col-sm-6 column-form column-login-right">
    <h3><?php echo __('Register with Models of Heaven in order to purchase a subscription.',MOHTRON_TEXT); ?></h3>
        <form method="POST" id="register-visitor" action="#">
            
            <div class="form-group">
                <label for="fname"><?php echo __('First name',MOHTRON_TEXT); ?> *</label>
                <input type="text" name="fname" class="form-control" id="fname" aria-describedby="fname" placeholder="<?php echo __('Enter First name',MOHTRON_TEXT ); ?>">            
            </div>
            <div class="form-group">
                <label for="fname"><?php echo __('Last name',MOHTRON_TEXT); ?></label>
                <input type="text" name="lname" class="form-control" id="lname" aria-describedby="lname" placeholder="<?php echo __('Enter Last name',MOHTRON_TEXT ); ?>">            
            </div>
        
            <div class="form-group">
                <label for="email"><?php echo __('Email address',MOHTRON_TEXT); ?> *</label>
                <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="<?php echo __('Enter email',MOHTRON_TEXT ); ?>">
                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
            </div>
            <div class="form-group">
                <label for="password1"><?php echo __('Password',MOHTRON_TEXT); ?> *</label>
                <input type="password" name="password1" class="form-control" id="password1" placeholder="<?php echo __('Enter Password',MOHTRON_TEXT ); ?>">
            </div>

            <div class="form-group">
                <label for="pssword2"><?php echo __('Retype-Password',MOHTRON_TEXT); ?></label>
                <input type="password" name="password2" class="form-control" id="password2" placeholder="<?php echo __('Retype password',MOHTRON_TEXT ); ?>">
            </div>

            <div class="form-check">
                <input type="checkbox" name="tnc" class="form-check-input" id="tnc">
                <label class="form-check-label" for="tnc"><?php echo __('I Agree to T&C',MOHTRON_TEXT); ?></label>
            </div>
            <input type="submit" class="btn btn-primary">
        </form>
    </div>
    </div>
</div>

<script>
jQuery(document).ready( () => {

       
    //registration form submit
    jQuery('#register-visitor').on('submit',(e)=>{
        e.preventDefault();
     
        var formData = jQuery('#register-visitor').serialize();  
        // var formData = JSON.parse(JSON.stringify( ( jQuery('#register-visitor').serializeArray() ) ) );  
        
        if( ("<?php echo $pid;?>" == "" ) && ("<?php echo $bname;?>" == "") ){

            alert( "Please select an angel and a package first!");
            return false;

        }else{

            jQuery.ajax({
                method: "POST",
                url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                data: { 'action': 'reg_visi', 'data': formData , 'bname': '<?php echo $bname;  ?>', 'packageid': '<?php echo $pid;  ?>' },
            })
            .done(function( data ) {
                var data =JSON.parse(data);
                // console.log(data);
                if(data.success){
                    
                    // alert(data.msg);
                    if(data.redirect){
                        window.location.href = data.redirect;
                    }
                }else{
                    jQuery(".alert").empty().show();
                    if(Array.isArray(data.msg)){
                        (data.msg).forEach(function(element) {                           
                            jQuery("#error-msg").append( '<div class="alert alert-warning" role="alert">' + element + '</div>');
                        });
                        
                    }else{
                        jQuery("#error-msg").append(data.msg);
                    }

                    if(data.redirect){
                        window.location.href = data.redirect;
                    }
                    return false;
                }
            });
        }
        
    });

    //login form submit
    jQuery('#loginform').on('submit',(e)=>{
        e.preventDefault();
     
        var formData = jQuery('#loginform').serialize();
        
        if( ("<?php echo $pid;?>" == "" ) && ("<?php echo $bid;?>" == "") ){

            alert( "Please select an angel and a package first!");
            return false;

        }else{

            jQuery.ajax({
                method: "POST",
                url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                data: { 'action': 'sub_reg_user', 'data': formData , 'bname': '<?php echo  $bname;  ?>', 'packageid': '<?php echo $pid;  ?>' },
            })
            .done(function( data ) {
                var data =JSON.parse(data);
                
                if(data.success){
                    
                    // alert(data.msg);
                    if(data.redirect){
                        window.location.href = data.redirect;
                    }
                }else{

                    jQuery(".alert").empty().show();
                    if(Array.isArray(data.msg)){
                        (data.msg).forEach(function(element) {                            
                            jQuery("#error-msg").append( '<div class="alert alert-warning" role="alert">' + element + '</div>');
                        });
                        
                    }else{
                        jQuery("#error-msg").append(data.msg);
                    }
                   
                    if(data.redirect){
                        window.location.href = data.redirect;
                    }
                    // return false;
                }
            });
        }
        
    });

});
</script>

<?php } ?>

<?php get_footer(); ?>