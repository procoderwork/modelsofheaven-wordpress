<?php
/**
 * Template Name: MOH Register
 */

?>

<?php get_header(); ?>
<?php 
  

    if ( isset($_POST['submit'] ) ) {

            global $reg_errors;
            $reg_errors = new WP_Error;

            $fname =   sanitize_text_field( $_POST['fname'] );
            $lname  =   sanitize_text_field( $_POST['lname'] );
            $password   =   esc_attr( $_POST['password1'] );
            $repassword   =   esc_attr( $_POST['password2'] );
            $email      =   sanitize_email( $_POST['email'] );
            $tnc      =    $_POST['tnc'] ;
           

            // if ( empty( $fname ) || empty( $password ) || empty( $email ) ) 
            //     $reg_errors->add('field', 'Required form field is missing');
            

            if ( 4 > strlen( $fname ) ) 
                $reg_errors->add( 'username_length', __('Username too short. At least 4 characters are required' ,MOHTRON_TEXT) );            

            if ( username_exists( $fname ) )
                $reg_errors->add('user_name', __('Sorry, that username already exists!' ,MOHTRON_TEXT));

            if ( ! validate_username( strtolower($fname) ) )
                $reg_errors->add( 'username_invalid', __('Sorry, the username you entered is not valid' ,MOHTRON_TEXT) );

            if ( 8 > strlen( $password ) ) 
                $reg_errors->add( 'password', __('Password length must be greater than 8' ,MOHTRON_TEXT));
            
            if ( !is_email( $email ) ) 
                $reg_errors->add( 'email_invalid', __('Email is not valid' ,MOHTRON_TEXT) );
            
            if ( email_exists( $email ) ) 
                $reg_errors->add( 'email',  __('Email Already in use' ,MOHTRON_TEXT) );

            if ( $password != $repassword ) 
                $reg_errors->add( 're-password', __("Passwords don't match!",MOHTRON_TEXT) );

            if ( ! $tnc ) 
                $reg_errors->add( 'tnc', __('Please check Terms and Conditions!',MOHTRON_TEXT)  );

                // pr(is_wp_error( $reg_errors ));
                // pr($reg_errors );
           
            //if any validation error
            if ( 1 < is_wp_error( $reg_errors ) ) {
                
                echo '<div class="alert alert-danger"><strong>' . __('Fix following errors:',MOHTRON_TEXT) .'</strong></div>';
                foreach ( $reg_errors->get_error_messages() as $error ) {
                
                    echo '<div class="alert alert-warning">';
                    echo '<strong>';
                    echo __('ERROR',MOHTRON_TEXT );
                    echo  " </strong> : ";
                    echo $error . '<br/>';
                    echo '</div>';                    
                }            
            }

            //if validation is passed
            if ( 1 > count( $reg_errors->get_error_messages() ) ) {
                $userdata = array(
                                'user_login'    =>   $fname,
                                'user_email'    =>   $email,
                                'user_pass'     =>   $password,                               
                                'first_name'    =>   $fname,
                                'last_name'     =>   $lname               
                            );
                $user_id = wp_insert_user( $userdata );           

            
                
                $errors = array();
                //if any error while creating user;
                if( is_wp_error( $user_id ) ) {

                    $errors['creation'] = $user_id->get_error_message();

                }else{

                    //Login user after registration;
                    $creds = array(
                        'user_login'    => $fname ,
                        'user_password' => $password ,
                        'remember'      => true
                    );     
                    $user = wp_signon( $creds, false );

                    // pr($user);

                    if ( is_wp_error( $user ) ) {    
                        $errors['signin'] = $user->get_error_message();
                    }else{
                        wp_set_current_user($user->ID,$user->user_login);
                        wp_set_auth_cookie($user->ID);
                        do_action( 'wp_login', $user->user_login );
                    }
                }

                
                // pr(get_user_by('id',get_current_user_id())); 
                //  pr($errors);
                //if any signin error
                if( !empty($errors) ){
                    foreach ( $errors as $error ) {               
                        echo '<div class="alert alert-warning">';
                        echo '<strong>';
                        echo __('ERROR',MOHTRON_TEXT );
                        echo  " </strong> : ";
                        echo $error . '<br/>';
                        echo '</div>';                    
                    }
                }
                else{
                    // wp_redirect( home_url('/angels/')  );
                    echo  '<div class="container">
                                <div class="row registration-custom">        
                                    <div class="alert "><p>' . __('You are now registered with Models of Heaven as a Visitor.',MOHTRON_TEXT) . ' </p></div>        
                                    <div class="alert "><p>'. __("Please subscribe an angel\'s blog from ",MOHTRON_TEXT) .' <a href="/angels">' . __( 'Angels', MOHTRON_TEXT ) .'</a> ' . __( 'page', MOHTRON_TEXT ) .'</p></div>      
                                </div>
                            </div>';
                }     
                
            }
                
            
    }

?>


<?php 

if ( !is_user_logged_in() ) {



?>

    <div class="container">
        <div class="row padd-row login-page">
        
            <div class="col-md-12 col-sm-12">
            <div class="login-form-page">
                    <h3><?php echo __('Register with Models of Heaven',MOHTRON_TEXT ); ?></h3>
                    <form method="POST" id="register-visitor" action="<?php echo  $_SERVER['REQUEST_URI'] ;?>">
                        
                        <div class="form-group">
                            <label for="fname"><?php echo __('First name',MOHTRON_TEXT ); ?> *</label>
                            <input type="text" name="fname" class="form-control" id="fname" aria-describedby="fname" placeholder="<?php echo __('Enter First name',MOHTRON_TEXT ); ?>" value="<?php echo ( isset( $_POST['fname'] ) ? $fname : null );?>">
                        </div>
                        <div class="form-group">
                            <label for="fname"><?php echo __('Last name',MOHTRON_TEXT ); ?></label>
                            <input type="text" name="lname" class="form-control" id="lname" aria-describedby="lname" placeholder="<?php echo __('Enter Last name',MOHTRON_TEXT ); ?>" value="<?php echo ( isset( $_POST['lname'] ) ? $lname : null );?>">
                        </div>
                    
                        <div class="form-group">
                            <label for="email"><?php echo __('Email address',MOHTRON_TEXT ); ?> *</label>
                            <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="<?php echo __('Enter email',MOHTRON_TEXT ); ?>"  value="<?php echo ( isset( $_POST['email'] ) ? $email : null );?>">  
                            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                        </div>
                        <div class="form-group">
                            <label for="password1"><?php echo __('Password',MOHTRON_TEXT ); ?> *</label>
                            <input type="password" name="password1" class="form-control" id="password1" placeholder="<?php echo __('Enter Password',MOHTRON_TEXT ); ?>" value="<?php echo ( isset( $_POST['password1'] ) ? $password1 : null );?>"> 
                        </div>

                        <div class="form-group">
                            <label for="pssword2"><?php echo __('Retype-Password',MOHTRON_TEXT ); ?></label>
                            <input type="password" name="password2" class="form-control" id="password2" placeholder="<?php echo __('Retype password',MOHTRON_TEXT ); ?>"  value="<?php echo ( isset( $_POST['password2'] ) ? $password2 : null );?>"> 
                        </div>

                        <div class="form-check">
                            <input type="checkbox" name="tnc" class="form-check-input" id="tnc" <?php checked($_POST['tnc'],'on')?>>
                            <label class="form-check-label" for="tnc"><a href="/moh-terms"><?php echo __("I Agree to T&C's",MOHTRON_TEXT); ?></a></label>
                        </div>
                        <div class="form-group register-login">
                        <input type="submit" name="submit" class="btn btn-primary">
                        <span><?php echo __("Already registered.",MOHTRON_TEXT); ?></span>
                        <a href="/moh-login" ><?php echo __("Login",MOHTRON_TEXT); ?></a>
                        </div>
                    </form>
                </div>
                </div>
        </div>
    </div>
    
<?php }else{ ?>

    <div class="container">
        <div class="row registration-custom">
            <div class="alert "><p><?php echo __("Please subscribe an angel's blog from ",MOHTRON_TEXT); ?><a href="/angels"><?php echo __("Angels page",MOHTRON_TEXT); ?></a></p></div>
        </div>
    </div>
<?php } ?>





<?php get_footer(); ?>