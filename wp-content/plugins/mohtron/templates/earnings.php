<?php
    /* 
    * Display Earning details of a blogger/angel for each subscription made against her blog
    */

    //if super admin then use displayed blogger's id for earnings listing
    $currentUserId = get_current_user_id();
    if(is_super_admin($currentUserId)){
        $currentUserId = bp_displayed_user_id();
    }

    $mohtron_crud = new Mohtron_CRUD;


    $walletData = $mohtron_crud->mohtron_get_user_transactions( $currentUserId );
    $accBalance = $mohtron_crud->mohtron_get_user_account_balance( $currentUserId );

    if( empty($accBalance)){
        $accBalance = 0;
    }

    // pr($walletData);

    $lastPaypalID = get_user_meta( $currentUserId , 'last_used_paypal_email' , true);
    $sum = 0.00;
  	

?>


<h2 style="color: #33cc33;"><?php echo __('Balance',MOHTRON_TEXT); ?>: <span id="balance"><?php echo get_option('mohtron_settings')['mohtron_currency'].$accBalance; ?></span></h2>
<p>You can initiate a payout to your PayPal account in the form below.</p>	
		<h3>Transactions</h3>		
<div class="container earning-page">
    <div class="row">
        <div class="col-sm-12">
            <table class="responsive-table table">
                <thead>
                <tr>                    
                    <th><?php echo __('Date',MOHTRON_TEXT); ?></th>                    
                    <th><?php echo __('Type',MOHTRON_TEXT); ?></th>
                    <th><?php echo __('Description',MOHTRON_TEXT); ?></th>
                    <th><?php echo __('Amount/Balance',MOHTRON_TEXT); ?></th>
                </tr> 
                </thead>
                <tbody>
                <?php                 
                if( !empty ($walletData) ){  
                    foreach($walletData as $data){

                        $supporter = get_user_by('id',$data['supporter_id']);
                       
                ?>
                    <tr>
                        <td><?php echo date("Y-m-d", strtotime($data['create_time'])) ?></td>
                        <td><?php echo ucfirst(__($data['transaction_type'],MOHTRON_TEXT)); ?></td>

                        <!-- check if credit or debit (subscription or payout) -->
                        <?php if( $data['transaction_type'] == "credit"):?>
                            <td><?php echo __('Payout to PayPal',MOHTRON_TEXT); ?> <?php echo $data['payout_email']; ?></td>
                            <td>-<?php echo get_option('mohtron_settings')['mohtron_currency'].$data['user_amount'];?></td>
                            <?php  $sum -= floatval($data['user_amount']); ?>
                        <?php else: ?>
                            <td><?php echo __('New supporter - ',MOHTRON_TEXT); ?> <?php echo __($data['package_name'],MOHTRON_TEXT); ?></td>
                            <td><?php echo $data['currency_type'].$data['user_amount'];?></td>
                            <?php  $sum += floatval($data['user_amount']); ?>
                        <?php endif; ?>
                                                
                    </tr>
                    <?php 
                    }  
                } else{ ?>
                    <tr>
                        <td></td>
                        <td></td> 
                        <td id="no-subscription"><?php echo __('No transactions yet.',MOHTRON_TEXT); ?></td>
                        <td></td> 
                    </tr>

                <?php } ?>
                    
                </tbody>
                <tfoot style="font-weight: 600; background: #eee;">
                    <tr>
                        <td style="/* background: #eee; */"></td>
                        <td></td>
                        <td style="text-align: right;"><?php echo __('Sum',MOHTRON_TEXT); ?></td>
                        <td><?php echo get_option('mohtron_settings')['mohtron_currency']. number_format($sum,2);?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <?php   if ( ! is_super_admin( get_current_user_id() ) ){ ?>
        <div class="ajax-loader" style="display:none;"></div>
        <div class="alert alert-info"  style="display:none;"  id="success-msg" role="alert">            
        </div>
        <div class="alert alert-danger"  style="display:none;"  id="error-msg" role="alert">            
        </div>
        <h3><?php echo __('Initiate a payout',MOHTRON_TEXT); ?></h3>
        <?php if( $accBalance >= 1){ ?>
            <form method="post" action="#" id="payout">
                <label for=""><?php echo __('Payout amount',MOHTRON_TEXT); ?></label>
                <input type="number" id="payamount" name="payamount" min="1" max="<?php echo $accBalance; ?>" step="0.5"/>
                <label for=""><?php echo __('PayPal email',MOHTRON_TEXT); ?></label>
                <input type="text" id="payid"  name="payid" value="<?php echo $lastPaypalID;?>"/>
                <input type="submit" value="<?php echo __('Initiate payout now',MOHTRON_TEXT); ?>"/>
            </form>
        <?php }else { ?>
        
            <div class="alert alert-warning" role="alert">
                <?php echo __('For Payout minimum total amount needs to be',MOHTRON_TEXT); ?> $1
            </div>

        <?php } ?>
    <?php } ?>
    
</div>


<script>

    jQuery( ()=>{

        jQuery("#payout").on('submit',(e)=>{

            e.preventDefault();  
            
            $('.ajax-loader').hide();
            jQuery("#error-msg").empty().hide();
            jQuery("#success-msg").empty().hide();
            
            var $amount = $('#payamount');
            var $id = $('#payid');
              
            if( $amount[0].value == "" || $id[0].value == "" ){
                jQuery("#error-msg").show().append('<?php echo __('Payout Amount and PayPal ID Fields cannot be empty!',MOHTRON_TEXT); ?>');
                return false;
            }

            if( confirm('You are making payout request of  '+  $amount[0].value +' for Paypal ID  '+ $id[0].value   ) ){
                var formData = jQuery('#payout').serialize();
                $('.ajax-loader').show();
                jQuery.ajax({
                        method: "POST",
                        url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                        data: { 'action': 'blogger_payout', 'data':  formData }                        
                    })
                    .done(function( data ) {
                            $('.ajax-loader').hide();
                            var res = JSON.parse(data);
                            if(res.success){
                                //update balance
                                $('#balance').text(res.amt);
                                jQuery("#success-msg").empty().show();
                                if(Array.isArray(res.msg)){
                                    (res.msg).forEach(function(element) {
                                        jQuery("#success-msg").append( '<div class="alert alert-success" role="alert">' + element + '</div>');
                                    });                                    
                                }else{ jQuery("#success-msg").append(res.msg);}
                            }else{                                
                                jQuery("#error-msg").empty().show();
                                if(Array.isArray(res.msg)){
                                    (res.msg).forEach(function(element) {
                                        jQuery("#error-msg").append( '<div class="alert alert-info" role="alert">' + element + '</div>');
                                    });
                                }else{jQuery("#error-msg").append(res.msg); }
                            }
                    });
            }
            else{
                return false;
            }
        });
    });   
</script>