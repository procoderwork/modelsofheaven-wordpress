<?php
/**
 * Template Name: MOH Login
 */

?>

<?php get_header(); ?>


<?php 

if ( !is_user_logged_in() ) {

?>

    <div class="container">
        <div class="row padd-row login-page">
        
            <div class="col-md-12 col-sm-12 column-form column-login">
            <div class="login-form-page">
                <h3><?php echo __('Login if you are a registered member.',MOHTRON_TEXT ); ?></h3>       
                <?php

                    $args = array(
                        'redirect' => '/angels', 
                        'form_id' => 'loginform-moh',
                        'label_username' => __( 'Username ' ),
                        'label_password' => __( 'Password ' ),
                        'label_remember' => __( 'Remember Me ' ),
                        'label_log_in' => __( 'Log In ' ),
                        'remember' => true
                    );
                    wp_login_form( $args );
                
                ?>
        
                <a href="/moh-register" ><?php echo __('Register',MOHTRON_TEXT ); ?></a>
            </div>
            </div>
        </div>
    </div>
    
<?php }else{ ?>

   
   <div class="container">
        <div class="row registration-custom">
            <div class="alert "><p><?php echo __("Please subscribe an angel's blog from ",MOHTRON_TEXT); ?><a href="/angels"><?php echo __("Angels page",MOHTRON_TEXT); ?></a></p></div>
        </div>
    </div>
<?php } ?>


<?php get_footer(); ?>

