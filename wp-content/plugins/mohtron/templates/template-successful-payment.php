<?php
/**
 * Template Name: Subscriptions
 */
?>

<?php get_header(); ?>


<?php   if ( ! isset($_SERVER['HTTP_REFERER'] ) ){  ?>


   
  <div class="container">
        <div class="row registration-custom">
            <?php if(!is_user_logged_in()){ ?>
                <div class="alert "><p><?php echo __( 'Please', MOHTRON_TEXT ); ?> <a href="/moh-login"><?php echo __( 'Login', MOHTRON_TEXT ); ?></a> <?php echo __( 'or', MOHTRON_TEXT ); ?> <a href="/moh-register"><?php echo __( 'Register', MOHTRON_TEXT ); ?></a> <?php echo __( 'to subscribe to an angel !', MOHTRON_TEXT ); ?></p></div>
            <?php } else {?>
                <div class="alert "><p>
					<?php echo __( 'Please first select an angel from', MOHTRON_TEXT ); ?><a href="/angels"> <?php echo __( 'Angels', MOHTRON_TEXT ); ?> </a> <?php echo __( 'page to subscribe.', MOHTRON_TEXT ); ?></p>
				</div>
            <?php }?>
        </div>
    </div>



<?php    
}else{

    $bname = get_query_var('bname');
    $pid = get_query_var('packageid');
?>


    <div class="payment-success">

    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">
        <circle class="path circle" fill="none" stroke="#73AF55" stroke-width="6" stroke-miterlimit="10" cx="65.1" cy="65.1" r="62.1"/>
        <polyline class="path check" fill="none" stroke="#73AF55" stroke-width="6" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>
    </svg>
        <p class="success">Payment Successful</p>
    
    <a href="/members/<?php echo $bname;?>">Go to blog</a> 
    </div>

<?php } ?>
<?php get_footer(); ?>