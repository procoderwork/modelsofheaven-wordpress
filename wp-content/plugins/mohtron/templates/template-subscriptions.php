<?php
/**
 * Template Name: Subscriptions
 */
?>


<?php get_header(); ?>

<?php 
	$bname = get_query_var('bname');
	$pid = get_query_var('packageid');


	$paymentsData = get_option('mohtron_payments');
	// pr($paymentsData);
	$bloggerObj = get_user_by( 'slug', $bname );

	// pr($bloggerObj);

	if( in_array('bbp_participant',$bloggerObj->roles ) ){

			$validBlogger = true; 
	}else{

		$validBlogger = false; 
	}

	$currency = get_option('mohtron_settings')['mohtron_currency'];

	$packages = mohtron_get_packages();

?>

<?php //if bloggername is set in query var and is valid blogger id
   if( $validBlogger ){
?>	

<section class="container-wrap main-color">
	<div id="main-container" class="container">
		<div class="row"> 
			<div class="template-page col-sm-12 tpl-no">
				<div class="wrap-content">

					<div class="row membership pricing-table" id="packages-list">

					<?php						

						if(!empty($packages)){

							foreach ($packages as $key=>$val):
								echo "<form  method='POST' action='".home_url('/moh-registration/' . $bname . '/' . $val['id']) . "'>";
								echo "<div class='col-md-4 subscribe'><div class='custom-subscribe'>";
								echo '<h3>' . $val['package_name'] . "</h3>";
								echo '<span class="pckg-desc-text">' . $val['package_description'] . '</span>';
								echo '<span class="pckf-price">' . __('Price',MOHTRON_TEXT) . ' : '.$currency.$val['billing_details'] .'</span>';
								echo "<span class='exp-text'>" . __('Expires in',MOHTRON_TEXT) . " :  ".$val['expiration'] ." days</span>";
								echo "<input type='hidden' data-bname='" . $bname . "' />";
								echo "<input type='hidden' data-packageid='" . $val['id'] . "' />";
								echo "<input type='submit' value='" . __('Select',MOHTRON_TEXT) . "'/>";
								echo "</div></div>";								
								echo "</form>";
							endforeach;

						}else{

							echo "<h2>" . __( 'No package available right now. Please visit later.', MOHTRON_TEXT ) . "</h2>";
						}
					?>
					
					</div>						
										
				</div><!--end wrap-content-->
			</div><!--end main-page-template-->
		</div><!--end .row-->		
	</div><!--end .container-->
  
</section>

<script>
	var forms = document.querySelectorAll('#packages-list form');
	
	for(i=0, len=forms.length; i<len; i++){
		forms[i].addEventListener('submit', function(e){	 	
				e.preventDefault(); 
				//get current package and bloger id's;;
				var data = jQuery(this).find('input[type="hidden"]');				
				<?php if( is_user_logged_in() ){ ?>						
				//if user is logged in						
				jQuery.ajax({
					method: "POST",
					url: "<?php echo admin_url( 'admin-ajax.php' );?>",
					data: { 'action': 'mohtron_subscribe', 'packageid': data[1].dataset.packageid, 'bname':  data[0].dataset.bname },
				})
				.done(function( data ) {
					var data =JSON.parse(data);
					if(data.success){
						// alert(data.msg);
						if(data.redirect){
							window.location.href = data.redirect;
						}
					}else{

						alert(data.msg);
						if(data.redirect){
							window.location.href = data.redirect;
						}
					}
				});
				<?php } else{ ?>		
				//if user is not logged in
			
					jQuery(this).unbind('submit');
					jQuery(this).trigger('submit');
			
				<?php } ?>
		});
	}	
</script>

<?php } else { ?>


  <div class="container">
        <div class="row registration-custom">
            <?php if(!is_user_logged_in()){ ?>
                <div class="alert "><p><?php echo __( 'Please', MOHTRON_TEXT ); ?> <a href="/moh-login"><?php echo __( 'Login', MOHTRON_TEXT ); ?></a> <?php echo __( 'or', MOHTRON_TEXT ); ?> <a href="/moh-register"><?php echo __( 'Register', MOHTRON_TEXT ); ?></a> <?php echo __( 'to subscribe to an angel !', MOHTRON_TEXT ); ?></p></div>
            <?php } else {?>
                <div class="alert "><p>
					<?php echo __( 'Please first select an angel from', MOHTRON_TEXT ); ?><a href="/angels"> <?php echo __( 'Angels', MOHTRON_TEXT ); ?> </a> <?php echo __( 'page to subscribe.', MOHTRON_TEXT ); ?></p>
				</div>
            <?php }?>
        </div>
    </div>


<?php } ?>


<?php get_footer(); ?>
