<?php

// $form->setRenderSetting('raw');
// pr($data);
echo '<div class="notice notice-error"  style="display:none;" id="mohtron-error"></div>';

echo $form->open();

echo "<h3>".__("Select Currency",'mohtron')."</h3>";
$currency_options = [    
    // 'Danish KRONE kr' => 'kr',
    'US DOLLAR $' => '$',
    // 'EURO &euro;' => '&euro;',
    // 'GB Pound &pound;' => '&pound;',
];
$selectCurrency = $form->select('Mohtron Currency')->setOptions($currency_options);
$selectCurrency->setSetting('default', '$');
echo $selectCurrency;

echo "<h3>Wallet settings</h3>";
echo $form->text('MOH Percentage');
echo $form->text('Blogger Percentage');

echo $form->submit('Save');
echo $form->close();


?>
<style>
div#typerocket-admin-page {
    width: 40%;
}
div#typerocket-admin-page form input, select {
    width: 100%;
    height: 44px;
    padding-left: 14px;
    margin: 5px 0px 14px;
    box-shadow: none;
} 
div#typerocket-admin-page form select {
    height: 40px;
    padding-left: 14px;
    box-shadow: none;
}
div#typerocket-admin-page form input.button.button-primary {
    width: 150px;
    box-shadow: none;
    height: 40px;
}
</style>



<script>
    jQuery(document).ready(()=>{       

        jQuery("select[name='tr[mohtron_currency]']").val('<?php echo $data['mohtron_currency'];?>');
        jQuery("input[name='tr[moh_percentage]']").val('<?php echo $data['moh_percentage'];?>');
        jQuery("input[name='tr[blogger_percentage]']").val('<?php echo $data['blogger_percentage'];?>');

        jQuery('input[name="_tr_submit_form"]').on('click', (e)=>{
            e.preventDefault;
            var moh = jQuery('input[name="tr[moh_percentage]"').val();
            var blogger = jQuery('input[name="tr[blogger_percentage]"').val();
            var total = parseInt(moh) + parseInt(blogger);
           

            if( 0 > total || total > 100 || isNaN(total) ){               
                jQuery('#mohtron-error').show();
                jQuery('#mohtron-error').text("Total of % should be equal to 100");
                return  false;
            }

            if( null === jQuery('select[name="tr[mohtron_currency]"').val() ){
                // alert("Select currency!");
                jQuery('#mohtron-error').show();
                jQuery('#mohtron-error').text("Select currency!");
                return false;
            } 

           

            return true;
        })

        
    });
</script>