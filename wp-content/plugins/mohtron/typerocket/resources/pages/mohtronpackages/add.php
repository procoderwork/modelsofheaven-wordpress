<?php

// pr($form->getItemId(),1);
// pr( $form->getResource(),1);

$args = $form->getSettings();
$args = array_merge( $args, array( 'render' => 'raw' ) );
$form->setSettings( $args );

echo $form->open();

echo "</br></br><span>Name</span></br>";
echo $form->text('Package Name');

echo "</br></br><span>Description</span></br>";
echo $form->editor('Package Description');

echo "</br></br><span>Confirmation message</span></br>";
echo $form->editor('Package Confirmation');

echo "</br></br><span>Price</span></br>";
echo $form->text('Billing Details');

echo "</br></br><span>Duration</span></br>";
echo $form->text('Expiration');

echo "</br></br><span>Allow signups</span></br>";
echo $form->select('Allow signups')->setOptions(array('Yes'=>'true','No'=>'false'));

echo "</br></br>";
echo $form->submit('Add');
echo $form->close();