<?php

pr($data);

echo $form->open();

$payment_options = [
    'paypal' => 'Paypal',
];
echo $form->select('Payment Gateway')->setOptions($payment_options);

$environment_options = [
    'sandbox' => 'Sandbox',
    'production' => 'Production'
];
echo $form->select('Environment')->setOptions($environment_options);


echo $form->text('API KEY');

echo $form->text('API Username');
echo $form->text('API Password');
echo $form->text('API Signature');



echo $form->submit('Save');
echo $form->close();

// pr($form,1);