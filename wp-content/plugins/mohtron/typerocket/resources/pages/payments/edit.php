<?php

// $form->setRenderSetting('raw');
// pr($data);

echo '<div class="fade updated"  style="display:none;" id="mohtron-error"></div>';
echo $form->open();

$payment_options = [
    'Paypal' => 'paypal',
];
echo $form->select('Payment Gateway')->setOptions($payment_options);

$environment_options = [
    'Sandbox' => 'sandbox',
    'Production' => 'production'
];
echo $form->select('Environment')->setOptions($environment_options);


echo $form->text('Production Client ID');

echo $form->text('Production Client Secret Key');


echo $form->text('Sandbox Client ID');

echo $form->text('Sandbox Client Secret Key');


echo $form->submit('Save');

echo $form->close();

echo '<div class="ajax-loader" style="display:none;"></div>';
?>
<style>
div#typerocket-admin-page {
    width: 40%;
}
div#typerocket-admin-page form input, select {
    width: 100%;
    height: 44px;
    padding-left: 14px;
    margin: 5px 0px 14px;
    box-shadow: none;
} 
div#typerocket-admin-page form select {
    height: 40px;
    padding-left: 14px;
    box-shadow: none;
}
div#typerocket-admin-page form input.button.button-primary {
    width: 150px;
    box-shadow: none;
    height: 40px;
}
</style>


<script>
    jQuery(document).ready(()=>{

        
        var payGateVal = '<?php echo $data['payment_gateway'];?>';
        var envVal = '<?php echo $data['environment'];?>';
        var prodClientID=  jQuery("input[name='tr[production_client_id]']");
        var prodClientSecret=  jQuery("input[name='tr[production_client_secret_key]']");
        var sandClientID= jQuery("input[name='tr[sandbox_client_id]']");
        var sandClientSecret=  jQuery("input[name='tr[sandbox_client_secret_key]']");
        var envSelector =  jQuery("select[name='tr[environment]']");
        var paymentGatewaySelector =  jQuery("select[name='tr[payment_gateway]']");

        paymentGatewaySelector.val(payGateVal);
        envSelector.val(envVal);
        prodClientID.val('<?php echo $data['production_client_id'];?>');
        prodClientSecret.val('<?php echo $data['production_client_secret_key'];?>');
        sandClientID.val('<?php echo $data['sandbox_client_id'];?>');
        sandClientSecret.val('<?php echo $data['sandbox_client_secret_key'];?>');
        
        //on load show/hide the clientid and secret key inputs
        if( 'sandbox' == envVal ){
            prodClientID.parents('.control-section').hide();
            prodClientSecret.parents('.control-section').hide();
        }else{
            sandClientID.parents('.control-section').hide();
            sandClientSecret.parents('.control-section').hide();
        }

        //on change show clientid and secret inputs based on env type
        envSelector.on('change',(e)=>{

            var envType = jQuery('select[name="tr[environment]"] option:selected').val();

            if( envType == 'sandbox' ){

                prodClientID.parents('.control-section').hide();
                prodClientSecret.parents('.control-section').hide();

                sandClientID.parents('.control-section').show();
                sandClientSecret.parents('.control-section').show();

            }else{

                prodClientID.parents('.control-section').show();
                prodClientSecret.parents('.control-section').show();

                sandClientID.parents('.control-section').hide();
                sandClientSecret.parents('.control-section').hide();

            }

        });
       
    
        jQuery('input[name="_tr_submit_form"]').on('click', (e)=>{
            e.preventDefault();

            if ( envSelector.val() == 'production' ){
                if( prodClientID.val() != "" && prodClientSecret.val() != "" ){
                    jQuery('.ajax-loader').show();
                    jQuery.ajax({
                            method: "POST",
                            url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                            data: { 'action': 'mohtron_api_key_test', 'env': envSelector.val(),'clientid': prodClientID.val(), 'secretkey':  prodClientSecret.val() },
                        })
                        .done(function( data ) {
                            var data =JSON.parse(data);
                            if(data.status){
                                    jQuery('#mohtron-error').show();
                                    jQuery('#mohtron-error').text("Valid credentials");

                                    jQuery('input[name="_tr_submit_form"]').unbind('click');
                                    jQuery('form').trigger('submit');
                            }
                            else{ jQuery('#mohtron-error').show(); jQuery('#mohtron-error').text(data.msg);  jQuery('.ajax-loader').hide(); return false; }
                             jQuery('.ajax-loader').hide();  
                        });
                }
                else{ jQuery('#mohtron-error').show(); jQuery('#mohtron-error').text('ClientID and Secret Key cannot not blank !'); }
            }else{
                if( sandClientID.val() != "" && sandClientSecret.val() != "" ){
                    jQuery('.ajax-loader').show();
                    jQuery.ajax({
                            method: "POST",
                            url: "<?php echo admin_url( 'admin-ajax.php' );?>",
                            data: { 'action': 'mohtron_api_key_test', 'env': envSelector.val(),'clientid': sandClientID.val(), 'secretkey':  sandClientSecret.val() },
                        })
                        .done(function( data ) {
                            var data =JSON.parse(data);
                            if(data.status){
                                    jQuery('#mohtron-error').show();
                                    jQuery('#mohtron-error').text("Valid credentials");
                                    jQuery('input[name="_tr_submit_form"]').unbind('click');
                                    jQuery('form').trigger('submit');                                     
                            }
                            else{jQuery('#mohtron-error').show(); jQuery('#mohtron-error').text(data.msg);  jQuery('.ajax-loader').hide(); return false;}
                        });
                }
                else{ jQuery('#mohtron-error').show(); jQuery('#mohtron-error').text('ClientID and Secret Key cannot not blank !');}
            }            

        });

        
    });
</script>