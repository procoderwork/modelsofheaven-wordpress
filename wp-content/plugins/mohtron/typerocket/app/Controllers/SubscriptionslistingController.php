<?php

namespace App\Controllers;

use \TypeRocket\Controllers\Controller;



class SubscriptionslistingController extends Controller
{


    public function __construct(){
         
        // parent::__construct();

        add_action('tr_table_search_form', array($this, 'customsearchfilters'));

    }

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    {
        // TODO: Implement index() method.
        
        // global $wpdb;
        // $table_name = $wpdb->prefix . "mohtron_subscriptions";        
        // $blogger = $wpdb->get_results( $wpdb->prepare( "SELECT blogger_id FROM $table_name") ,ARRAY_A  );
        // $package = $wpdb->get_results( $wpdb->prepare( "SELECT product_id FROM $table_name") ,ARRAY_A  );
        // return tr_view('subscriptionslisting.index',[ 'blogger' => $blogger,'package'=>$package ] );

        return tr_view('subscriptionslisting.index');


    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.
    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
        // TODO: Implement update() method.
    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }



    /**
     * Custom search filter
     */
    public function customsearchfilters(){

        global $wpdb;
        $table_name = $wpdb->prefix . "mohtronpackages";
        
        $args = array(
            'role__in'    => array('bbp_participant','bbp_blogger'),
            'orderby' => 'user_nicename',
            'order'   => 'ASC'
        );
        $blogger = get_users( $args );
        $package = $wpdb->get_results( $wpdb->prepare( "SELECT id,package_name FROM $table_name") ,ARRAY_A  );

        ?>
        
        <label>Filter by angel</label>
        <select id ="blogger-filter" name="blogger-filter">
    
            <option value="" selected>All</option>      
            <?php
    
            foreach( $blogger as $col ){                 
                
                if( $_GET['blogger-filter'] == $col->ID ):
                    $selected = ' selected = "selected"';
                else:
                    $selected = "";    
                endif;
            ?>
                    <option value="<?php echo $col->ID; ?>" <?php echo $selected; ?>><?php echo $col->data->display_name; ?></option>
            <?php
                
            }
            ?>
        </select>

        <label>Filter by package</label>
        <select id ="package-filter" name="package-filter">
    
            <option value="" selected>All</option>      
            <?php
    
            foreach( $package as $col ){                        
    
                if( $_GET['package-filter'] == $col['id'] ):
                    $selected = ' selected = "selected"';
                else:
                    $selected = "";    
                endif;
            ?>
                    <option value="<?php echo trim($col['id'] ); ?>" <?php echo $selected; ?>><?php echo $col['package_name']; ?></option>
            <?php
                
            }
            ?>
        </select>
    
    
    <?php

    }




}