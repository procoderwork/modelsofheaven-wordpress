<?php
namespace App\Controllers;

use \TypeRocket\Controllers\Controller;

class MohtronsettingsController extends Controller
{

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    { 
        // TODO: Implement index() method.       
    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.
    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.

        $mohtronsettings = new \App\Models\Mohtronsettings;
        $res = $mohtronsettings->show();
      
        $form = tr_form('mohtronsettings', 'update');
        return tr_view('mohtronsettings.edit',['form' => $form,
                                        'data' => $res]
                                    );
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
       // TODO: Implement update() method.
        
       $mohtronsettings = new \App\Models\Mohtronsettings;
       $res = $mohtronsettings->save();
       $this->response->flashNext($res);     
       return tr_redirect()->toPage('mohtronsettings', 'edit')->withFields( ['mohtron_currency' => $_POST['tr']['mohtron_currency'] ], 
                                                                            ['moh_percentage' => $_POST['tr']['moh_percentage']],
                                                                            ['blogger_percentage' => $_POST['tr']['blogger_percentage'] ]                                                                          
                                                                        ); 
    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}