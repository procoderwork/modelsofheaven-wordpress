<?php
namespace App\Controllers;

use \TypeRocket\Controllers\Controller;

class PaymentsController extends Controller
{

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    {
        // TODO: Implement index() method.    
      
    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.


    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.
   
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.

        $payments = new \App\Models\Payments;
        $res = $payments->show();
      
        $form = tr_form('payments', 'update');
        return tr_view('payments.edit',['form' => $form,
                                        'data' => $res]
                                    );
    
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
        // TODO: Implement update() method.
        
        $payments = new \App\Models\Payments;
        $res = $payments->save();
        $this->response->flashNext($res);     
        return tr_redirect()->toPage('payments', 'edit')->withFields( ['payment_gateway' => $_POST['tr']['payment_gateway'] ], 
                                                                      ['environment' => $_POST['tr']['environment'] ],
                                                                      ['api_key' => $_POST['tr']['api_key'] ],
                                                                      ['api_username' => $_POST['tr']['api_username'] ],
                                                                      ['api_password' => $_POST['tr']['api_password'] ],
                                                                      ['api_signature' => $_POST['tr']['api_signature']]
                                                                    ); 

    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}