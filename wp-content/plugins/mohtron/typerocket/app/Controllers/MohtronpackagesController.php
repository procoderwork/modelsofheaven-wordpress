<?php
namespace App\Controllers;

use \TypeRocket\Controllers\Controller;

class MohtronpackagesController extends Controller
{

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    {
        // TODO: Implement index() method.

        return tr_view('mohtronpackages.index');

    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.
        $form = tr_form('mohtronpackages', 'create');        
        return tr_view('mohtronpackages.add', ['form' => $form]);
    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.

        $packages = new \App\Models\Mohtronpackages;
        $packages->package_name = $this->request->getFields('package_name');
        $packages->package_description = $this->request->getFields('package_description');
        $packages->package_confirmation = $this->request->getFields('package_confirmation');
        $packages->billing_details = $this->request->getFields('billing_details');
        $packages->expiration = $this->request->getFields('expiration');
        $packages->allow_signups = $this->request->getFields('allow_signups');
        $packages->time = current_time( 'mysql' );
  
        $res = $packages->save();
        if($res == 1){
            $msg = "Package created.";
        }else{
            $msg = "Sorry there was an error. Try again.";
        }
        $this->response->flashNext($msg);
        return tr_redirect()->toPage('mohtronpackages', 'index');
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.

        $form = tr_form('mohtronpackages', 'update', $id);
        return tr_view('mohtronpackages.edit', ['form' => $form] );
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id , \App\Models\Mohtronpackages $packages)
    {
        // TODO: Implement update() method.

        // $packages = new \App\Models\Mohtronpackages

        $packages->package_name = $this->request->getFields('package_name');
        $packages->package_description = $this->request->getFields('package_description');
        $packages->package_confirmation = $this->request->getFields('package_confirmation');
        $packages->billing_details = $this->request->getFields('billing_details');
        $packages->expiration = $this->request->getFields('expiration');
        $packages->allow_signups = $this->request->getFields('allow_signups');
        $packages->time = current_time( 'mysql' );

        $res = $packages->save();
        if($res == 1){
            $msg = "Package updated.";
        }else{
            $msg = "Sorry there was an error updating package. Try again.";
        }
        $this->response->flashNext($msg);
        return tr_redirect()->toPage('mohtronpackages', 'edit', $id);
    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}