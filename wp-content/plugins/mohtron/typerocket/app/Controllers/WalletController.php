<?php
namespace App\Controllers;

use \TypeRocket\Controllers\Controller;

class WalletController extends Controller
{


    public function __construct(){
         
        // parent::__construct();

        add_action('tr_table_search_form', array($this, 'customsearchfilters'));

    }

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    {
        // TODO: Implement index() method.
      
        return tr_view('wallet.index');
    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.
    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
        // TODO: Implement update() method.
    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }



    /**
     * Custom search filter
     */
    public function customsearchfilters(){

        global $wpdb;
        $table_name = $wpdb->prefix . "mohtron_user_transactions";
        
        $args = array(
            'role__in'    => array('bbp_participant','bbp_blogger'),
            'orderby' => 'user_nicename',
            'order'   => 'ASC'
        );
        $txntype = array('credit','debit');
        $amount = $wpdb->get_results( $wpdb->prepare( "SELECT DISTINCT user_amount FROM $table_name WHERE user_id=1") ,ARRAY_A  );
        // pr($amount,1);
        ?>
        
        <!-- <label>Filter by amount</label>
        <select id ="amount-filter" name="amount-filter">
    
            <option value="" selected>All</option>      
            <?php
    
            // foreach( $amount as $col ){                 
                
            //     if( $_GET['amount-filter'] == $col['user_amount'] ):
            //         $selected = ' selected = "selected"';
            //     else:
            //         $selected = "";
            //     endif;
            ?>
                    <option value="<?php //echo $col['user_amount']; ?>" <?php //echo $selected; ?>><?php //echo $col['user_amount']; ?></option>
            <?php
                
            //}
            ?>
        </select>

        <label>Filter by transaction type</label>
        <select id ="txntype-filter" name="txntype-filter">
    
            <option value="" selected>All</option>
            <?php
    
            // foreach( $txntype as $col=>$val ){
            //     if( $_GET['txntype-filter'] == $val ):
            //         $selected = ' selected = "selected"';
            //     else:
            //         $selected = "";
            //     endif;
           
            //     echo "<option value='" . $val . "' " . $selected . ">".ucfirst($val)."</option>";
                
            // }
            ?>
        </select> -->

        
             
        <input type="text" id ="search-filter" value="<?php if( isset( $_GET['search-filter']  ) ) echo $_GET['search-filter']; ?>" name="search-filter">
       
       
    
    <?php

    }

}