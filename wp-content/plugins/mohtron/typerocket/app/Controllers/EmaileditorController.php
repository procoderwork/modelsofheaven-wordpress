<?php
namespace App\Controllers;

use \TypeRocket\Controllers\Controller;

class EmailEditorController extends Controller
{

    /**
     * The index page for admin
     *
     * @return mixed
     */
    public function index()
    {
        // TODO: Implement index() method.
    }

    /**
     * The add page for admin
     *
     * @return mixed
     */
    public function add()
    {
        // TODO: Implement add() method.
    }

    /**
     * Create item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @return mixed
     */
    public function create()
    {
        // TODO: Implement create() method.
    }

    /**
     * The edit page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function edit($id)
    {
        // TODO: Implement edit() method.

        $emaileditor = new \App\Models\Emaileditor;
        $res = $emaileditor->show();
        
        $form = tr_form('emaileditor', 'update');
        return tr_view('emaileditor.edit',['form' => $form,
                                        'data' => $res]
                                    );
    }

    /**
     * Update item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function update($id)
    {
        // TODO: Implement update() method.

        $emaileditor = new \App\Models\Emaileditor;
        $res = $emaileditor->save();
        $this->response->flashNext($res);     
        return tr_redirect()->toPage('emaileditor', 'edit')->withFields( ['email_subject' => $_POST['tr']['email_subject'] ], 
                                                                      ['email_body' => $_POST['tr']['email_body'] ]                                                                     
                                                                    ); 
    }

    /**
     * The show page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function show($id)
    {
        // TODO: Implement show() method.
    }

    /**
     * The delete page for admin
     *
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * Destroy item
     *
     * AJAX requests and normal requests can be made to this action
     *
     * @param $id
     *
     * @return mixed
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
    }
}