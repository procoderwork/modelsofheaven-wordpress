<?php
namespace App\Models;

use \TypeRocket\Models\Model;

class Payments extends Model
{
    protected $resource = 'options';
     
    
    public function show(){

        $result = get_option('mohtron_payments');
        return $result;
    }

    public function save(){
        
        if ( ! get_option( 'mohtron_payments' ) )$result = add_option('mohtron_payments',$_POST['tr']);
        else  $result = update_option('mohtron_payments',$_POST['tr']);
        
        // if($result)$res = "Updated";
        // else $res = "Some error occured ! Plese try again.";

        $res = "Updated";

        return $res;
    }
}