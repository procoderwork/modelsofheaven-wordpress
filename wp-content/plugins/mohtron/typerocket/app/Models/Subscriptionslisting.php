<?php
namespace App\Models;

use \TypeRocket\Models\Model;

class Subscriptionslisting extends Model
{
    protected $resource = 'mohtron_subscriptions';

    public function __construct(){
         
        parent::__construct();

        add_action('tr_table_search_model', array($this, 'updatesearch'));

    }
    

    public function updatesearch($model){
        

        if( !empty($_GET['blogger-filter'])   &&  empty( $_GET['package-filter'] ) ){            

            return $model->select()->where('blogger_id', sanitize_text_field($_GET['blogger-filter']) )->get();

        }

        if( empty($_GET['blogger-filter'])  &&  !empty( $_GET['package-filter'] ) ){
        
            return $model->select()->where('product_id', sanitize_text_field($_GET['package-filter']) )->get();
        }

        if( !empty($_GET['blogger-filter']) &&  !empty( $_GET['package-filter'] ) ){

            return $model->select()->where('blogger_id', sanitize_text_field($_GET['blogger-filter']) )->where('product_id', sanitize_text_field($_GET['package-filter']) )->get();

        }
        
        return $model->select()->get();
     

       
    }

   
}