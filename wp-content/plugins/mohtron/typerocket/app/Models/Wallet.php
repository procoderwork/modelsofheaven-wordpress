<?php
namespace App\Models;

use \TypeRocket\Models\Model;

class Wallet extends Model
{
    protected $resource = 'mohtron_user_transactions';

    public function __construct(){
         
        parent::__construct();

        add_action('tr_table_search_model', array($this, 'updatesearch'));

    }


    public function old_updatesearch($model){
        

        if( !empty($_GET['amount-filter'])   &&  empty( $_GET['txntype-filter'] ) ){

            return $model->select()->where('user_amount', sanitize_text_field($_GET['amount-filter']) )->where('user_id', 1 )->get();

        }

        if( empty($_GET['amount-filter'])  &&  !empty( $_GET['txntype-filter'] ) ){
        
            return $model->select()->where('transaction_type', sanitize_text_field($_GET['txntype-filter']) )->where('user_id', 1 )->get();
        }

        if( !empty($_GET['amount-filter']) &&  !empty( $_GET['txntype-filter'] ) ){

            return $model->select()->where('user_amount', sanitize_text_field($_GET['amount-filter']) )->where('transaction_type', sanitize_text_field($_GET['txntype-filter']) )->where('user_id', 1 )->get();
        }
        
        return $model->select()->where('user_id', 1 )->get();

       
    }

    public function updatesearch($model){
            
        global $wpdb;

        if( !empty($_GET['search-filter']) ){

            $s = sanitize_text_field($_GET['search-filter']);
            $table = $wpdb->prefix. "mohtron_user_transactions";
            $users = get_wallet_user($s);
            $transid = $s;

            $subquery = '(
                            user_amount LIKE %s
                            OR total_amount LIKE %s
                            OR payout_batch_id LIKE %s
                            OR transaction_id LIKE %s
                        )';
          
            if( !empty( $users ) ){

                foreach($users as $key=>$user){
                    $res = $wpdb->get_results( $wpdb->prepare( "SELECT transaction_id FROM $table WHERE  user_id = %s AND user_id NOT IN (1)" , $user) ,ARRAY_A );
                }

                // pr(sizeof($res));

                $transids = array();

                // $transids  = '(';
               
                foreach($res  as $k=>$tid){                   
                    
                    if( $tid['transaction_id'] != '0' ){

                        // $transids .= "'".$tid['transaction_id']."'";


                        $transids[] = $tid['transaction_id'];

                        // if( sizeof($res) != $k ) $transids .= ",";

                    }


                }



                $_transids = '(\''.implode("','",$transids).'\')';
                 
                $subquery = '(
                                user_amount LIKE %s
                                OR total_amount LIKE %s
                                OR payout_batch_id IN '. $_transids  .'
                                OR transaction_id IN '. $_transids .'
                            )';
            }
         
            $s = '%'.$s.'%';
            $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table 
                                                        WHERE   user_id = 1 
                                                        AND 
                                                        $subquery ",$s,$s,$s,$s,ARRAY_A ));          

            foreach( $result as $k=>$obj ){
                $where[$k] =  $obj->id;
            }            
            // pr($result);
            // pr($wpdb->last_query,1);

            return $model->select()
                         ->where('id', 'IN' , $where  )    
                         ->get();
           
        }

        return $model->select()->where('user_id', 1 )->get();

    }

}