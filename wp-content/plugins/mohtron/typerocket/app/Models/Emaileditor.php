<?php
namespace App\Models;

use \TypeRocket\Models\Model;

class EmailEditor extends Model
{
    protected $resource = 'emaileditors';
    
    public function show(){

        $result = get_option('mohtron_emaileditor');
        return $result;
    }

    public function save(){
        
        if ( ! get_option( 'mohtron_emaileditor' ) )$result = add_option('mohtron_emaileditor',$_POST['tr']);
        else  $result = update_option('mohtron_emaileditor',$_POST['tr']);
        
        // if($result)$res = "Updated";
        // else $res = "Some error occured ! Plese try again.";

        $res = "Updated";

        return $res;
    }
}