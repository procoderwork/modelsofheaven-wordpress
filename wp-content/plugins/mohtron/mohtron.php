<?php
/*
Plugin Name: Mohtron
Plugin URI: http://www.dmarkweb.com
Description: This plugin is an addon to BuddyPress.
Version: 1.0.0
Text Domain: mohtron
Domain Path: /lang
Author: DM+ Team
Author URI: http://www.dmarkweb.com
License: GPL2
*/

// Exit if the file is accessed directly over web.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//pretty array printer
if ( !function_exists('pr') ){
    function pr( $arr , $dye = 0){
        $root = debug_backtrace();
        $file = str_replace($_SERVER['DOCUMENT_ROOT'],'',$root[0]['file']);
        $string = '</br>Line: '.$root[0]['line'].'</br>File : '.$file;

        echo "<pre>";        
        print_r($arr);        
        echo "</pre>";    

        if($dye == 1){            
            die($string);
        }
    }
}


// die( var_dump( get_locale() ) );

if(!defined( 'MOHTRON_TEXT'))
	define( 'MOHTRON_TEXT', 'mohtron');

if(!defined( 'MOHTRON_PLUGIN_DIR'))
    define( 'MOHTRON_PLUGIN_DIR', dirname(__FILE__));
    
if(!defined( 'MOHTRON_PLUGIN_URI'))
	define( 'MOHTRON_PLUGIN_URI', '/wp-content/plugins/mohtron');

// include(MOHTRON_PLUGIN_DIR . '/includes/class-mohtron-settings.php');


add_action('init', 'mohtron_load');
function mohtron_load(){
    load_plugin_textdomain( MOHTRON_TEXT, false, MOHTRON_PLUGIN_DIR.'/lang/' );
}

/* If BP is loaded and initialized. */
function mohtron_init() {
    //Check if bbPress plugin is loaded
    if ( class_exists( 'bbPress' ) ) {
        // bbPress is enabled so let's begin   
        // require_once( MOHTRON_PLUGIN_DIR . '/includes/class-mohtron-subscribe-to-blog-widget.php');
        require_once( MOHTRON_PLUGIN_DIR . '/includes/class-mohtron-email.php' );
        require_once( MOHTRON_PLUGIN_DIR . '/admin/settings-email.php' );
        require_once( MOHTRON_PLUGIN_DIR . '/includes/mohtron-funcs.php' );

    }else{
        //throw bbPress requirement error
        echo '<div class="error"><p>' . __( 'Warning: The theme needs bbPress Plugin to function', MOHTRON_TEXT ) . '</p></div>';
    }
}
add_action( 'bp_include', 'mohtron_init' );


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-mohtron-activator.php
 */
function activate_mohtron() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mohtron-activator.php';
	Mohtron_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-mohtron-deactivator.php
 */
function deactivate_mohtron() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-mohtron-deactivator.php';
	Mohtron_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_mohtron' );
register_deactivation_hook( __FILE__, 'deactivate_mohtron' );
