<?php

/**
 * BuddyPress - Users Activity
 *
 * @package BuddyPress
 * @subpackage bp-legacy
 */

?>

<?php
	// pr( get_user_by( 'id', bp_displayed_user_id() ) );
	// pr(bp_displayed_user_id()); //id of displayed user
	// pr(get_current_user_id());  // id of logged in user
	// pr(bbp_get_user_role(get_current_user_id())); // logged in user role
	// pr(bbp_get_user_role(bp_displayed_user_id())); // displayed user role

?>

<div class="item-list-tabs no-ajax" id="subnav" aria-label="<?php esc_attr_e( 'Member secondary navigation', 'buddypress' ); ?>" role="navigation">

	<ul>

		<?php 

			bp_get_options_nav(); 
		
		?>

		<!-- <li id="activity-filter-select" class="last">
			<label for="activity-filter-by"><?php //_e( 'Show:', 'buddypress' ); ?></label>
 
			<select id="activity-filter-by">
            <option value="-1"><?php //_e( '&mdash; Everything &mdash;', 'buddypress' ); ?></option>

				<?php //bp_activity_show_filters(); ?>

				<?php

				/**
				 * Fires inside the select input for member activity filter options.
				 *
				 * @since 1.2.0
				 */
				//do_action( 'bp_member_activity_filter_options' ); ?>

			</select>

		</li> -->
	</ul> 
</div><!-- .item-list-tabs -->

<?php

/**
 * Fires before the display of the member activity post form.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_member_activity_post_form' ); ?>

<?php

if ( is_user_logged_in() && bp_is_my_profile() && ( !bp_current_action() || bp_is_current_action( 'just-me' ) ) 
		&& 
		( checkUserRole('bbp_keymaster') || checkUserRole('bbp_participant') ) 
	)
	bp_get_template_part( 'activity/mohtron-post-form' );

/**
 * Fires after the display of the member activity post form.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_member_activity_post_form' );

/**
 * Fires before the display of the member activities list.
 *
 * @since 1.2.0
 */
do_action( 'bp_before_member_activity_content' ); ?>

<div class="activity" aria-live="polite" aria-atomic="true" aria-relevant="all">

	<?php bp_get_template_part( 'activity/activity-loop' ) ?>

</div><!-- .activity -->

<?php

/**
 * Fires after the display of the member activities list.
 *
 * @since 1.2.0
 */
do_action( 'bp_after_member_activity_content' ); ?>

<script>
	function getval(sel){

		// console.log(sel.getAttribute('data-act-id'));
		if(sel.getAttribute('data-act-id') != -1){

			jQuery.ajax({
				method: "POST",
				url: "<?php echo admin_url( 'admin-ajax.php' );?>",
				data: { 'action': 'updatePostVisibility', 'postid' : sel.getAttribute('data-act-id'), 'visibility': sel.value  },			
			})
			.done(function( data ) {			
				console.log( jQuery(sel) );
				// console.log( data );
				var res = JSON.parse(data);

				jQuery(sel).siblings('span').remove();				
				jQuery(sel).after( '<span class="visi-msg">' + res.msg + '</span>' );
						
			});
		}
		
	}


</script>