<?php

/**
 * Fired during plugin activation
 *
 * @link       www.dmarkweb.com
 * @since      1.0.0
 *
 * @package    Mohtron
 * @subpackage Mohtron/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Mohtron
 * @subpackage Mohtron/includes
 * @author     DM+ TEAM <dmarkweb@gmail.com>
 */
class Mohtron_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {	

		
		require_once(MOHTRON_PLUGIN_DIR . '/includes/class-mohtron-migrations.php');
		

		//Add required pages
		$pages = array('Subscriptions','MOH-Registration','MOH-Payments', 'MOH-Success','MOH-Login','MOH-Register','MOH-Terms');

		foreach($pages as $key=> $page){

				$new_page_title = $page;
				$new_page_content = $page;
				$new_page_template = ''; //MOHTRON_PLUGIN_DIR . '/templates/template-subscriptions.php';
			
				$page_check = get_page_by_title($new_page_title);
				$new_page = array(
						'post_type' => 'page',
						'post_title' => $new_page_title,
						'post_content' => $new_page_content,
						'post_status' => 'publish',
						'post_author' => 1,
				);
				if(!isset($page_check->ID)){
					$new_page_id = wp_insert_post($new_page);
					if(!empty($new_page_template)){
							update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
					}
				}
		}



		//add supporter and blogger roles to superadmin with keymaster role	
		$admins = get_super_admins();
		foreach( $admins as $admin){

			$adminObj = get_user_by( 'slug', $admin );
			
			if ( in_array('bbp_keymaster',$adminObj->roles) ){

				$adminObj->add_role( 'bbp_supporter');
				$adminObj->add_role( 'bbp_participant');			
			}   
			
		}




		
		
	}

}
