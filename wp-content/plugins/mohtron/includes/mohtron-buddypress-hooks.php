<?php

//------------------------------------------------------------
// Filter to render template

function mohtron_filter_init() {
 
    // add_action( 'bp_template_title', 'mohtron_template_title' );
    // add_action( 'bp_template_content', 'mohtron_template_content' );
    add_filter( 'bp_get_template_part', 'mohtron_template_part_filter', 10, 3 );
    
}
add_action('bp_init', 'mohtron_filter_init');


// Function to provide no template if User is visitor for Posting Activity
function mohtron_template_part_filter( $templates, $slug, $name ) {
    
    // $currentUserRole = bbp_get_user_role(get_current_user_id());

    // pr($currentUserRole, 1);

    // if ( 'activity/post-form' != $slug )
        // return $templates;
    
    // return bp_get_template_part( 'members/single/plugins' );
    return $templates;
}
    
function mohtron_template_title() {
    echo 'Title';
}
    
function mohtron_template_content() {
    echo 'Content';
}