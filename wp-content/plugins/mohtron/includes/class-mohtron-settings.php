<?php
class MohtronSettings
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
    
        // This page will be under "Settings"
        add_menu_page(
            'Settings Admin',
            'Mohtron',
            'manage_options',
            'mohtron-settings-admin',
            array( $this, 'create_admin_page' ),
            // 'dashicons-admin-settings'
            'dashicons-groups'
        );

    
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'mohtron_options' );
        ?>
        <div class="wrap">
            <h1>Mohtron Settings</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'mohtron_option_group' );
                do_settings_sections( 'mohtron-settings-admin' );
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {
        
        register_setting(
            'mohtron_option_group', // Option group
            'mohtron_options', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'wallet_section', // ID
            'Wallet Configuration', // Title
            array( $this, 'print_wallet_section_info' ), // Callback
            'mohtron-settings-admin' // Page
        );  

        add_settings_field(
            'moh_percentage', 
            'MOH %', 
            array( $this, 'moh_percentage' ), 
            'mohtron-settings-admin', 
            'wallet_section'
        );     

        add_settings_field(
            'blogger_percentage', 
            'Bloggers %', 
            array( $this, 'blogger_percentage' ), 
            'mohtron-settings-admin', 
            'wallet_section'
        );      

        add_settings_section(
            'paypal_section', 
            'Paypal Configuration', 
            array( $this, 'print_paypal_section_info' ), 
            'mohtron-settings-admin'
        );  

        add_settings_field(
            'paypal_api_key', 
            'Paypal API KEY', 
            array( $this, 'paypal_api_key' ), 
            'mohtron-settings-admin', 
            'paypal_section'
        );     
       
        add_settings_section(
            'package_configurator', 
            'Package Configuration', 
            array( $this, 'print_package_section_info' ), 
            'mohtron-settings-admin'
        );  

        add_settings_field(
            'package_1',
            'Package 1',
            array( $this, 'package_1' ), 
            'mohtron-settings-admin', 
            'package_configurator'
        );     
       
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        // if( isset( $input['id_number'] ) )
        //     $new_input['id_number'] = absint( $input['id_number'] );

        // if( isset( $input['title'] ) )
        //     $new_input['title'] = sanitize_text_field( $input['title'] );

        if( isset( $input['moh_percentage'] ) )
            $new_input['moh_percentage'] = sanitize_text_field( $input['moh_percentage'] );

        if( isset( $input['blogger_percentage'] ) )
            $new_input['blogger_percentage'] = sanitize_text_field( $input['blogger_percentage'] );

        if( isset( $input['paypal_api_key'] ) )
            $new_input['paypal_api_key'] = sanitize_text_field( $input['paypal_api_key'] );

        if( isset( $input['package_1'] ) )
            $new_input['package_1'] = sanitize_text_field( $input['package_1'] );

        return $new_input;
    }

    /** 
     * Print wallet the Section text
     */
    public function print_wallet_section_info()
    {
        print 'Enter percentage ratios below:';
    }

    /** 
     * Print wallet the Section text
     */
    public function print_paypal_section_info()
    {
        print 'Enter PAYPAL details below:';
    }

    /** 
     * Print wallet the Section text
     */
    public function print_package_section_info()
    {
        print 'Enter Package details below:';
    }

   

    /** 
     * Get the settings option array and print one of its values
     */
    public function moh_percentage()
    {
        printf(
            '<input type="text" id="moh_percentage" name="mohtron_options[moh_percentage]" value="%s" />',
            isset( $this->options['moh_percentage'] ) ? esc_attr( $this->options['moh_percentage']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function blogger_percentage()
    {
        printf(
            '<input type="text" id="blogger_percentage" name="mohtron_options[blogger_percentage]" value="%s" />',
            isset( $this->options['blogger_percentage'] ) ? esc_attr( $this->options['blogger_percentage']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function paypal_api_key()
    {
        printf(
            '<input type="text" id="paypal_api_key" name="mohtron_options[paypal_api_key]" value="%s" />',
            isset( $this->options['paypal_api_key'] ) ? esc_attr( $this->options['paypal_api_key']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function package_1()
    {
        printf(
            '<input type="text" id="package_1" name="mohtron_options[package_1]" value="%s" />',
            isset( $this->options['package_1'] ) ? esc_attr( $this->options['package_1']) : ''
        );
    }
    
}

if( is_admin() )
    $my_settings_page = new MohtronSettings();