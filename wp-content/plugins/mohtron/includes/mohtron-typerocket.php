<?php

// visibility level
$settings = ['capability' => 'administrator'];




// admin settings parent page
$mohtron = tr_page('MohtronSettings', 'edit', 'Mohtron',  $settings);
$mohtron->removeMenu();
$arguments = $mohtron->getArguments();
$arguments = array_merge( $arguments, [ 'position' => 65 ] );
$mohtron->setArguments( $arguments );
$mohtron->useController();



// admin settings page for payments
$packages = tr_resource_pages('Mohtronpackages', 'Packages',  $settings)->setIcon('tr-icon-euro')->useController();
$args1 = $packages->getArguments();
$args1 = array_merge( $args1, [ 'position' => 65 , 'view_file' => 'index' ] );
$packages->setArguments( $args1 );
// $packages->removeMenu();
// $mohtron->addNewButton();
$packages->useController();
$packages->setParent($mohtron);

// admin settings page for Payments
$payments = tr_page('Payments', 'edit', 'Payments',  $settings);
$args2 = $payments->getArguments();
$args2 = array_merge( $args2, [ 'position' => 65 ] );
$payments->setArguments( $args2 );
$payments->useController();
$payments->setParent($mohtron);

// admin settings page for Wallet
$wallet = tr_page('Wallet', 'index', 'Wallet',  $settings);
$args3 = $wallet->getArguments();
$args3 = array_merge( $args3, [ 'position' => 65 ] );
$wallet->setArguments( $args3 );
$wallet->useController();

$wallet->setParent($mohtron);


// admin settings page for Sale statistic
// $statistic = tr_page('SaleStatistic', 'index', 'Sale Statistic',  $settings);
// $args4 = $statistic->getArguments();
// $args4 = array_merge( $args4, [ 'position' => 65 ] );
// $statistic->setArguments( $args4 );
// $statistic->useController();
// $statistic->setParent($mohtron);


// admin settings page for Subscriptions Listing
$sublisting = tr_page('SubscriptionsListing', 'index', 'Subscriptions Listing',  $settings);
$args5 = $sublisting->getArguments();
$args5 = array_merge( $args5, [ 'position' => 65 ] );
$sublisting->setArguments( $args5 );
$sublisting->useController();
$sublisting->setParent($mohtron);


// admin settings page for HTML Editor
// $emaileditor = tr_page('EmailEditor', 'edit', 'Email Editor',  $settings);
// $args6 = $emaileditor->getArguments();
// $args6 = array_merge( $args6, [ 'position' => 65 ] );
// $emaileditor->setArguments( $args6 );
// $emaileditor->useController();
// $emaileditor->setParent($mohtron);

