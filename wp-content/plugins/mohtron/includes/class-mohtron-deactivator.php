<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.dmarkweb.com
 * @since      1.0.0
 *
 * @package    Mohtron
 * @subpackage Mohtron/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Mohtron
 * @subpackage Mohtron/includes
 * @author     DM+ TEAM <dmarkweb@gmail.com>
 */
class Mohtron_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

		//remove supporter and blogger role of admin
		$admins = get_super_admins();
		foreach( $admins as $admin){

			$adminObj = get_user_by( 'slug', $admin );
			
			if ( in_array('bbp_keymaster',$adminObj->roles) ){

				$adminObj->remove_role( 'bbp_supporter');
				$adminObj->remove_role( 'bbp_participant');			
			}   
			
		}

		
		//remove cron job
		// $timestamp = wp_next_scheduled( 'update_subscription_status_cron_hook' );
		wp_unschedule_event( time() + 3600, 'update_subscription_status_cron_hook' );
   		wp_unschedule_event( time() + 3600, 'update_payout_status_cron_hook' );

	}

}
