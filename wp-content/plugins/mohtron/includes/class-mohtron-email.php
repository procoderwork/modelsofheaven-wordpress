<?php


class MohtronEmail {
    /**
     * Array or String of emails where to send
     * @var mixed
     */
    protected $emails;
    /**
     * Subject of email
     * @var string
     */
    protected $subject;
    /**
     * Associative Array of dynamic data
     * @var array
     */
    protected $dynamicData = array();
    /**
     * Template used to send data
     * @var string
     */
    protected $template;
    /**
     * Prepared template with real data instead of placeholders
     * @var string
     */
    protected $outputTemplate;

    /**
     * Prepared template with real data instead of placeholders
     * @var string
     */
    protected $outputBodyTemplate;
    
    public function __construct($emails, $subject, $dynamicData, $template ){
		$this->emails = $emails;
		$this->subject = $subject;
		$this->dynamicData = $dynamicData;
		$this->template = $template;
		$this->prepareBodyTemplate();
		$this->prepareTemplate();
        $this->send();
    }

    private function replaceVars($template, $toreplace ){
        

        foreach ($toreplace as $placeholder => $value) {
			// Ensure that the placeholder will be in uppercase
			// $securePlaceholder = strtoupper( $placeholder );
			$securePlaceholder =  $placeholder;
			// Placeholder used in our template
			$preparedPlaceholder = "{{" . $securePlaceholder . "}}";
			// Template with real data
            $template = str_replace( $preparedPlaceholder, $value, $template );

        }        

        return $template;
    }
    
    /*
    *Prepare email body
    */    
    private function prepareBodyTemplate(){

        $this->outputBodyTemplate  = $this->replaceVars( get_option( $this->template ) ,$this->dynamicData );
    
    }

    /*
    *Prepare final email template 
    */
    private function prepareTemplate(){

        // $template = $this->getTemplate();
        $_wrapper = get_option( 'email_template_wrapper' );
        $fullTemplate =  $this->replaceVars( $_wrapper, ['email_body' => $this->outputBodyTemplate]  );
        $this->outputTemplate = $fullTemplate;    

    }
    
    /*
    *Send email
    */
	private function send(){
            
        // pr($this->outputTemplate);
        // pr($this->emails);
        // pr($this->subject,1);
		wp_mail( $this->emails, $this->subject, $this->outputTemplate );
	}
    
}