<?php
require MOHTRON_PLUGIN_DIR . '/typerocket/init.php';
require MOHTRON_PLUGIN_DIR . '/includes/mohtron-hooks.php';
require MOHTRON_PLUGIN_DIR . '/includes/mohtron-buddypress-hooks.php';
require MOHTRON_PLUGIN_DIR . '/includes/class-mohtron-crud.php';

//add admin notice if mohtron settings not updated
add_action( 'admin_notices', 'sample_admin_notice__error',100 );
function sample_admin_notice__error() {
    $mohtronSettings = get_option('mohtron_settings');
    // pr($mohtronSettings['mohtron_currency'] );
    if(  empty( $mohtronSettings['mohtron_currency'] ) || empty( $mohtronSettings['blogger_percentage'] ) || empty( $mohtronSettings['moh_percentage'] ) ){
        
        $class = 'notice notice-error';
        $message = __( 'Please update MOHTRON plugin settings before enabling any payments.', 'mohtron' );

        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
    
    }
}



add_action( 'typerocket_loaded', function() {
  
    require_once MOHTRON_PLUGIN_DIR . '/includes/mohtron-typerocket.php';

});

// global $l10n;
// pr($l10n);


//use TypeRocket on the front-end
tr_frontend();

/*******************************************
 * Enqueue Scripts nad Styles
*******************************************/
add_action( 'wp_enqueue_scripts', 'mohtron_scripts_styles',100);
function mohtron_scripts_styles() {
    
    wp_enqueue_style( 'mohtron-style', plugins_url( '/assets/css/mohtron-style.css', dirname(__FILE__) ) );
    wp_enqueue_script( 'mohtron-script',  plugins_url( '/assets/js/mohtron-script.php', dirname(__FILE__) ), array(), '1.0.0', true );    

    wp_enqueue_style( 'slim-style', plugins_url( '/assets/css/slim.min.css', dirname(__FILE__) ) );
    wp_enqueue_script( 'slim-js',  plugins_url( '/assets/js/slim.jquery.min.js', dirname(__FILE__) ), array('jquery') );
   
    // wp_enqueue_script( 'dropzone',  plugins_url( '/assets/js/dropzone.js', dirname(__FILE__) ), array('jquery') );

}


add_action( 'admin_enqueue_scripts', 'mohtron_admin_scripts',100 );
function mohtron_admin_scripts() {
    wp_enqueue_style( 'mohtron-admin-style', plugins_url( '/assets/css/mohtron-admin-style.css', dirname(__FILE__) ) );
    wp_enqueue_script( 'mohtron-admin-script',  plugins_url( '/assets/js/mohtron-admin-script.php', dirname(__FILE__) ), array(), '1.0.0', true );
}


/*
* After succesful logut redirect user to homepage
*/
add_action('wp_logout', 'send_to_home_after_logout');
function send_to_home_after_logout() {
    wp_safe_redirect(get_site_url());
    exit;
}


/*************************************************
* Change default user registration role to visitor
*************************************************/
add_filter('pre_option_default_role', function($default_role){
    
     
    return 'bbp_spectator'; // visitor role
    //return $default_role; // This allows default
});


// change page template for plugin pages
add_filter( 'page_template', 'mohtron_page_template' );
function mohtron_page_template( $page_template )
{
    $currentUserRole = bbp_get_user_role(get_current_user_id());

    if ( is_page( 'subscriptions' ) ) {

        if ( $currentUserRole == 'bbp_participant' ){
            wp_safe_redirect(get_site_url());
            exit;
        }

        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-subscriptions.php';
    }

    if ( is_page( 'moh-registration' ) ) {
        
        if ( $currentUserRole == 'bbp_participant' ){
            wp_safe_redirect(get_site_url());
            exit;
        }

        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-moh-registration.php';
    }

    if ( is_page( 'moh-payments' ) ) {

        if ( $currentUserRole == 'bbp_participant' ){
            wp_safe_redirect(get_site_url());
            exit;
        }

        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-moh-payments.php';
    }

    if ( is_page( 'moh-success' ) ) {

        if ( $currentUserRole == 'bbp_participant' ){
            wp_safe_redirect(get_site_url());
            exit;
        }

        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-successful-payment.php';
    }

    if ( is_page( 'moh-login' ) ) {
        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-moh-login.php';
    }

    if ( is_page( 'moh-register' ) ) {
        $page_template = MOHTRON_PLUGIN_DIR . '/templates/template-moh-register.php';
    }


    return $page_template;
}




/***********************************************
 * Subscription and MOH-registration Page Rewite
***********************************************/
add_action('init', 'custom_rewrite_rule', 10, 0);
function custom_rewrite_rule() {   

    $subscriptions = get_page_by_title('Subscriptions');
    add_rewrite_rule('^subscriptions/([^/]*)/?','index.php?page_id='.$subscriptions->ID.'&bname=$matches[1]','top');

    $mohregistration = get_page_by_title('MOH-Registration');
    add_rewrite_rule('^moh-registration/([^/]*)/([^/]*)/?$','index.php?page_id='.$mohregistration->ID.'&bname=$matches[1]&packageid=$matches[2]','top');

    $mohpayments = get_page_by_title('MOH-Payments');
    add_rewrite_rule('^moh-payments/([^/]*)/([^/]*)/?$','index.php?page_id='.$mohpayments->ID.'&bname=$matches[1]&packageid=$matches[2]','top');  



    //redirect visitor or supporter to homepage if accessing any supporter profile
    if( bp_is_member() ){
        $profileRole = bbp_get_user_role( bp_displayed_user_id() );
        $userRole = bbp_get_user_role(get_current_user_id());       
        if( ( "bbp_keymaster" != $userRole )  &&  ( "bbp_supporter" == $profileRole || "bbp_spectator" == $profileRole ) && !bp_is_my_profile() )
        {          
            wp_safe_redirect(get_site_url());
            exit;
        }

    }


}


add_action('init', 'custom_rewrite_tag', 10, 0);
function custom_rewrite_tag() {
    // $bname = get_query_var('bname');
    add_rewrite_tag('%bname%', '([^&]+)');
    add_rewrite_tag('%packageid%', '([^&]+)');
}


function getBloggerSubscriptionUrl($bloggerSlug = '', $packageID = ''){

    // $subscriptions = get_page_by_title('subscriptions');
    // $url = home_url('page_id='.$subscriptions->ID.'&bname='.$bloggerSlug);
    
    if( '' !== $bloggerSlug && '' == $packageID){

        $url = home_url('/subscriptions/'.$bloggerSlug);

    }else if( '' !== $bloggerSlug && '' !== $packageID){

        $url = home_url('/moh-registration/'.$bloggerSlug.'/'.$packageID);

    }

    return $url;

}


add_action('template_redirect' ,'mohtron_redirection_for_pages' );
function mohtron_redirection_for_pages(){

        $bname = get_query_var('bname');
        $pid = get_query_var('packageid');
        
        if( is_page('activity')){

            wp_redirect( home_url() );
            exit();
        }
        
        // if (strpos($_SERVER['REQUEST_URI'], "subscriptions") !== false){            
            
        //     if( '' !== $bname &&  '' == $pid  ){               
        //         wp_redirect( home_url('/subscriptions/'. $bname)  );
        //         exit;
        //     }
        // }

        // if (strpos($_SERVER['REQUEST_URI'], "moh-registration") !== false ){         
           
        //     if( '' !== $bname &&  '' !== $pid  ){
        //         wp_redirect( home_url('/moh-registration/'.$bname.'/'.$pid)  );
        //         exit;   
        //     }
        // }

        // if (strpos($_SERVER['REQUEST_URI'], "moh-payments") !== false ){         
           
        //     if( '' !== $bname &&  '' !== $pid  ){
        //         wp_redirect( home_url('/moh-payments/'.$bname.'/'.$pid)  );
        //         exit;   
        //     }
        // }
}

/*****************
*Update Role Names
******************/
add_filter( 'bbp_get_dynamic_roles', 'my_bbp_custom_role_names',1);
function my_bbp_custom_role_names(){
 return array(
		 
		// Keymaster
		bbp_get_keymaster_role() => array(
			'name'         => __( 'Keymaster', 'bbpress' ),
			'capabilities' => bbp_get_caps_for_role( bbp_get_keymaster_role() )
		),

		// Moderator
		bbp_get_moderator_role() => array(
			'name'         => __( 'Moderator', 'bbpress' ),
			'capabilities' => bbp_get_caps_for_role( bbp_get_moderator_role() )
		),

		// Participant changed to Blogger
		bbp_get_participant_role() => array(
			'name'         => __( 'Blogger', 'bbpress' ),
			'capabilities' => bbp_get_caps_for_role( bbp_get_participant_role() )
		),

		// Spectator changed to Visitor
		bbp_get_spectator_role() => array(
			'name'         => __( 'Visitor', 'bbpress' ),
			'capabilities' => bbp_get_caps_for_role( bbp_get_spectator_role() )
		),

		// Blocked
		bbp_get_blocked_role() => array(
			'name'         => __( 'Blocked', 'bbpress' ),
			'capabilities' => bbp_get_caps_for_role( bbp_get_blocked_role() )
		)
        
       
    );
}

/*****************
*Add custom roles
*****************/
add_filter( 'bbp_get_dynamic_roles', 'add_new_roles', 1 );
function add_new_roles( $bbp_roles ){
    /* Add a role called Supporter */
    $bbp_roles['bbp_supporter'] = array(
        'name' => 'Supporter',
        'capabilities' => custom_capabilities( 'bbp_supporter' )
        );
 
    return $bbp_roles;
} 

add_filter( 'bbp_get_caps_for_role', 'add_role_caps_filter', 10, 2 );
function add_role_caps_filter( $caps, $role ){
    /* Only filter for roles we are interested in! */
    if( $role == 'bbp_supporter' )
        $caps = custom_capabilities( $role );
 
    return $caps;
}

 
function custom_capabilities( $role ){

    switch ( $role )
    {
 
        /* Capabilities for 'Supporter' role */
        case 'bbp_supporter':
            return array(
                // Primary caps
                'spectate'              => true,
                'participate'           => true,
                'moderate'              => false,
                'throttle'              => false,
                'view_trash'            => false,
 
                // Forum caps
                'publish_forums'        => false,
                'edit_forums'           => false,
                'edit_others_forums'    => false,
                'delete_forums'         => false,
                'delete_others_forums'  => false,
                'read_private_forums'   => false,
                'read_hidden_forums'    => false,
 
                // Topic caps
                'publish_topics'        => false,
                'edit_topics'           => false,
                'edit_others_topics'    => false,
                'delete_topics'         => false,
                'delete_others_topics'  => false,
                'read_private_topics'   => false,
 
                // Reply caps
                'publish_replies'       => true,
                'edit_replies'          => false,
                'edit_others_replies'   => false,
                'delete_replies'        => false,
                'delete_others_replies' => false,
                'read_private_replies'  => false,
 
                // Topic tag caps
                'manage_topic_tags'     => false,
                'edit_topic_tags'       => false,
                'delete_topic_tags'     => false,
                'assign_topic_tags'     => false,
            );
 
            break;
 
        default :
            return $role;
    }
}
/*********************************END add custom roles*********************************/


/*****************************
*Remove tabs from proflie page
******************************/
add_action('bp_init','remove_tabs',100);
function remove_tabs(){
   
    $currentUserRole = bbp_get_user_role(get_current_user_id()) ;
    $displayedUserRole = bbp_get_user_role(bp_displayed_user_id());

    
    //roles array 
	$roles = array("bbp_spectator","bbp_subscriber","bbp_participant","bbp_supporter", "bbp_blogger", "bbp_visitor","bbp_keymaster","bbp_moderator");
	
    if ( in_array( $currentUserRole, $roles) ): 

		$tabs = array('blogs','forums','articles','media');
		foreach($tabs as $tab){
            
			bp_core_remove_nav_item($tab);
        }

        //remove earnings tab if loggedin user is not blogger and  hide on other profile pages
        if (  !is_user_logged_in() 
                || 
                ( bp_is_my_profile() && in_array( $currentUserRole,['bbp_spectator','bbp_visitor','bbp_supporter']) ) 
                    || 
                    ( is_user_logged_in() && !bp_is_my_profile() && in_array( $displayedUserRole,['bbp_spectator','bbp_visitor','bbp_supporter','bbp_participant','bbp_keymaster']) && !is_super_admin() )
            ):
            
            bp_core_remove_nav_item('earnings');
                     
            // bp_core_remove_nav_item( 'activity' );
        endif;


        // remove my_subscriptions tab if not supporter profile
        if (  !is_user_logged_in()
                || 
                ( bp_is_my_profile() && in_array( $currentUserRole,['bbp_participant']) )
                    || 
                    ( is_user_logged_in() && !bp_is_my_profile() && in_array( $displayedUserRole,['','bbp_participant','bbp_visitor','bbp_supporter','bbp_spectator','bbp_keymaster']) )
            ):

            bp_core_remove_nav_item('my-subscriptions');

        endif;

        //remove activity tab if supporter
        // if ( in_array( $currentUserRole,['bbp_visitor','bbp_spectator','bbp_supporter']) ): 
        //     bp_core_remove_nav_item( 'activity' );
        // endif;

        //remove sub-nav
		bp_core_remove_subnav_item(  'activity',  'mentions'); 
		bp_core_remove_subnav_item(  'activity',  'favorites'); 

    endif;
    
}



// add_action( 'bp_core_setup_globals', 'set_default_component' );
function set_default_component () {

    $currentUserRole = bbp_get_user_role(get_current_user_id()) ;

    //pr($currentUserRole);

	// if (   in_array( $currentUserRole,['bbp_visitor','bbp_spectator','bbp_supporter'])) {
    //     echo "1";
    //     define ( 'BP_DEFAULT_COMPONENT', 'profile' );

	// } else {
    //     echo "2";
    //     define ( 'BP_DEFAULT_COMPONENT', 'activity' );

    // }

    // define ( 'BP_DEFAULT_COMPONENT', 'profile' );
    
    // echo BP_DEFAULT_COMPONENT;
    // die();

}


/*********************************END Remove Tabs***************************************/



/*******************
*Rename Subnav items
********************/
add_action( 'bp_setup_nav', 'rename_subnav_item', 100 );
function rename_subnav_item() {	
	
	//Rename 'Personal' sub-nav item to 'All Posts'
    buddypress()->members->nav->edit_nav( array(
        'name' => 'All Posts',
    ), 'just-me', 'activity' );
}
/**************************END Subnav items*****************************************/



/*********************
*Override BP templates
**********************/

// register the location of the plugin templates
function bp_tol_register_template_location() {
    return MOHTRON_PLUGIN_DIR . '/buddypress/';
}
 
 
// replace member-header.php with the template overload from the plugin
function bp_replace_template( $templates, $slug, $name ) {

    // pr($templates);
    // pr($slug);
    // pr($name);
    
    switch ($slug) {
        case 'members/single/activity':           
            return array( 'members/single/bp-activity.php' );
            break;
        // case 'members/single/profile':           
        //     return array( 'members/single/profile-loop.php' );
        //     break;
        // case 'activity/activity-loop':            
        //     return array( 'activity/activity-loop' );
        //     break;
        // case 'activity/entry':            
        //     return array( 'activity/entry' );    
        //     break;
        default:            
            return $templates;
    }

}
 
add_action( 'bp_init', 'bp_tol_start' );
function bp_tol_start() {
     
    if( function_exists( 'bp_register_template_stack' ) )
        bp_register_template_stack( 'bp_tol_register_template_location' );
     
    // if viewing a member page, overload the template
    if ( bp_is_user()  ) 
        add_filter( 'bp_get_template_part', 'bp_replace_template', 10, 3 );
     
}


/*************************END Override BP templates*************************************/




/********************************************
*Add new Earnings tab to blogger profile page
********************************************/
add_action( 'bp_setup_nav', 'tab_earnings' );
function tab_earnings() {
    global $bp;

    bp_core_new_nav_item( array( 
          'name' => 'Earnings', 
          'slug' => 'earnings', 
          'screen_function' => 'earnings_screen', 
          'position' => 1000,
          'parent_url'      => bp_loggedin_user_domain() . '/earnings/',
          'parent_slug'     => $bp->profile->slug,
          'default_subnav_slug' => 'earnings'
    ) );
}


function earnings_screen() {
    
    // Add title and content here - last is to call the members plugin.php template.
    // add_action( 'bp_template_title', 'earnings_title' );
    add_action( 'bp_template_content', 'earnings_content' );
    bp_core_load_template( 'buddypress/members/single/plugins' );
}

function earnings_title() {
    echo __('Earnings',MOHTRON_TEXT);
}

function earnings_content() {
   
    require_once MOHTRON_PLUGIN_DIR . '/templates/earnings.php';
}
/******************************END Earnings***********************************************/




/******************************************************
*Add new My Subscriptions tab to supporter profile page
******************************************************/
add_action( 'bp_setup_nav', 'tab_my_subscriptions' );
function tab_my_subscriptions() {
    global $bp;

    bp_core_new_nav_item( array( 
          'name' => 'My Subscriptions', 
          'slug' => 'my-subscriptions',
          'screen_function' => 'my_subscriptions_screen', 
          'position' => 1000,
          'parent_url'      => bp_loggedin_user_domain() . '/my-subscriptions/',
          'parent_slug'     => $bp->profile->slug,
          'default_subnav_slug' => 'my-subscriptions'
    ) );
}


function my_subscriptions_screen() {
    
    // Add title and content here - last is to call the members plugin.php template.
    add_action( 'bp_template_title', 'my_subscriptions_title' );
    add_action( 'bp_template_content', 'my_subscriptions_content' );
    bp_core_load_template( 'buddypress/members/single/plugins' );
}

function my_subscriptions_title() {
    echo __('My Subscriptions',MOHTRON_TEXT);
}

function my_subscriptions_content() {
   
    require_once MOHTRON_PLUGIN_DIR . '/templates/my-subscriptions.php';
}




/******************************END My Subscriptions***********************************************/




/*****************
*Post Visibility
*****************/
//add post visibility dropdown with activity id
function postVisibility($activityid = -1)
{

    ob_start();

    $status = getPostVisibility($activityid);

    ?>
    <div id="mohtron-post-visibility">
        <!-- <h3>Visibility</h3> -->
        <Select name="post-visibility-status[<?php echo trim($activityid); ?>]" data-act-id="<?php echo trim($activityid); ?>" onchange="getval(this);">
            <option default value="public" <?php if($status == 'public')echo "selected";?>><?php echo __('Public',MOHTRON_TEXT); ?></option>
            <option value="private" <?php if($status == 'private')echo "selected";?>><?php echo __('Private',MOHTRON_TEXT); ?></option>
        </Select>       
        <div class="visi-state"></div>    
    </div>
    


    <?php

    $html = ob_get_contents();

    ob_clean();

    return $html;
} 

//set activity visibility 
add_action( 'bp_activity_posted_update', 'setPostVisibility', 10, 3 );
function setPostVisibility( $content, $user_id, $activity_id  ) 
{
    // if( isset($_POST['slim']) ){

    //     $imageObj = [];
    //     foreach($_POST['slim'] as $index=>$img){

    //         if( !empty($img) )
    //            $imageObj[$index] = json_decode( stripslashes($img) )->output;
    //     }       
    //     mohtron_add_media_to_post( $content, $user_id, $activity_id , (object)$imageObj);        
    // }   
   

   if(isset($_POST['post-visibility-status'][-1])):
        $add_visibility = new Mohtron_CRUD; 
        $result =  $add_visibility->mohtron_post_visibility_insert( sanitize_text_field($_POST['post-visibility-status'][-1]), $activity_id , $user_id);
   endif;
}

//update post visibility
add_action( 'wp_ajax_updatePostVisibility', 'updatePostVisibility' );
function updatePostVisibility()
{
//    pr($_POST,1);
   if(!empty($_POST['postid'])):
        $mohrtron_crud = new Mohtron_CRUD;
        
        $status = $mohrtron_crud->mohtron_get_post_visibility($_POST['postid'], get_current_user_id());        
        
        if( '' == $status ){ //if post visibility record does not exists
            
            $response['result'] =  $mohrtron_crud->mohtron_post_visibility_insert( sanitize_text_field($_POST['visibility']), $_POST['postid'], get_current_user_id());
            
        }else{ //if post visibility record exists

            $response['result'] =  $mohrtron_crud->mohtron_post_visibility_update( sanitize_text_field($_POST['visibility']), $_POST['postid'], get_current_user_id());
           
        }

        if( $response['result'] ){
            $response['msg']= "Visibility updated to ".$_POST['visibility'];
        }else{
            $response['msg'] = "Nothing updated";
        }

        
        echo json_encode($response);
   endif;

   wp_die();
}
 
//check post visibilty 
function getPostVisibility($postid)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'mohtron_post_visibility';
    $postStatus = $wpdb->get_row( $wpdb->prepare( "SELECT visibility FROM $table_name WHERE postid = %d ", $postid )  );
    // pr($postStatus->visibility,1);
    return $postStatus->visibility;
}

//delete post visibility record
add_action( 'bp_activity_deleted_activities', 'remove_activity_visibility' );
function remove_activity_visibility($postid){
    
    $remove_visibility = new Mohtron_CRUD;
    $result =  $remove_visibility->mohtron_post_visibility_delete(  $postid );
}


//check if user is subscribed to blogger
function checkUserSubscriptionStatus( $blogger_id = undefined , $user_id = undefined){

    if ( !isset($blogger_id) )$blogger_id = bp_displayed_user_id();
    if ( !isset($user_id) ) $user_id = get_current_user_id();

     global $wpdb;
     $table_name = $wpdb->prefix . 'mohtron_map_subscriber';
     $status = $wpdb->get_row( $wpdb->prepare( "SELECT subscription_status FROM  $table_name WHERE blogger_id = %d AND supporter_id = %d", $blogger_id,$user_id )  );
     
    //  pr($wpdb->last_query,1);
    //  pr($status,1);

    return $status->subscription_status;
}

/* *******************************END Post Visibility **************************************/



//if user is logged in and role is visitor then upgrade to supporter and add purchased package to their list
add_action( 'wp_ajax_mohtron_subscribe', 'mohtron_subscribe' );
add_action( 'wp_ajax_nopriv_mohtron_subscribe', 'mohtron_subscribe' );
function mohtron_subscribe()
{

    $user_id = get_current_user_id();

    //if blogger id is not set;
    if( empty($_POST['bname']) ){
        echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
        wp_die();
    }else{
       
        $bloggerObj = get_user_by( 'slug',  $_POST['bname']);        
        
        //if blogger id is not found
        if( !$bloggerObj || empty($bloggerObj) ){
            echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an active angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
            wp_die();
        }else{

            $blogger_id = $bloggerObj->ID;
        }
        
    }

    
     //if package id is not set;
    if( empty($_POST['packageid']) ){
        echo json_encode( array( 'success'=>false, 'msg'=>__("Please select a package!",MOHTRON_TEXT) ,  'redirect'=>get_site_url().'/subscriptions' ));
        wp_die();
    }else{
        $package_id = $_POST['packageid'];
    }

    // check if user is admin
    if(is_super_admin()){
        echo json_encode( array( 'success'=>false, 'msg'=>__('Admin cannot subscribe! Login with visitor or supporter role!',MOHTRON_TEXT), 'redirect'=>get_site_url()));
        wp_die();
    }


    //check if user is already subscribed
    if( checkUserSubscriptionStatus($blogger_id ) == "active"){
        echo json_encode( array( 'success'=>false, 'msg'=>__('You are already subscribed to this Angel. Select any other angel.',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels'));
        wp_die();
    }   

    echo json_encode(array( 'success'=>true, 
                            'msg'=>__('Redirecting to subscription payment page.',MOHTRON_TEXT), 
                            // 'redirect'=>get_site_url().'/moh-payments?bname='.$_POST['bname'].'&packageid='.$package_id 
                            'redirect'=>get_site_url().'/moh-payments/'.$_POST['bname'].'/'.$package_id
                        ));

    wp_die();
}


/*************************************************************
*register visitor after selecting package against a blogger id
**************************************************************/
add_action( 'wp_ajax_reg_visi', 'register_visitor' );
add_action( 'wp_ajax_nopriv_reg_visi', 'register_visitor' );
function register_visitor(){
    
    

    //if blogger id is not set;
    if(!$_POST['bname']){    
        echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
        wp_die();       
    }else{
        $bloggerObj = get_user_by( 'slug',  $_POST['bname']);
         //if blogger id is not found
         if( !$bloggerObj || empty($bloggerObj) ){
            echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an active angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
            wp_die();
        }else{

            $blogger_id = $bloggerObj->ID;
        }
    }

    //if package id is not set;
    if(!$_POST['packageid']){    
        echo json_encode( array( 'success'=>false, 'msg'=>__("Please select a package!",MOHTRON_TEXT),  'redirect'=>get_site_url().'/subscriptions' ));
        wp_die();       
    }else{
        $package_id = $_POST['packageid'];
    }
    
    
    //make array of user data;
    $data = array();
    parse_str($_POST['data'], $data);   
    $errors = array();
    
    //if password not matching or terms not selected
    if( empty($data['fname'])){
        array_push( $errors, __("First name cannot be empty!",MOHTRON_TEXT) );        
    }
    if( $data['password1'] != $data['password2'] ){
        array_push($errors, __("Passwords not matching!",MOHTRON_TEXT));        
    }
    if( !$data['tnc']  ){
        array_push($errors, __("Please check Terms and Conditions!",MOHTRON_TEXT));       
    }
    if( ! is_email($data['email'])  ){
        array_push($errors, __("Please enter a valid email !", MOHTRON_TEXT));        
    }

    if(!empty($errors)){
        echo json_encode( array( 'success'=>false, 'msg'=>$errors ));
        wp_die(); 
    }

    
    $usermeta = array( 
                        'user_pass'=> sanitize_text_field($data['password1']),
                        'user_login'=> sanitize_text_field(strtolower($data['fname'])),
                        'user_nicename'=> sanitize_text_field($data['fname']),
                        'user_email'=>  sanitize_email($data['email']),
                        'first_name'=> sanitize_text_field($data['fname']),
                        'last_name'=> sanitize_text_field($data['lname']),
                        'role' => 'bbp_supporter'
    );

    $user_id = wp_insert_user($usermeta);
    //if any error while creating user;
    if( is_wp_error( $user_id ) ) {              
        echo json_encode( array( 'success'=>false, 'msg'=>$user_id->get_error_message()) );
        wp_die(); 
    }

    
    //Login user after registration;
    $creds = array(
        'user_login'    => $data['email'] ,
        'user_password' => $data['password1'] ,
        'remember'      => true
    );     
    $user = wp_signon( $creds, false );

    // pr($user,1);
    
    if ( is_wp_error( $user ) ) {        
        echo json_encode( array( 'success'=>false, 'msg'=>$user->get_error_message() ) );
        wp_die();
    }else{
        // wp_set_current_user($user->ID,$user->user_login);
        wp_set_auth_cookie($user->ID);
        // do_action( 'wp_login', $user->user_login );
    }

        
    
    $packageDetails = mohtron_get_packages($package_id);

    // echo json_encode(array('success'=>true, 'msg'=>'You are now registered with Models of Heaven.', 'redirect'=>get_site_url().'/members/'.$_POST['bname']  ));
    echo json_encode(array( 'success'=>true, 
                            'msg'=> __('You are now registered. Please make subscription payment.',MOHTRON_TEXT),
                            'redirect'=>get_site_url().'/moh-payments?bname='.$_POST['bname'].'&packageid='.$package_id  ));
    wp_die();
}


/*
*login user and add package against the selected blogger id
*/
add_action( 'wp_ajax_sub_reg_user', 'subscribe_registered_user' );
add_action( 'wp_ajax_nopriv_sub_reg_user', 'subscribe_registered_user' );
function subscribe_registered_user(){

         //if blogger id is not set;
        if(!$_POST['bname']){
            echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels'));
            wp_die();
        }else{
            $bloggerObj = get_user_by( 'slug',  $_POST['bname']);
            //if blogger id is not found
            if( !$bloggerObj || empty($bloggerObj) ){
                echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an active angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
                wp_die();
            }else{

                $blogger_id = $bloggerObj->ID;
            }
        }

        //if package id is not set;
        if(!$_POST['packageid']){
            echo json_encode( array( 'success'=>false, 'msg'=>__("Please select a package!",MOHTRON_TEXT),  'redirect'=>get_site_url().'/subscriptions' ));
            wp_die();
        }else{
            $package_id = $_POST['packageid'];
        }

        //make array of user data;
        $data = array();
        parse_str($_POST['data'], $data);

        //check if login data is not empty
        if( !empty($data['log']) &&  !empty($data['pwd']) ){

            $creds = array(
                'user_login'    => $data['log'] ,
                'user_password' => $data['pwd'] ,
                'remember'      => true
            );     
            $user = wp_signon( $creds, false );          
             
            if ( is_wp_error( $user ) ) {
                echo json_encode( array( 'success'=>false, 'msg'=>$user->get_error_message() ) );
                wp_die();
            }else{
                wp_set_current_user($user->ID,$user->user_login);
                wp_set_auth_cookie($user->ID);
                do_action( 'wp_login', $user->user_login );
            }

        }else{ //tHrow error if login data empty

            echo json_encode( array( 'success'=>false, 'msg'=> __("Login fields cannot be empty!", MOHTRON_TEXT)) );
            wp_die();

        }
      

            
        //check if user is a blogger (as she cannot subscribe so redirect to home)
        if( checkUserRole( 'bbp_participant' , $user->ID ) ){        
            echo json_encode( array( 'success'=>true, 'msg'=> __('Bloggers are not allowed to subscribe other angels.',MOHTRON_TEXT)  , 'redirect'=>get_site_url() ));         
            wp_die();
        }

        //check if user is already subscribed
        if( checkUserSubscriptionStatus($blogger_id) == "active"){        
            echo json_encode( array( 'success'=>false, 'msg'=> __('You are already subscribed to this Angel. Select any other angel.', MOHTRON_TEXT)  , 'redirect'=>get_site_url().'/angels' ));         
            wp_die();
        }

        echo json_encode( array( 'success'=>true, 
                                 'msg'=> __('Logged in. Redirecting to subscription payment page.', MOHTRON_TEXT),
                                //  'redirect'=>get_site_url().'/moh-payments?bname='.$_POST['bname'].'&packageid='.$package_id 
                                'redirect'=>get_site_url().'/moh-payments/'.$_POST['bname'].'/'.$package_id 

                         ));
                         
        
        wp_die();
       
}


add_action( 'wp_ajax_chksub', 'check_user_subscription' );
// add_action( 'wp_ajax_nopriv_add_sub', 'map_subscriber_insert_order_details' );
function check_user_subscription(){

    $mohtron_crud = new Mohtron_CRUD;
    $subs = $mohtron_crud->mohtron_get_user_subscriptions(get_current_user_id());
    

    $bloggerOBJ = get_user_by('slug',$_POST['bname']);

    foreach( $subs as $sub){

        if( $sub['blogger_id'] == $bloggerOBJ->data->ID  && $sub['product_id'] == $_POST['packageid'] ){

            echo json_encode( array('result'=>false, "msg"=> "You are already subscribed to this angel.") );

            wp_die();
        } 
    }    
    echo json_encode( array('result'=>true, "msg"=> "Not subscribed to this angel.") );
    wp_die();

}

/**********************************************
* After successful payment insert order details ,
* map subscriber to blogger
* send user and admin mail
***********************************************/
add_action( 'wp_ajax_add_sub', 'map_subscriber_insert_order_details' );
// add_action( 'wp_ajax_nopriv_add_sub', 'map_subscriber_insert_order_details' );
function map_subscriber_insert_order_details(){ 

        // Get current user object
        $user_id = get_current_user_id();       
        $userObj = get_user_by( 'id', $user_id );

        $bloggerName = $_POST['bname'];

        //if user is not logged in;
        // if($user_id == 0){
        //     echo json_encode( array( 'success'=>false, 'msg'=>"Please login!",  'redirect'=>get_site_url().'/wp-login' ));
        //     wp_die();
        // }

        //if blogger id is not set;
        if( !$bloggerName ){
            echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels'));
            wp_die();
        }else{
            $bloggerObj = get_user_by( 'slug',  $bloggerName);
            //if blogger id is not found
            if( !$bloggerObj || empty($bloggerObj) ){
                echo json_encode( array( 'success'=>false, 'msg'=>__('Please select an active angel which you want to subscribe to !',MOHTRON_TEXT), 'redirect'=>get_site_url().'/angels') );
                wp_die();
            }else{

                $blogger_id = $bloggerObj->ID;
            }
        }

        //if package id is not set;
        if(!$_POST['packageid']){
            echo json_encode( array( 'success'=>false, 'msg'=>__("Please select a package!",MOHTRON_TEXT),  'redirect'=>get_site_url().'/subscriptions' ));
            wp_die();
        }else{
            $package_id = $_POST['packageid'];

            $packageData = mohtron_get_packages($package_id);

        }

        //if package id is not set;
        if(!$_POST['data']){
            echo json_encode( array( 'success'=>false, 'msg'=> __("Payment error occured. Please try again!", MOHTRON_TEXT),  'redirect'=>get_site_url().'/moh-payments/'.$bloggerName.'/'.$package_id ));
            wp_die();
        }else{
            $txnData = $_POST['data'];
        }


        //cross check the package details sent from user 



        // pr($txnData,1);

        $mohtron_crud = new Mohtron_CRUD;
        //insert details of subscription to subscriptions table;
        $subRes = $mohtron_crud->mohtron_insert_subscription(  $user_id,  $blogger_id , $package_id , $txnData );
            

        //map supporter to blogger with selected package;
        $mapRes = $mohtron_crud->mohtron_map_subscriber(  $user_id,  $blogger_id , $package_id , $subRes);


        //insert transaction details in order logs table
        $orderRes = $mohtron_crud->mohtron_insert_paypal_transactions(  $txnData );


        if( $blogger_id == '1'){
            
            $mohOldBalance = $mohtron_crud->mohtron_get_user_account_balance('1');               
                      
            
            // pr($mohOldBalance,1);
            $walletData['moh_data'] = array(
                'user_id' => '1',
                'subscription_id' => $subRes,
                'transaction_id' => $txnData['id'],
                'total_amount' => $txnData['transactions'][0]['amount']['total'],
                'user_amount' => $txnData['transactions'][0]['amount']['total'],
                'create_time' => $txnData['create_time'],
                'percent' => '100',
                'transaction_type' => 'debit',
                'account_balance' =>  $mohOldBalance+ $txnData['transactions'][0]['amount']['total']
            );


        }else{
            
            //distribute amount according to moh-wallet percentage rules
            $walletRule = get_option( 'mohtron_settings' );
            $mohAmount = $txnData['transactions'][0]['amount']['total'] * ( $walletRule['moh_percentage'] / 100 );
            $userAmount = $txnData['transactions'][0]['amount']['total'] * ( $walletRule['blogger_percentage'] / 100 );

            //get account balance from last txn of moh and user
            $mohOldBalance = $mohtron_crud->mohtron_get_user_account_balance('1');
            $userOldBalance = $mohtron_crud->mohtron_get_user_account_balance($blogger_id);

            
            // pr(get_user_by( 'login',get_super_admins()[3]),1);
            $walletData['moh_data'] = array(
                            'user_id' => '1',
                            'subscription_id' => $subRes,
                            'transaction_id' => $txnData['id'],
                            'total_amount' => $txnData['transactions'][0]['amount']['total'],
                            'user_amount' => $mohAmount,
                            'create_time' => $txnData['create_time'],
                            'percent' => $walletRule['moh_percentage'],                        
                            'transaction_type' => 'debit',
                            'account_balance' =>  $mohOldBalance+ $mohAmount
                        );

            $walletData['blogger_data'] = array(
                            'user_id' => $blogger_id,
                            'subscription_id' => $subRes,
                            'transaction_id' => $txnData['id'],
                            'total_amount' => $txnData['transactions'][0]['amount']['total'],
                            'user_amount' => $userAmount,
                            'create_time' => $txnData['create_time'],
                            'percent' => $walletRule['blogger_percentage'],                      
                            'transaction_type' => 'debit',
                            'account_balance' =>  $userOldBalance + $userAmount

                        );                   

        }

        // pr($walletData,1);
        $walletRes = $mohtron_crud->mohtron_insert_user_transactions( $walletData );

        /* register using buddypress (NOTE: it is not returning userid or any success message, might be due to pending activation) */
        // $result = bp_core_signup_user( sanitize_text_field($data['fname']),  sanitize_text_field($data['password1']), sanitize_email($data['email']), $usermeta);     
        // pr($result);

        //if user is having visitor role update it supporter
        if( bbp_get_user_role($user_id) == "bbp_spectator" ){          
           // Remove visitor role
           $userObj->remove_role( 'bbp_spectator' );
           // Add suppoerter role
           $userObj->add_role( 'bbp_supporter' );
        }
    
        //send notification to blogger about new subscriber;

        if ( bp_is_active( 'notifications' ) ) {
            bp_notifications_add_notification( array(
                'user_id'           =>  $blogger_id,
                'item_id'           =>  $user_id,
                'component_name'    => 'supporters',
                'component_action'  => 'added_supporter',
                'date_notified'     => bp_core_current_time(),
                'is_new'            => 1,
            ) );
        }
        
        //post activity to subscriber activity logs
        $bp_activity_args = array(
                                'id'                => false,               // Pass an existing activity ID to update an existing entry.
                                'action'            => '<a href="/members/'.$bloggerObj->data->user_nicename.'">Subscribed to '.$bloggerObj->data->display_name.' under '.$packageData['package_name'].' </a>' ,                     // The activity action - e.g. "Jon Doe posted an update"
                                'content'           => '',                     // Optional: The content of the activity item e.g. "BuddyPress is awesome guys!"
                                'component'         => 'members',              // The name/ID of the component e.g. groups, profile, mycomponent.
                                'type'              => 'activity_update',      // The activity type e.g. activity_update, profile_updated.
                                'primary_link'      => '',                     // Optional: The primary URL for this item in RSS feeds (defaults to activity permalink).
                                'user_id'           => $user_id,               // Optional: The user to record the activity for, can be false if this activity is not for a user.
                                'item_id'           => false,                  // Optional: The ID of the specific item being recorded, e.g. a blog_id.
                                'secondary_item_id' => false,                  // Optional: A second ID used to further filter e.g. a comment_id.
                                'recorded_time'     => bp_core_current_time(), // The GMT time that this activity was recorded.
                                'hide_sitewide'     => false,                  // Should this be hidden on the sitewide activity stream?
                                'is_spam'           => false,                  // Is this activity item to be marked as spam?
                                'error_type'        => 'bool'
                            );  
        $activityID = bp_activity_add( $bp_activity_args );

        //send subscription email 
        $expiry = $mohtron_crud->mohtron_get_user_subscription_expiry( $subRes);

        $emailData = array(
            'user_object' => $userObj,
            'blogger_object' => $bloggerObj,           
            'siteurl' => get_option('siteurl'),
            'sitename' => get_option('blogname'),
            'date' => date_i18n(get_option('date_format')),
            'time' => date_i18n(get_option('time_format')),
            'sub_plan' => 'SUBCRIPTION PLAN '.$packageData['package_name'],
            'sub_expiry' => 'SUBCRIPTION EXPIRY ' . date('d-F-Y',  strtotime($expiry['ends_on']))
        );

        mohtron_send_subscribe_email( $emailData );

        echo json_encode( 
                            array(  'success'=>true,
                                    'msg' => __('You are now subscribed to ',MOHTRON_TEXT).get_user_by( 'id',$blogger_id )->data->user_nicename ,
                                    // 'redirect'=>get_site_url().'/members/'.get_user_by( 'id',$blogger_id )->data->user_nicename )
                                    'redirect' => get_site_url().'/moh-success?bname='.get_user_by( 'id',$blogger_id )->data->user_nicename
                                )
                        );

        wp_die();

}


/*
*Register Subscribe to me widget
*/
function register_mohtron_subscribe_to_blog_widget() {

    register_widget( 'Mohtron_Subscribe_To_Blog_Widget' );

    register_sidebar( array(
        'name' => __( 'Subscribe to blog sidebar', 'mohtron' ),
        'id' => 'subscribe-to-blog',
        'description' => __( 'Drop Subscribe To Blog widget here.', 'mohtron' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

}
// add_action( 'widgets_init', 'register_mohtron_subscribe_to_blog_widget' );




/*******************************
*Add subscribe to Blog shortcode
********************************/
add_shortcode( 'subscribetoblog', 'subscribe_to_blog_widget_shortcode' );
function subscribe_to_blog_widget_shortcode( $atts ) {

    $atts = shortcode_atts( array(
		'angel' => ''
    ), $atts, 'subscribetoblog' );      

    $bloggerid = bp_displayed_user_id();
    $userid = get_current_user_id();
    $displayedUser = get_user_by('id', $bloggerid);

    //check user suscription status
    $status = checkUserSubscriptionStatus(  $bloggerid , $userid );
    
    if($status != 'active'){
       
        $name = $displayedUser->data->user_nicename;

        if( !empty($atts['angel']) ){
            $url = '/subscriptions/'.$atts['angel'];
        }else{
            if($displayedUser->ID){               
                $url = getBloggerSubscriptionUrl($name);
            }
        }
        
        ?>        
        <style>
            #buddypress button#subscribe-me{
                background: #2d2d2d;
                width: 125px;
                height:40px;
                text-align: center;
                color: #f7d138;
                border-radius: 2px;
            }
        </style>
        <h2 class="widgettitle"><?php echo __('Subscribe to blog', MOHTRON_TEXT);?></h2>
        <form action="<?php echo $url; ?>" method="POST">
            <!-- <input type="hidden" name="bid" value="<?php //echo $currentUser->ID;?>"> -->
            <button  id="subscribe-me" type="submit"><?php echo __('Subscribe', MOHTRON_TEXT);?></button>
        </form>

        <?php
    }


  
}


/****************
*Get all Packages
****************/
function mohtron_get_packages( $packageID ='' ){

    global $wpdb;        
    $table_name = $wpdb->prefix . "mohtronpackages";

    if( '' == $packageID){
        $packages = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table_name","" ) ,ARRAY_A  );
    }
    else{
        $packages = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE id = %d",$packageID ) ,ARRAY_A  );       
    }
    
    return $packages;

}



/***************************************************************************************/
/*******************Add Post visibility option to activity edit page in admin***********/
/***************************************************************************************/
add_action( 'bp_activity_admin_edit', 'wpdocs_register_meta_boxes' );
function wpdocs_register_meta_boxes($activity) { 
    add_meta_box( 'mohtron-post-visibility', __( 'Post Visibility', 'mohtron' ), 'admin_post_visibility_callback' ,'','side', 'high',array($activity));
}

function admin_post_visibility_callback($activity){
 
    $status = getPostVisibility($activity->id);
   
    ?>
        <select name="post_visibility" id="post-visibility">
            <option value="public" default <?php if($status == 'public')echo "selected";?>><?php echo __('Public',MOHTRON_TEXT);?></option>
            <option value="private" <?php if($status == 'private')echo "selected";?>><?php echo __('Private',MOHTRON_TEXT);?></option>            
        </select>
    <?php
    
}



/****************************************************************************************/
/********************Update Post visibility from admin***********************************/
/***************************************************************************************/
add_action( 'bp_activity_admin_load', 'add_meta_to_activity', 10);
function add_meta_to_activity(  )
 {

    // pr($_REQUEST,1);
    $visibility = $_REQUEST['post_visibility'];
    $postid = $_REQUEST['aid'];

    if ( isset($visibility) ):

        $update_visibility = new Mohtron_CRUD;
        $update_visibility->mohtron_post_visibility_update( sanitize_text_field($visibility), $postid, get_current_user_id());

    endif;
    
}
    



/***************************************************************************************/
/* *******************************Custom Notification **************************************/
/***************************************************************************************/

//register new notification 
add_filter( 'bp_notifications_get_registered_components', 'mohtron_register_new__notifications' );
function mohtron_register_new__notifications( $component_names = array() ) {

	// Force $component_names to be an array
	if ( ! is_array( $component_names ) ) {
		$component_names = array();
	}

	// Add new components to registered components array
    array_push( $component_names, 'supporters','payout' );

	return $component_names;
}



add_filter( 'bp_notifications_get_notifications_for_user', 'new_supporter_notifications', 10, 7 );
// function new_supporter_notifications(  $action, $item_id, $secondary_item_id, $total_items, $format = 'string') {
function new_supporter_notifications(   $content, $item_id, $secondary_item_id, $total_items, $format = 'string', $action, $component ) {

    $mohtron_crud = new Mohtron_CRUD;
    // pr( $content);
    // pr( $format);
    // pr( $total_items);
    // pr( $component);
    // pr( $item_id);
    // pr( $secondary_item_id);
    // pr( $action);

    // New custom notifications
	if ( 'added_supporter' === $action ) {

        $supporter = get_user_by( 'id',$item_id );
	
		// $custom_title = $supporter->data->display_name . ' subscribed to your blog';
		$custom_title = __('A new supporter subscribed to your blog !', MOHTRON_TEXT);
		$custom_link  = '/members/'.$supporter->data->user_nicename; 
		$custom_text = __('A new supporter subscribed to your blog !', MOHTRON_TEXT);

		// WordPress Toolbar
		if ( 'string' === $format ) {
			$return = apply_filters( 'custom_filter', '<a  title="' . esc_attr( $custom_title ) . '">' . esc_html( $custom_text ) . '</a>', $custom_text, $custom_link );

		// Deprecated BuddyBar
		} else {
			$return = apply_filters( 'custom_filter', array(
				'text' => $custom_text,
				'link' => $custom_link
			), $custom_link, (int) $total_items, $custom_text, $custom_title );
		}		
		return $return;	
    }
    

    
	if ( 'payout_status_pending' === $action ) {
	
        $bloggerOBJ = get_user_by( 'id',get_current_user_id() );

        $txnData = $mohtron_crud->mohtron_get_user_transaction( $item_id );
        	
		// $custom_title = $supporter->data->display_name . ' subscribed to your blog';
		$custom_title =  sprintf( __( 'Payout of amount %d DKK is under processing.', MOHTRON_TEXT ), $txnData['total_amount'] );
		$custom_link  = ''; 
        $custom_text =  sprintf( __( 'Payout of amount %d DKK is under processing.', MOHTRON_TEXT ), $txnData['total_amount'] );
        
       

		// WordPress Toolbar
		if ( 'string' === $format ) {
			$return = apply_filters( 'custom_filter', '<a  title="' . esc_attr( $custom_title ) . '">' . esc_html( $custom_text ) . '</a>', $custom_text);
        
		// Deprecated BuddyBar
		} else {
			$return = apply_filters( 'custom_filter', array(
				'text' => $custom_text,
				'link' => $custom_link
            ), $custom_link, (int) $total_items, $custom_text, $custom_title );
            
        }		
        
		return $return;	
	}
    
	if ( 'payout_status_success' === $action ) {
	
        // $bloggerOBJ = get_user_by( 'id',get_current_user_id() );
        // $txnData = $mohtron_crud->mohtron_get_user_transaction( $item_id );        	
		
		$custom_title =  sprintf( __( 'Payout of amount %d DKK has been processed. Please check your paypal account.', MOHTRON_TEXT ), $item_id );
		$custom_link  = ''; 
        $custom_text =  sprintf( __( 'Payout of amount %d DKK has been processed. Please check your paypal account.', MOHTRON_TEXT ), $item_id);
        
       

		// WordPress Toolbar
		if ( 'string' === $format ) {            
			$return = apply_filters( 'custom_filter', '<a  title="' . esc_attr( $custom_title ) . '">' . esc_html( $custom_text ) . '</a>', $custom_text);
        
		// Deprecated BuddyBar
		} else {
			$return = apply_filters( 'custom_filter', array(
				'text' => $custom_text,
				'link' => $custom_link
            ), $custom_link, (int) $total_items, $custom_text, $custom_title );
            
        }		
        
		return $return;	
	}
	
	
}




/**********************************************
* Ajax for blogger payout
***********************************************/
add_action( 'wp_ajax_blogger_payout', 'blogger_payout' );
// add_action( 'wp_ajax_nopriv_blogger_payout', 'blogger_payout' );
function blogger_payout(){

    // update_payout_status();
    // wp_die();

    //make array of user data;
    $data = array();
    parse_str($_POST['data'], $data);

    if( $data['payamount'] == '') {

        echo json_encode( 
            array(  'success'=>false,
                    'msg' => __('You have entered invaild amount! Please enter valid amount for payout.', MOHTRON_TEXT) 
                )
        );
        wp_die();
    }
    
    if( $data['payid'] == '') {

        echo json_encode( 
            array(  'success'=>false,
                    'msg' => __('Invalid paypal ID.', MOHTRON_TEXT)
                )
        );
        wp_die();
    }
    

    $bloggerid = get_current_user_id();    


    $mohtron_crud = new Mohtron_CRUD;
    //get blogger wallet data
    $accBalance = $mohtron_crud->mohtron_get_user_account_balance( $bloggerid );   

    // pr($totalWalletAmount);

    //first cross check if the blogger have the requested amount in her wallet
    if ( $data['payamount'] > $accBalance ){

        echo json_encode( 
            array(  'success'=>false,
                    'msg' =>  __('Your balance is less than minimum payable amount! Please enter valid amount for payout.', MOHTRON_TEXT)
                )
        );
        wp_die();
    }else{

        $response = curl_paypal_payout($data);  //if success then contains status(true/false),msg and inserted user txn record_id

        if( $response['status'] ){       

            echo json_encode( 
                array(  'success'=>true,
                        'msg' => $response['msg'],
                        'amt' =>  $mohtron_crud->mohtron_get_user_account_balance( $bloggerid )
                    )
            );
        }else{

            echo json_encode(
                array(  'success'=>false,
                        'msg' => $response['msg']
                    )
            );
        }

        //notify user about the status of payout
        if ( bp_is_active( 'notifications' ) ) {

           $notification_id =  bp_notifications_add_notification( array(
                'user_id'           =>  $bloggerid,
                'item_id'           =>  $response['record_id'], //row id of user txn table where payout data is stored
                'component_name'    =>  'payout',
                'component_action'  =>  'payout_status_pending',
                'date_notified'     =>  bp_core_current_time(),
                'is_new'            =>  1,
            ) );
        }

        //prepare mail data
        $bloggerObj = get_userdata($bloggerid);
        $payoutData = array(
                        'username' => $bloggerObj->user_login,
                        'useremail' => $bloggerObj->user_email,
                        'siteurl' => get_option('siteurl'),
                        'sitename' => get_option('blogname'),                       
                        'date' => date("D M j, Y G:i:s", strtotime(bp_core_current_time())),
                        'payout_amount' => $data['payamount'],
                        'payout_status' => 'PENDING'
                    );
        //send mail
        mohtron_send_payout_email( $payoutData );


        //update user meta with the provided paypal email 
        update_user_meta( $bloggerid, 'last_used_paypal_email', $data['payid'] );

        //update emails in payout table
        $pdata = array(
                    'user_id' => $bloggerid,
                    'user_transaction_id' => $response['record_id'],
                    'payout_emails' => $data['payid'],
                    'create_time' => bp_core_current_time()
                );
        $mohtron_crud->mohtron_insert_payout_emails($pdata);
        
        wp_die();
    }
    
 

    wp_die();
}


/*********************
*Payout curl operation
**********************/
function curl_paypal_payout( $data ){

    $amount = $data['payamount'];
    $payid = $data['payid'];

    //get paypal envrioment type and its keys
    $paymentsData = get_option('mohtron_payments');
    $currency = get_option('mohtron_settings')['mohtron_currency'];
    
    //check currency
    switch( $currency  ){
        case html_entity_decode('kr'):
            $currency_code = 'DKK';
            break;
            
        case html_entity_decode('&#36;'):
            $currency_code = 'USD';
            break;

        case html_entity_decode('&euro;'):
            $currency_code = 'EUR';
            break;

        case html_entity_decode('&pound;'):
            $currency_code = 'GBP';
            break;
    }

    $env = $paymentsData['environment'];

    if( 'sandbox' == $env ){
        $clientID = trim($paymentsData['sandbox_client_id']);
        $secretKey = trim($paymentsData['sandbox_client_secret_key']);
        $endpoint = "https://api.sandbox.paypal.com";
    }
    if( 'production' == $env ){
        $clientID = trim($paymentsData['production_client_id']);
        $secretKey = trim($paymentsData['production_client_secret_key']);
        $endpoint = "https://api.paypal.com";
    }    
   
    //set unique values for payout batchid and itemid
    $batchid = time();
    $itemid = time();

    $headers = array(
        'Content-Type:application/x-www-form-urlencoded',
        'Cache-Control: no-cache',
        'Authorization: Basic '. base64_encode($clientID.':'.$secretKey) 
    );

    $curl = curl_init();

    //generate access token
    curl_setopt_array($curl, array(
        CURLOPT_URL => $endpoint."/v1/oauth2/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_HTTPHEADER => $headers
    ));
      
    $response = json_decode(curl_exec($curl));
    $err = json_decode(curl_error($curl));


    if( !empty($response) ){

        //if access token is generated
        if( isset($response->access_token) ){


            $requestData = array(
                                    CURLOPT_URL => $endpoint."/v1/payments/payouts",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_POSTFIELDS =>  "{\n  \"sender_batch_header\": {\n    \"sender_batch_id\": \"$batchid\",\n    \"email_subject\": \"You have a payout!\",\n    \"email_message\": \"You have received a payout! Thanks for using our service!\"\n  },\n  \"items\": [\n    {\n      \"recipient_type\": \"EMAIL\",\n      \"amount\": {\n        \"value\": \"$amount\",\n        \"currency\": \"$currency_code\"\n      },\n      \"note\": \"Thanks for your patronage!\",\n      \"sender_item_id\": \"$itemid\",\n      \"receiver\": \"$payid\"\n    }\n  ]\n}",
                                    CURLOPT_HTTPHEADER => array(
                                        "Authorization: Bearer ".$response->access_token,
                                        "Cache-Control: no-cache",
                                        "Content-Type: application/json"
                                    )
                                );
            //request payout
            curl_setopt_array($curl,$requestData);           
            
            $res = json_decode(curl_exec($curl));
            $error = json_decode(curl_error($curl));

            //check http status code for response type 
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);   //https://developer.paypal.com/docs/api/overview/#api-responses

            curl_close($curl);         
            
            // pr($requestData);
            // pr($res,1);

            if ( in_array($httpcode,array('200','201','202','203')) ) {
               
                //check for status of payout request
                if( $res->batch_header->batch_status == "PENDING"){
                    //insert record for pending txn with payout batch id
                    $insertRes = insert_payout_details( $res , $amount );
                    if( $insertRes['status'] ){
                       
                        return array(  'status'=>true,
                                       'msg' => __('Payout is under process. Please check later.', MOHTRON_TEXT),
                                       'record_id' => $insertRes['record_id']
                            );
                    }else{
                       
                        return array(  'status' => false,
                                       'msg' => sprintf( __( 'Please quote this %d Payout batch id to admin and check status of your payout', MOHTRON_TEXT ), $res->batch_header->payout_batch_id ),
                                       'record_id' => $insertRes['record_id']
                                       
                        );
                    }
                }
                
            }else{               
                //handle paypal error
                return array( 'status' => false,
                              'msg' => $res->name == __("VALIDATION_ERROR",MOHTRON_TEXT) ?  __('Invalid paypal ID.', MOHTRON_TEXT) : $res->name
                       );
            }
            
            //if any curl error
            if( !empty($error) ){             
               
                return array( 'status' => false,
                              'msg' => $error
                        );
            }
        }

        // if any identity error for getting accesss token
        if( isset($response->error) ){
           
            return array( 'status'=>false,
                          'msg'=>$response->error_description
                        );
        }

      }

      curl_close($curl);

      //if error in curl request 
      if( !empty($err)){
        
        return array( 'status'=>false,
                        'msg'=>$err
                    );                   

      }   

}

/*
*Insert payout txn record  in database
*/
function insert_payout_details( $res , $amount){
        
        $mohtron_crud = new Mohtron_CRUD;

        $paypalTxnData = array(
                            'id' => $res->batch_header->payout_batch_id,
                            'status' => $res->batch_header->batch_status,
                            'amount' => $amount,
                            'create_time' => date("Y-m-d H:i:s"),
                    );
        
        $userTxnData = array();

        $userBalance = $mohtron_crud->mohtron_get_user_account_balance( get_current_user_id() );

        //check currency
        switch( $res->batch_header->amount->currency ){
            case 'DKK':
                $currency_code = 'kr';
                break;
            case 'USD':
                $currency_code = '&#36;';
                break;
            case 'EUR':
                $currency_code = '&euro;';
                break;
            case 'GBP':
                $currency_code = '&pound;';
                break;
        }

        $userTxnData['user'] = array(
                                    'id' => $res->batch_header->payout_batch_id,
                                    'user_id' => get_current_user_id(),
                                    'total_amount' => $amount,
                                    'user_amount' => $amount,
                                    'account_balance' => $userBalance - $amount,
                                    'transaction_type' => 'credit',
                                    'currency_type' => $currency_code,
                                    'create_time' => date("Y-m-d H:i:s")
                            );

        return  $mohtron_crud->mohtron_insert_payout_status($paypalTxnData , $userTxnData);

}


/*****************************************************
*CRON Scheduling for checking user subscription status
******************************************************/

add_filter( 'cron_schedules', 'add_cron_interval' );
function add_cron_interval( $schedules ) {
    $schedules['minute'] = array(
        'interval' => 60,
        'display'  => esc_html__( 'Every Minute' ),
    );

    // pr($schedules,1);
    return $schedules;
}

#delete
// pr(wp_get_schedules());
// pr(get_option( 'cron' ));
// $mohtroncrud =  new Mohtron_CRUD; 
// pr($mohtroncrud->mohtron_get_package_name(4));
// pr($mohtroncrud->mohtron_get_user_subscriptions(31857),1);
// check_user_subscription_status();
#delete

//check for scheduled cron job
if (! wp_next_scheduled ( 'update_subscription_status_cron_hook' )) {
    wp_schedule_event(time(), 'daily', 'update_subscription_status_cron_hook');
}

add_action( 'update_subscription_status_cron_hook', 'check_user_subscription_status' );
function check_user_subscription_status(){

    $mohtroncrud =  new Mohtron_CRUD; 

    // get all subscribers
    $args = array( 
                   'role__in' => array('bbp_supporter'),
                   'role__not_in' => array('bbp_keymaster'),
                   'orderby' => 'user_nicename',
                   'order'   => 'ASC'
                );
    
    $users = get_users( $args );
  
    
    foreach($users as $user ){
       
        $subscriptionData = $mohtroncrud->mohtron_get_user_subscriptions($user->ID);
        // pr($subscriptionData);
        if ( !empty($subscriptionData) ){             
                     

            foreach($subscriptionData as $subscription ){                    
                   
                    $expiring = new DateTime($subscription['ends_on']);
                    $current = new DateTime();
                    
                    if( $expiring < $current){               

                        //update subscription status and notify user if subscription ends
                        $mohtroncrud->mohtron_update_user_subscription_status($subscription['id']);

                        $emailData = array(
                                        'username' => get_userdata($subscription['supporter_id'])->user_login,
                                        'useremail' => get_userdata($subscription['supporter_id'])->user_email,
                                        'siteurl' => get_option('siteurl'),
                                        'sitename' => get_option('blogname'),
                                        'sub_plan' => $mohtroncrud->mohtron_get_package_name($subscription['product_id'])['package_name'],
                                        'sub_expiry' => date('d-F-Y',  strtotime($subscription['ends_on'])),
                                        'blogger_name' => get_userdata($subscription['blogger_id'])->user_login,
                                        'blogger_email' => get_userdata($subscription['blogger_id'])->user_email                    
                                    );
                                    
                        mohtron_send_subscription_expired_email($emailData);

                    }
            }          

        }

        //after subscription status update, check for user subscriptions list, if not any then update user role to visitor
        $updatedSubscriptionData = $mohtroncrud->mohtron_get_user_subscriptions($user->ID);
        // pr($updatedSubscriptionData);
        if ( empty($updatedSubscriptionData) ){
            // Remove  suppoerter role
            $user->remove_role( 'bbp_supporter' );
            // Add  visitor role
            $user->add_role( 'bbp_spectator' );
        }

    }

    
}



//check for scheduled payout cron job
if (! wp_next_scheduled ( 'update_payout_status_cron_hook' )) {
    wp_schedule_event(time(), 'minute', 'update_payout_status_cron_hook');
}

#delete
// wp_clear_scheduled_hook( 'update_payout_status_cron_hook' ); 
// update_payout_status();
#delete

add_action( 'update_payout_status_cron_hook', 'update_payout_status' );
function update_payout_status(){

    $mohtron_crud =  new Mohtron_CRUD;  

    $pendingPayouts = $mohtron_crud->mohtron_get_pending_payout_records( 'PENDING' );

    // pr($pendingPayouts,1);
   
    if( !empty( $pendingPayouts )){
        //get paypal envrioment type and its keys
        $paymentsData = get_option('mohtron_payments');
        $env = $paymentsData['environment'];
        if( 'sandbox' == $env ){
            $clientID = trim($paymentsData['sandbox_client_id']);
            $secretKey = trim($paymentsData['sandbox_client_secret_key']);
            $endpoint = "https://api.sandbox.paypal.com";
        }
        if( 'production' == $env ){
            $clientID = trim($paymentsData['production_client_id']);
            $secretKey = trim($paymentsData['production_client_secret_key']);
            $endpoint = "https://api.paypal.com";
        }

        $headers = array(
            'Content-Type:application/x-www-form-urlencoded',
            'Cache-Control: no-cache',
            'Authorization: Basic '. base64_encode($clientID.':'.$secretKey)
        );

        $response = array();
        $err = array();

        $curl = curl_init();

        foreach( $pendingPayouts as $payout ){

            curl_setopt_array($curl, array(
                CURLOPT_URL => $endpoint."/v1/payments/payouts/".$payout['payout_batch_id'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => $headers
            ));

           
            array_push($response,json_decode(curl_exec($curl)));
            array_push($err,json_decode(curl_error($curl)));
        }
       
        // pr($response,1);


        //check for status of payout header and its items
        foreach( $response as $status){

            if( isset($status->name) ){

                $payoutStatus = $status->name;

            }else{
                $bloggerid = $mohtron_crud->mohtron_get_userid_from_usertxns($status->batch_header->payout_batch_id);
                $bloggerObj = get_userdata($bloggerid);

           
                //prepare mail data                               
                $payoutData = array(
                                'username' => $bloggerObj->user_login,
                                'useremail' => $bloggerObj->user_email,
                                'siteurl' => get_option('siteurl'),
                                'sitename' => get_option('blogname'),                       
                                'date' => date("d-F-Y", strtotime(bp_core_current_time())),
                            );
                $payoutStatus = $status->batch_header->batch_status;

            }
            

            switch( $payoutStatus ){

                case "SUCCESS":

                    //check for item status if success
                    //then update txn id and set status as approved
                    //then insert new record with txn id for both moh and blogger (deduct amount from moh account)                                        
                   

                    $result = checkPayoutItemStatus( $status->items[0] , $bloggerid ); //contains current status of payout item           
                    
                    //notify user about the status of payout 
                    if( $result['status'] ){

                        if ( bp_is_active( 'notifications' ) ) {                        

                            $notification_id =  bp_notifications_add_notification( array(
                                'user_id'           =>  $bloggerid,
                                'item_id'           =>  $status->batch_header->amount->value, //amount
                                'component_name'    =>  'payout',
                                'component_action'  =>  'payout_status_success',
                                'date_notified'     =>  bp_core_current_time(),
                                'is_new'            =>  1,
                            ) );

                            // pr($notification_id);
                        }

                        //send mail
                        $payoutData['payout_amount'] = $status->batch_header->amount->value;
                        $payoutData['payout_status'] = 'SUCCESS';
                       
                        mohtron_send_payout_email( $payoutData );

                    }
                    
                    else{
                        
                        $data = array(
                            'payout_batch_id' => $status->batch_header->payout_batch_id,
                            'transaction_id' =>  0,
                            'batch_status' => $status->batch_header->batch_status,
                            'time_completed' => $status->batch_header->time_created,
                            'amount' => $status->batch_header->amount->value,
                        );
                        $result = reversePayout( $data );

                        //send mail
                        $payoutData['payout_amount'] = $status->batch_header->amount->value;
                        $payoutData['payout_status'] = $status->batch_header->batch_status;                             
                        mohtron_send_payout_email( $payoutData );
                    }
                    
                    
                    break;

                case "DENIED":
                    // update status to DENIED  and reverse txn
                    $data = array(
                                'payout_batch_id' => $status->batch_header->payout_batch_id,
                                'transaction_id' =>  0,
                                'batch_status' => $status->batch_header->batch_status,
                                'time_completed' => $status->batch_header->time_created,
                                'amount' => $status->amount->value,
                            );
                    $result = reversePayout( $data );

                     //send mail
                     $payoutData['payout_amount'] = $status->batch_header->amount->value;
                     $payoutData['payout_status'] = $status->batch_header->batch_status;                             
                     mohtron_send_payout_email( $payoutData );

                    break;

                case "PROCESSING":
                    //will check in next cron schedule
                    break;

                case "NEW":
                    //will check in next cron schedule
                    break;

                default:                        
                    // Payout Error
            }
        }
        
        curl_close($curl);

    }
    
}


/*
* check payout status and insert/update txn records
*/
function checkPayoutItemStatus( $item , $bloggerid  ){

    $mohtron_crud =  new Mohtron_CRUD;    
    
    $result = array('status'=>'false','msg'=>'');
    $currency_code = "";

    //check currency
    switch( $item->payout_item->amount->currency ){
        case 'DKK':
            $currency_code = 'kr';
            break;
        case 'USD':
            $currency_code = '$';
            break;
        case 'EUR':
            $currency_code = '&euro;';
            break;
        case 'GBP':
            $currency_code = '&pound;';
            break;
    }

    // pr($item->payout_item->amount->currency);
    // pr($currency_code);


    switch( $item->transaction_status ){
        
        case "SUCCESS":
            //update txn id and set status as approved
            //insert new record with txn id for moh (deduct amount from moh account) and update blogger record

            $userTxnData = array();
            $mohBalance = $mohtron_crud->mohtron_get_user_account_balance(1);
            $userTxnData = array(
                                    'payout_batch_id' => $item->payout_batch_id,
                                    'transaction_id' => $item->transaction_id,
                                    'user_id' => 1,
                                    'total_amount' => $item->payout_item->amount->value,
                                    'user_amount' => $item->payout_item->amount->value,
                                    'account_balance' => $mohBalance - $item->payout_item->amount->value ,
                                    'transaction_type' => 'credit',
                                    'currency_type' => $currency_code,
                                    'create_time' => $item->time_processed,
                                    'transaction_status' => $item->transaction_status
                                );
            // pr($userTxnData ,1);
            $result = $mohtron_crud->mohtron_update_payout_status(  $userTxnData , $bloggerid ); //contains updated record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK is completed. Please check your paypal account.', MOHTRON_TEXT ), $userTxnData['total_amount'] );
                $result['status'] = true;                
            }
            break;

        case "REFUNDED":
            //update user txn record with status REFUNDED and refund the amount deducted earlier
            $data = array(
                        'payout_batch_id' => $item->payout_batch_id,
                        'transaction_id' =>  $item->transaction_id,
                        'batch_status' => $item->transaction_status,
                        'time_completed' => $item->time_processed,
                        'amount' => $item->payout_item->amount->value
                    );
            $result = reversePayout( $data ); //contains inserted record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK has failed. You can reinitiate the payout again.', MOHTRON_TEXT ), $data['amount'] );
                $result['status'] = false; 
            }
            break;

        case "RETURNED":
             //update user txn record with status RETURNED and reverse txn
             $data = array(
                        'payout_batch_id' => $item->payout_batch_id,
                        'transaction_id' =>  $item->transaction_id,
                        'batch_status' => $item->transaction_status,
                        'time_completed' => $item->time_processed,
                        'amount' => $item->payout_item->amount->value
                    );
            $result = reversePayout( $data ); //contains inserted record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK has failed. You can reinitiate the payout again.', MOHTRON_TEXT ), $data['amount'] );
                $result['status'] = false;
            }
            break;
            
       
        case "BLOCKED":
            //update user txn record with status BLOCKED and reverse txn
            $data = array(
                        'payout_batch_id' => $item->payout_batch_id,
                        'transaction_id' =>  $item->transaction_id,
                        'batch_status' => $item->transaction_status,
                        'time_completed' => $item->time_processed,
                        'amount' => $item->payout_item->amount->value
                    );
            $result = reversePayout( $data ); //contains inserted record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK has failed. You can reinitiate the payout again.', MOHTRON_TEXT ), $data['amount'] );
                $result['status'] = false;
            }
            break;

        case "DENIED":
            // update status to DENIED  and reverse txn
            $data = array(
                        'payout_batch_id' => $item->payout_batch_id,
                        'transaction_id' =>  $item->transaction_id,
                        'batch_status' => $item->transaction_status,
                        'time_completed' => $item->time_processed,
                        'amount' => $item->payout_item->amount->value
                    );
            $result = reversePayout( $data ); //contains inserted record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK has failed. You can reinitiate the payout again.', MOHTRON_TEXT ), $data['amount'] );
                $result['status'] = false;
            }
            break;

       
        case "FAILED":
           //update user txn record with status FAILED and refund the amount deducted earlier
           $data = array(
                        'payout_batch_id' => $item->payout_batch_id,
                        'transaction_id' =>  $item->transaction_id,
                        'batch_status' => $item->transaction_status,
                        'time_completed' => $item->time_processed,
                        'amount' => $item->payout_item->amount->value
                    );
            $result = reversePayout( $data ); //contains inserted record id
            if( $result['status']  ){
                $result['msg'] =  sprintf( __( 'Payout of amount %d DKK has failed. You can reinitiate the payout again.', MOHTRON_TEXT ), $data['amount'] );
                $result['status'] = false;
            }
            break;        

        case "UNCLAIMED":
            //will check in next cron schedule (by default if not claimed for 30 days amount will be refunded)
            break;

        case "NEW":
            //will check in next cron schedule
            break;

        case "ONHOLD":
            //will check in next cron schedule
            break;

        case "PENDING":
            //will check in next cron schedule
            break;

        default:                        

    }

    return $result;

}


/*
 *If status denied then Reverse payout amount to blogger and update record as per 
 */
function reversePayout( $status ){

    $mohtron_crud =  new Mohtron_CRUD;

    $batch_id = $status['payout_batch_id'];
    $batch_status = $status['batch_status'];
    $time = $status['time_completed'];
    $amount = $status['amount'];
    $txn_id = $status['transaction_id'];
    $txn_type = $status['transaction_id'];

    return $mohtron_crud->mohtron_reverse_payout(  $batch_id, $txn_id, $batch_status, $time, $amount, $txn_type );


}




/*******************************************************************
* Checks for a user role if supplied as param and return True/False
* otherwise returns roles array 
*******************************************************************/
function checkUserRole( $role="" , $userid= "" ){

    if ( $userid == "" ){

        $userid = get_current_user_id();
    }

    $user_meta = get_userdata($userid);
    $user_roles = $user_meta->roles;

    // pr($user_roles);

    if( "" == $role ){           

        return $roles;

    }else{

        return in_array( $role, $user_roles);
    }


}

function checkUserForumRole( $role="" , $userid= "" ){

    if ( $userid == "" )
        $userid = get_current_user_id();

    if( bbp_get_user_role($userid) == $role)
        return true;
    else
        return false;

}





/**********************************************
 *Add media upload button to activity post form
 **********************************************/
function mohtronUploadMedia(){

    //get number of images settings value
    // $imgCount = 10;
    ?>   
    <style>                
        /* Activity post images */
        #buddypress .activity-inner .row.posted img {
            display: block;
            width: 100%;
        }

        #buddypress .activity-inner .row.image-group .slim-image {
            max-height:200px;
            max-width:200px;
            margin-bottom:20px;
        }

        #buddypress button.slim-btn {
            width:auto;
            font-size:1em;
            padding: 0 1em;
            border-radius: 1px;
            background-image: none;
            color: #000;
            background: #999;
            /* filter: invert(100%); */
        }
       
    </style>  
  
    <div class="row image-group">
        <div class="col-sm-2 slim-image"><input type="file" name="slim[]" class="myCropper" /></div>
        <div class="col-sm-2 slim-image"><input type="file" name="slim[]" class="myCropper" /></div>
        <a class="col-sm-2 admedia" href="" id="add-media"></a>
    </div>    
    <div class="alert alert-info" id="media-limit" style="display:none;"><?php echo __('Maximum 10 images allowed.', MOHTRON_TEXT); ?></div>
    <div class="ajax-loader" style="display:none;"></div>
    <script>
    
        $( ()=>{       
            var imgData = [];
            var cropper = $(".myCropper").slim({
                            ratio: 'free',                            
                            service : '',
		                    push    : true,                                                                           
                            download: false,                           
                            label: '<?php echo __('Add image here.',MOHTRON_TEXT);?>',
                            buttonEditLabel : "Edit"
                    });

            $('#add-media').on('click',(e)=>{
                e.preventDefault();
                // console.log(cropper.slim('dataBase64')[0].output.image)
                if( $('.image-group').children().length === 11 ){

                    $('#add-media').hide();
                    $('#media-limit').show();   
                    return false;
                }else{
                    $('<div class="col-sm-2 slim-image"><input type="file" class="myCropper"/></div>').insertBefore('.col-sm-2.admedia');
                    cropper  =  $(".myCropper").slim({
                        // ratio: 'free',
                        service: '',
                        push    : true,                                          
                        download: false,                            
                        label: '<?php echo __('Add image here.',MOHTRON_TEXT);?>',
                        buttonEditLabel : "Edit"                          
                    });

                }               
            });

            //reset slim after post is submitted
            $('#aw-whats-new-submit').on('click', ()=>{

                    $('.ajax-loader').show();

                    cropper.slim('destroy');
                    // console.log($('.slim-image'));
                    var slims = $('.slim-image');
                    $.each( slims, (index,value)=>{
                        if(index > 1){$(value).remove(); }
                    });
                    cropper = $(".myCropper").slim({
                            ratio: 'free',                            
                            service : '',
		                    push    : true,                                                                           
                            download: false,                           
                            label: '<?php echo __('Add image here.',MOHTRON_TEXT);?>',
                            buttonEditLabel : "Edit"
                    });                    
                    
            });
            
            //add ajax loader on post activty click
            $( document ).ajaxComplete(function( event, xhr, settings ) {
                
                if( undefined != settings.data){
                    //parse query string to check if action is post_update
                    var qstring = settings.data;
                    var querystring = qstring.substring(qstring.indexOf('?')+1).split('&');
                    var params = {}, pair, d = decodeURIComponent, i;              
                    for (i = querystring.length; i > 0;) {
                        pair = querystring[--i].split('=');
                        params[d(pair[0])] = d(pair[1]);
                    }

                    if( params.action == 'post_update'){
                        
                        $('.ajax-loader').hide();
                    }
                }               

            });           

        });
    
    </script>
    <?php
}



/*
* function to upload images and attach them to current activity 
*
* @param  content
* @param  user id
* @param  activity id
* @param  image object
*/
function mohtron_add_media_to_post( $content, $user_id, $activity_id = "" , $imageObj){
    
    //Call WP Helper
    if ( !function_exists('wp_handle_upload') ) require_once( ABSPATH . 'wp-admin/includes/file.php' );

    $uploadedImages = [];
    $imgURL = [];
    
    foreach( $imageObj as $index=>$data){            
          
            //Base64 (We have to takeout the string "data:image/png;base64," from the output data)
            $img = $data->image;
            $img = str_replace('data:' . $data->type . ';base64,', '', $img);
            $img = str_replace(' ', '+', $img);
            $decoded = base64_decode($img);
            //File name
            $type = str_replace('image/', '',$data->type);
            $filename = $activity_id . "-" . md5(date('Y-m-d-H-i-s')) . "." . $data->name;
            // $filename = $data->name;
            //Upload
            $upload_dir = wp_upload_dir();
            $upload_path = $upload_dir['path'] . $filename;
            $image_upload = file_put_contents($upload_path, $decoded);

            //WP safety (required)
            $upload_overrides = array( 'test_form' => false );
            //File
            $file             = array();
            $file['error']    = '';
            $file['tmp_name'] = $upload_path;
            $file['name']     = $filename;
            $file['type']     = $data->type;
            $file['size']     = filesize( $upload_path );
            //Try
            $uploadedImages[$index] = wp_handle_sideload( $file, $upload_overrides );

            $imgURL[$index] = $uploadedImages[$index]['url'];
    }

    $newContent  = '';
    $newContent  = '<div class="row posted">';
     
    if( !empty($imgURL)){        
        foreach( $imgURL as $index=>$url){
            $newContent =  $newContent  . '<div class="col-sm-4 posted-image"><a href="'.$url.'"><img src="'.$url.'" /></a></div>';          
        }
        $content =  $newContent."</div>".$content;
    }  

    return $content;
    
    
}

  

add_action( 'bp_activity_before_save', 'activity_before_save', 10, 1 ); 
// define the bp_activity_before_save callback 
function activity_before_save( $array ) {

    if( isset($_POST['slim']) ){       

        $imageObj = [];
        foreach($_POST['slim'] as $index=>$img){

            if( !empty($img) )
               $imageObj[$index] = json_decode( stripslashes($img) )->output;
        }       
       $content = mohtron_add_media_to_post( $array->content, $array->user_id, $activity_id , (object)$imageObj);    
       $array->content = $content;

       return $array;
    }   


    return $array;
    
}; 
      



/*
* override activity template to full page
*/

add_action('wp_head', 'activity_page_sidebar', 100);
function activity_page_sidebar(){
    if(bp_is_activity_component()){
        kleo_switch_layout('full', 100);
    }
}


 

/*
*Filter activites on basis of subscription
*/
function mohtron_filter_activity( $a, $activities ) {	
	
	if ( is_super_admin() )	
		return $activities;	
	
	foreach ( $activities->activities as $key => $activity ) { 
       

        if( ( !is_user_logged_in() && getPostVisibility($activity->id) == "private" ) //for random site visitor
				|| 
					( !bp_is_my_profile() && getPostVisibility($activity->id) == "private" && (checkUserSubscriptionStatus() == '' || checkUserSubscriptionStatus() == 'inactive' ) ) //for visitor role
						||
						  	( is_user_logged_in() && !bp_is_my_profile() && getPostVisibility($activity->id) == "private" && (checkUserSubscriptionStatus() == '' || checkUserSubscriptionStatus() == 'inactive' ) &&  ( checkUserRole('bbp_participant') || checkUserRole('bbp_supporter')) ) //for not subscribed blogger and Supporter
        )
        {
            $activity->content  = '<div class="private-content"><img src="https://via.placeholder.com/790x100?text=Private+Content"></div>';
        }

	}		
   
	return $activities;
	
}
add_action( 'bp_has_activities', 'mohtron_filter_activity', 10, 2 );





//test api credentials in payments settings
add_action( 'wp_ajax_mohtron_api_key_test', 'test_paypal_api_crdentials' );
function test_paypal_api_crdentials(){

    $env = trim($_POST['env']);
    $clientID = trim($_POST['clientid']);
    $secretKey = trim($_POST['secretkey']);

    if($env == 'production'){
        $endpoint = "https://api.paypal.com";
    }
    else if( $env == 'sandbox'){
        $endpoint = "https://api.sandbox.paypal.com";
    }
    else{
        echo  json_encode(array( 'status'=>false,'msg'=>'Environment error!'));
        wp_die();
    }

    $headers = array(
        'Content-Type:application/x-www-form-urlencoded',
        'Cache-Control: no-cache',
        'Authorization: Basic '. base64_encode($clientID.':'.$secretKey)
    );

    $curl = curl_init();

    //generate access token
    curl_setopt_array($curl, array(
        CURLOPT_URL => $endpoint."/v1/oauth2/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "grant_type=client_credentials",
        CURLOPT_HTTPHEADER => $headers
    ));
      
    $response = json_decode(curl_exec($curl));
    $err = json_decode(curl_error($curl));

    // if any identity error for getting accesss token
    if( isset($response->error) ){           
        echo  json_encode(array( 'status'=>false,'msg'=>$response->error_description ) );
        wp_die();
    }else{
        echo  json_encode(array( 'status'=>true,'msg'=>$response->access_token));
        wp_die();
    }

    echo  json_encode(array( 'status'=>false,'msg'=>"Some error occured. Please try again!" ) );
    wp_die();

}


// get user id from username in wallet listing in admin
function get_wallet_user($username){       

    $users = new WP_User_Query( array (  'search'         => '*'.esc_attr( $username ).'*',
                                         'search_columns' => array(
                                                                    'user_login',
                                                                    'user_nicename',
                                                                    'user_email',
                                                                    'user_url', 
                                         ),
                                         'role__in' => array('bbp_participant','bbp-spectator')
                                      )
                            );
    
    $users_found = $users->get_results();

    $users_id = array();

    foreach( $users_found as $key=>$val ){
        array_push($users_id , $val->ID);
    }

    return $users_id;
    

}