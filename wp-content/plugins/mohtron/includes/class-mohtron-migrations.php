<?php
class Mohtron_Migrations 
{   
    public $db_prefix = "mohtron_";

	/** Class constructor */
	public function __construct() {   

        $this->mohtron_post_visibility_table_install();
        $this->mohtron_map_subscriber_table_install();
        $this->mohtron_subscriptions_table_install();
        // $this-> mohtron_packages_table_install();
        $this->mohtron_paypal_transactions_table_install();
        $this->mohtron_user_transactions_table_install();
        $this->mohtron_payout_email_table_install();
        // $this-> mohtron_paypal_transactions_table_install();
        
    }

    
    //Add post visibility table 
    public function mohtron_post_visibility_table_install() 
    {

        global $wpdb;

        $table_name = $wpdb->prefix . $this->db_prefix . "post_visibility";

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name  (
            id int NOT NULL AUTO_INCREMENT,            
            postid int NOT NULL UNIQUE,            
            blogger_id int NOT NULL,            
            visibility varchar(20) DEFAULT 'public' NOT NULL,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }



    //Add map_supporter table
    public function mohtron_map_subscriber_table_install() 
    {

        global $wpdb;              
        
        $charset_collate = $wpdb->get_charset_collate();

        $table_name = $wpdb->prefix. $this->db_prefix . "map_subscriber";

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,            
            supporter_id int NOT NULL ,            
            blogger_id int NOT NULL,
            product_id int NOT NULL,
            subscription_status varchar(20) NOT NULL,
            subscription_id int NOT NULL,
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }


  
    //Add subscriptions  table
    public function mohtron_subscriptions_table_install() 
    {

        global $wpdb;              
        
        $charset_collate = $wpdb->get_charset_collate();

        $table_name = $wpdb->prefix . $this->db_prefix . "subscriptions";

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,            
            supporter_id int NOT NULL ,            
            blogger_id int NOT NULL,
            product_id int NOT NULL,
            transaction_id varchar(50) NOT NULL,
            starts_on datetime NOT NULL,
            ends_on datetime NOT NULL,
            transaction_amount decimal(10, 2) NOT NULL,
            created_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
  
    //Add packages  table
    public function mohtron_packages_table_install() 
    {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        // $table_name = $wpdb->prefix . $this->db_prefix . "packages";
        $table_name = $wpdb->prefix .  "mohtronpackages";
    

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,            
            package_name varchar(50) NOT NULL,         
            package_description  varchar(50) NOT NULL,  
            package_confirmation  varchar(50) NOT NULL,  
            billing_details  varchar(50) NOT NULL,  
            expiration  varchar(50) NOT NULL,      
            allow_signups  varchar(10)  NOT NULL,      
            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        // pr($sql,1);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
  
    //Add paypal transactions table
    public function mohtron_paypal_transactions_table_install() 
    {

        global $wpdb;              
        
        $charset_collate = $wpdb->get_charset_collate();

        $table_name =  $wpdb->prefix . $this->db_prefix . "paypal_transactions";

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,
            transaction_id varchar(50) DEFAULT '0' NOT NULL ,
            payout_batch_id varchar(50)  DEFAULT '0' NOT NULL ,
            transaction_status varchar(10) NOT NULL,
            transaction_amount  decimal(10,2) NOT NULL,           
            create_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        // pr($sql,1);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }


  
    //Add user transactions table
    public function mohtron_user_transactions_table_install()
    {

        global $wpdb;              
        
        $charset_collate = $wpdb->get_charset_collate();

        $table_name =  $wpdb->prefix . $this->db_prefix . "user_transactions"; 

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,
            subscription_id varchar(50) DEFAULT 0 NOT NULL,
            user_id  int NOT NULL,
            total_amount decimal(10, 2) DEFAULT 0 NOT NULL,
            user_amount decimal(10, 2) DEFAULT 0 NOT NULL,                       
            percent decimal(10, 2) DEFAULT 0 NOT NULL,
            account_balance decimal(10, 2) DEFAULT 0 NOT NULL,            
            transaction_id varchar(50) DEFAULT 0 NOT NULL ,
            payout_batch_id varchar(50)  DEFAULT 0 NOT NULL ,
            transaction_type varchar(50) NOT NULL,
            currency_type varchar(50) DEFAULT '$' NOT NULL,
            create_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        // pr($sql,1);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }


    //Add user transactions table
    public function mohtron_payout_email_table_install()
    {
        global $wpdb;              
        
        $charset_collate = $wpdb->get_charset_collate();

        $table_name =  $wpdb->prefix . $this->db_prefix . "payout_emails"; 

        $sql = "CREATE TABLE  $table_name (
            id int NOT NULL AUTO_INCREMENT,
            user_id  int NOT NULL,
            user_transaction_id varchar(10)  DEFAULT 0 NOT NULL,
            payout_emails varchar(100)  DEFAULT 0 NOT NULL,
            create_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
            PRIMARY KEY  (id)
        ) $charset_collate;";

        // pr($sql,1);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
    }
  
   

}

$migrations  = new Mohtron_Migrations;