<?php
class Mohtron_CRUD
{

    private $db_prefix;

	/** Class constructor */
	public function __construct() {
            
        $this->db_prefix = 'mohtron_' ;   
      
    }


   //Insert activity visibility
   public function mohtron_post_visibility_insert( $visibility, $activityid , $blogger_id=-1) 
   {

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "post_visibility";

        
        if (!empty($activityid) && !empty($visibility)):
      
            $result = $wpdb->insert( 
                $table_name, 
                array( 
                    'time' => current_time( 'mysql' ), 
                    'visibility' => $visibility, 
                    'postid' => $activityid,
                    'blogger_id' => $blogger_id
                )                   
            ); 
            return  $result;
        endif;

        return "Sorry some error occured";
       
    } 

   //Fetch activity visibility
   public function mohtron_get_post_visibility($activityid , $blogger_id=-1) 
   {
       
        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "post_visibility";

        $visibility = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE postid = %d AND blogger_id= %d", $activityid ,$blogger_id ) ,ARRAY_A  );

        // pr($wpdb->last_query);
        
       return $visibility;
    } 	

   //Update activity visibility
   public function mohtron_post_visibility_update( $visibility, $activityid ,$blogger_id=-1) 
   {

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "post_visibility";

        if (!empty($activityid) && !empty($visibility)):
            $result = $wpdb->update( 
                $table_name, 
                array( 
                    'time' => current_time( 'mysql' ), 
                    'visibility' => $visibility, 
                    'postid' => $activityid,
                    'blogger_id' => $blogger_id
                ),
                array(
                    'postid' => $activityid
                ) 
            );              
            return  $result;
        endif;

        return "Sorry some error occured";
       
    } 	

    
    //remove visibility record
    public function mohtron_post_visibility_delete($activityid) 
    {
        global $wpdb;   
        
        $table_name = $wpdb->prefix . $this->db_prefix . "post_visibility";

        if( isset($activityid) ):
           $result = $wpdb->query( $wpdb->prepare("DELETE FROM $table_name WHERE postid = %d ", $activityid) );
           return  $result;
        endif; 
        
        return "Sorry some error occured";
        
    } 


    //map subscriber to a blogger 
    public function mohtron_map_subscriber( $subscriberid , $bloggerid, $productid, $subscriptionid){

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "map_subscriber";

        
        if (!empty($productid) && !empty($bloggerid)):
      
            $result = $wpdb->insert( 
                $table_name, 
                array( 
                    'time' => current_time( 'mysql' ), 
                    'supporter_id' => $subscriberid, 
                    'product_id' => $productid,
                    'blogger_id' => $bloggerid,
                    'subscription_status' => 'active',
                    'subscription_id' => $subscriptionid,

                )                   
            ); 

            // pr($wpdb->last_query,1);
            return  $result;
        endif;
        
        return "Sorry some error occured";
    }


    //Insert Supporter's subscription details in Subscriptions table
    public function mohtron_insert_subscription( $subscriberid , $bloggerid, $packageid , $transactiondata){

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "subscriptions";

        //get package details
        $package_table =  $wpdb->prefix . "mohtronpackages";
        $package = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $package_table WHERE id = $packageid" ) ,ARRAY_A  );
        
        // pr($package['expiration'],1);

        $start = current_time( 'mysql' );
        // pr($start);       
                

        $end = strtotime("+".$package['expiration']." Days");   //strtotime("+1 week 2 days 4 hours 2 seconds"), "\n";
        $end =  date('Y-m-d H:i:s', $end);

        // pr($orderid,1);
        
        // if (!empty($orderid) ):      
            $result = $wpdb->insert( 
                $table_name, 
                array( 
                    'created_time' => current_time( 'mysql' ), 
                    'supporter_id' => $subscriberid,
                    'product_id' => $packageid,
                    'blogger_id' => $bloggerid,
                    'transaction_id' => $transactiondata['id'],
                    'starts_on' => $start,
                    'ends_on' => $end,
                    'transaction_amount' => $transactiondata['transactions'][0]['amount']['total']                    
                )                   
            );             
            return  $wpdb->insert_id;
        // endif;
        
        return "Sorry some error occured";
    }

    



    //Insert paypal transaction details
    public function mohtron_insert_paypal_transactions(  $txndata){

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "paypal_transactions";
        
            $result = $wpdb->insert(
                $table_name,
                array(
                    'transaction_id' => $txndata['id'],
                    'transaction_status' => $txndata['state'],
                    'transaction_amount' => $txndata['transactions'][0]['amount']['total'],                    
                    'create_time' => $txndata['create_time']
                )                   
            );         
            // pr($wpdb->last_query,1);    
            return  $result;
        
        
        // return "Sorry some error occured";
    }


    //Insert user_transactions details
    public function mohtron_insert_user_transactions(  $walletdata ){ 

        global $wpdb;
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";

        // pr($walletdata);
        
        foreach($walletdata as $data):

            $wpdb->insert(
                $table_name,
                array(
                    'transaction_id' => $data['transaction_id'],
                    'user_id' => $data['user_id'],                  
                    'subscription_id' =>  $data['subscription_id'],               
                    'total_amount' => $data['total_amount'],
                    'user_amount' => $data['user_amount'],
                    'percent' => $data['percent'],                  
                    'account_balance' =>$data['account_balance'],                   
                    'transaction_type' => $data['transaction_type'],
                    'currency_type' => get_option('mohtron_settings')['mohtron_currency'],
                    'create_time' => $data['create_time']
                )
            );
        endforeach;
        // pr($wpdb->last_query,1);
        return  $wpdb->insert_id;        
        
       
    }

        
    //Get blogger account balance
    public function mohtron_get_user_account_balance( $userid ){

        global $wpdb;

        //get earned amount
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";
        $txn =  $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE  user_id = %d  ORDER BY id DESC LIMIT 1" , $userid) ,ARRAY_A  );
        
        $accBalance = $txn['account_balance'];

        return $accBalance;
       
    }


    
    //Get blogger earnings data
    public function mohtron_get_user_transactions( $userid ){

        global $wpdb;
        
        //get subscriptions against this blogger
        $subs_table = $wpdb->prefix . $this->db_prefix . "subscriptions";
        $payout_emails_table = $wpdb->prefix . $this->db_prefix . "payout_emails";
        $subscriptions = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $subs_table WHERE  blogger_id = %d" , $userid) ,ARRAY_A  );

        // pr($subscriptions,1);

        //get earned amount
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";
        $result = [];
        
        // foreach($subscriptions as $subscription):
            // foreach( $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table_name WHERE  subscription_id = %d AND  user_id = %d" , $subscription['id'],$userid) ,ARRAY_A  ) as $data ):
            foreach( $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table_name WHERE  user_id = %d" , $userid) ,ARRAY_A  ) as $data ):

                // array_push($result, array('supporter_id'=>$subscription['supporter_id'],'user_amount'=>$data['user_amount'],'create_time'=>$subscription['starts_on']));
                // array_push($data, array('supporter_id'=>$subscription['supporter_id']));           
                // pr($data['id']);
                $payoutEmail = $wpdb->get_row( $wpdb->prepare( "SELECT payout_emails FROM  $payout_emails_table WHERE  user_transaction_id = %d" , $data['id']) ,ARRAY_A  );
                if ( !empty($payoutEmail) ){

                    $data['payout_email'] = $payoutEmail['payout_emails'];
                }
                $subscriptionData = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM  $subs_table WHERE  id = %d" , $data['subscription_id']) ,ARRAY_A  );
                // pr( $subscriptionData,1 );
                if( !empty ($subscriptionData) ){ 
                    $data['package_name'] = $this->mohtron_get_package_name($subscriptionData['product_id'])['package_name']; //get package name
                    $data['supporter_id'] = $subscriptionData['supporter_id']; //add supporter to data array
                }
                array_push($result, $data);
            endforeach;
        // endforeach;
            // pr($result,1);
        return  $result;
       
    }
    
    //Get blogger's single transcation record
    public function mohtron_get_user_transaction( $record_id ){

        global $wpdb;
       
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";       
       
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE  id = %d AND  user_id = %d" , $record_id ,get_current_user_id()) ,ARRAY_A  );
       
        return  $result;
       
    }


    //Get blogger's id using payout batch id
    public function mohtron_get_userid_from_usertxns( $payout_batch_id ){

        global $wpdb;
       
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";
       
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT user_id FROM $table_name WHERE payout_batch_id = %s" , $payout_batch_id) ,ARRAY_A  );
       
        return  $result['user_id'];
       
    }
    

    //getSubscriber
    public function mohtron_get_subscriber( $subscriptionid ){

        global $wpdb;
        $table_name = $wpdb->prefix . $this->db_prefix . "subscriptions";
        
        $result = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE  id = %d " , $subscriptionid) ,ARRAY_A  );
        // pr($wpdb->last_query);
        return  $result;
       
    }
    
    

    //get user subscriptions
    public function mohtron_get_user_subscriptions( $userid ){

        global $wpdb;

        //get active subscriptions array
        $table_name = $wpdb->prefix . $this->db_prefix . "map_subscriber";
        $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table_name WHERE  supporter_id= %d AND subscription_status = 'active' " , $userid) ,ARRAY_A  );


        //get subscription info
        $table_name = $wpdb->prefix . $this->db_prefix . "subscriptions";
        $results = array();
        foreach($result as $key=>$val){
            $results[$key] = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $table_name WHERE  id= %d " ,$val['subscription_id']) ,ARRAY_A  );
        }
        // pr($results);
        // pr($wpdb->last_query,1);
        return  $results;
    }

    //get user subscription expiry date
    public function mohtron_get_user_subscription_expiry(  $subid ){

        global $wpdb;  

        //get subscription info
        $table_name = $wpdb->prefix . $this->db_prefix . "subscriptions";

        $result = $wpdb->get_row( $wpdb->prepare( "SELECT ends_on FROM $table_name WHERE  id= %d " ,$subid) ,ARRAY_A  );
        
       
        // pr($wpdb->last_query,1);
        return  $result;
    }


    //get package name
    public function mohtron_get_package_name(  $pid ){

        global $wpdb;  

        //get subscription info
        $table_name = $wpdb->prefix . "mohtronpackages";

        $result = $wpdb->get_row( $wpdb->prepare( "SELECT package_name FROM $table_name WHERE  id= %d " ,$pid) ,ARRAY_A  );
        
       
        // pr($wpdb->last_query,1);
        return  $result;
    }


    //update user subscription status
    public function mohtron_update_user_subscription_status( $subscriptionid ){

        global $wpdb;

        //get active subscriptions array
        $table_name = $wpdb->prefix . $this->db_prefix . "map_subscriber";
        $result = $wpdb->update( $table_name,
                                 array(
                                    'subscription_status' => 'inactive'
                                 ),
                                 array( 'ID' => $subscriptionid )
                );

        // pr($results);
        // pr($wpdb->last_query,1);
        return  $results;
    }


    
    //get pending payout record
    public function mohtron_get_pending_payout_records( $status = "PENDING" ){

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "paypal_transactions";        

        $result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $table_name WHERE  transaction_status = %s AND transaction_id = 0" , $status) ,ARRAY_A );
  
        // pr($wpdb->last_query,1);
        return  $result;
        
    }


    
    //insert  pending payout record
    public function mohtron_insert_payout_status( $paypalTxnData , $userTxnData){

        global $wpdb;        
        $table_name = $wpdb->prefix . $this->db_prefix . "paypal_transactions";

        $result = $wpdb->insert(
            $table_name,
            array(                   
                'payout_batch_id' => $paypalTxnData['id'],
                'transaction_status' => $paypalTxnData['status'],
                'transaction_amount' => $paypalTxnData['amount'],                   
                'create_time' => $paypalTxnData['create_time']
            )                   
        );

       
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";
        
        foreach($userTxnData as $data):

            $results['status'] = $wpdb->insert(
                $table_name,
                array(
                    'payout_batch_id' => $data['id'],
                    'user_id' => $data['user_id'],
                    'total_amount' => $data['total_amount'],
                    'user_amount' => $data['user_amount'],
                    'account_balance' =>$data['account_balance'],
                    'transaction_type' => $data['transaction_type'],
                    'currency_type' => get_option('mohtron_settings')['mohtron_currency'],
                    'create_time' => $data['create_time']
                )
            );
        endforeach;      
        
        $results['record_id'] = $wpdb->insert_id;
       
        return  $results;
        
        
    }



    
    
    //add new payout record if payout is processed or declined
    public function mohtron_update_payout_status(  $txnData, $bloggerid ){

        global $wpdb;
        $result = array();
        $table_name = $wpdb->prefix . $this->db_prefix . "paypal_transactions";
           
        $result['status']  = $wpdb->update(
            $table_name,
            array(
                // 'payout_batch_id' => $txnData['payout_batch_id'],
                'transaction_id' => $txnData['transaction_id'],
                'transaction_status' => $txnData['transaction_status'],
                'transaction_amount' => $txnData['total_amount'],
                'create_time' => $txnData['create_time']
            ),
            array(
                'payout_batch_id' => $txnData['payout_batch_id'],
                'transaction_id' => 0,
            )
        );
        // pr($result);
        
       //update blogger txn id 
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";

        if( $result['status'] !== 0 ){
            $result['status'] =  $wpdb->update(
                $table_name,
                array(
                    'transaction_id' => $txnData['transaction_id'],                              
                    // 'transaction_type' => $txnData['transaction_type'],
                    // 'create_time' => $txnData['create_time']
                ),
                array(
                    'payout_batch_id' => $txnData['payout_batch_id'],
                    'user_id' => $bloggerid
                )
            );
            // pr($result);
        }

        // pr($wpdb->last_error);
        // pr($result);
        // pr($wpdb->last_query,1);
        
        //insert moh txn record
        if( $result['status']  ){
            $result['status']  = $wpdb->insert(
                $table_name,
                array(
                    'payout_batch_id' => $txnData['payout_batch_id'],
                    'transaction_id' => $txnData['transaction_id'],
                    'user_id' => 1, 
                    'total_amount' => $txnData['total_amount'],
                    'user_amount' => $txnData['user_amount'],
                    'account_balance' =>$txnData['account_balance'],
                    'transaction_type' => $txnData['transaction_type'],
                    'currency_type' => $txnData['currency_type'],
                    'create_time' => $txnData['create_time']
                )
            );          

        }

        // pr($wpdb->last_error);
        // pr($result);
        // pr($wpdb->last_query,1);

        //get row id of updated record (needed in payout notification)
        if( $result['status']  ){

            $row = $wpdb->get_row( $wpdb->prepare( "SELECT id FROM $table_name WHERE  payout_batch_id = %s AND user_id = %d" , $txnData['payout_batch_id'], get_current_user_id()) ,ARRAY_A  );
            $result['record_id'] = $row['id'];
        }

         
        return  $result;
    }


    
    
    //reverse payout if DENIED or UNCLAIMED
    public function mohtron_reverse_payout( $batch_id, $txn_id, $status, $time, $amount, $txn_type ){
        
        global $wpdb;
        $result = array();

        //update payapl txn record for unsuccessful payout
        $table_name = $wpdb->prefix . $this->db_prefix . "paypal_transactions";
        $result['status']  =  $wpdb->update(
                $table_name,
                array(
                    'transaction_status' => $status,
                    'create_time' => $time
                ),
                array(
                    'payout_batch_id' => $batch_id
                )
            );

        //reverse amount deducted
        $table_name = $wpdb->prefix . $this->db_prefix . "user_transactions";

        $accBalance = $this->mohtron_get_user_account_balance(get_current_user_id());
        
        if( $result['status'] ){

           $result['status'] = $wpdb->insert(
                $table_name,
                array(
                    'payout_batch_id' => $batch_id,
                    'transaction_id' => $txn_id,
                    'user_id' => get_current_user_id(),
                    'total_amount' => $amount,
                    'user_amount' => $amount,
                    'account_balance' => $accBalance + $amount,
                    'transaction_type' => $txn_type,
                    'create_time' => $time
                )
            );
        }

        if( $result['status'] ){
            $result['record_id'] = $wpdb->$wpdb->insert_id;
        }
        return $result; 

    }



    
    //insert payout emails of blogger in payout emails table
    public function mohtron_insert_payout_emails( $payoutdata ){

        global $wpdb;
        $table_name = $wpdb->prefix . $this->db_prefix . "payout_emails";

        // pr($payoutdata);

        $result = $wpdb->insert( $table_name,
                                 array(
                                    'user_id' => $payoutdata['user_id'],
                                    'user_transaction_id' => $payoutdata['user_transaction_id'],
                                    'payout_emails' => $payoutdata['payout_emails'],
                                    'create_time' => $payoutdata['create_time']
                                 )
                );

        // pr($wpdb->last_error);
        // pr($result);
        // pr($wpdb->last_query,1);
                

        return $result;

    }
    


    

}
