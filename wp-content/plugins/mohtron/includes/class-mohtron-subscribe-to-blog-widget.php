<?php
/**
 * Adds Subscribe to Blog widget.
 */
class Mohtron_Subscribe_To_Blog_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mohtron_subscribe_to_blog', // Base ID
			esc_html__( 'Subscribe to blog', 'mohtron' ), // Name
			array( 'description' => esc_html__( 'Subscribe to blog widget for Blogger', 'mohtron' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
        echo $args['before_widget'];
        
			
		if ( ! empty( $instance['show-title-text'] ) ) {
			$showhideTitle = $instance['show-title-text'] ? true : false;
		}
		
		if ( ! empty( $instance['title'] ) && $showhideTitle  ) { 
			
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
        
		if ( ! empty( $instance['button-text'] ) ) {
			$btntxt = $instance['button-text'] ;
		}
        
		if ( ! empty( $instance['if-subscribed-button-text'] ) ) {
			$ifsubbtntxt = $instance['if-subscribed-button-text'] ;
		}

		$bloggerid = bp_displayed_user_id();
		$userid = get_current_user_id();

		//check user suscription status
		$status = checkUserSubscriptionStatus(  $bloggerid , $userid );
		
		//if user is already subscribed
		if($status == 'active'){
			
			?>
			<style>
				#buddypress button#subscribe-me{
					background: #2d2d2d;                
					width: 125px;
					height:40px;
					text-align: center;
					color: #f7d138;
					border-radius: 2px;
				}
			</style> 
			<button  id="subscribe-me" type="button">You are already subscribed</button>
			<?php

		}
		else{

			$currentUser = get_user_by('id', bp_displayed_user_id());
			$name = $currentUser->data->user_nicename;
			if($currentUser->ID){
				// $url = '/subscriptions?bname=' . $name . '&bid=' . $currentUser->ID;
                // $url = '/subscriptions?bname=' . $name ;
                $url = getBloggerSubscriptionUrl($name);
			}else{
				$url = '/angels/?ref=subscribetoblog';
			}
			
			?>
			<style>
				#buddypress button#subscribe-me{
					background: #2d2d2d;                
					width: 125px;
					height:40px;
					text-align: center;
					color: #f7d138;
					border-radius: 2px;
				}
			</style>        
			<form action="<?php echo $url; ?>" method="POST">
				<!-- <input type="hidden" name="bid" value="<?php //echo $currentUser->ID;?>"> -->
				<button  id="subscribe-me" type="submit"><?php echo $btntxt;?></button>
			</form>
	
			<?php
		}

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {  
		$title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Subscribe to blog', 'mohtron' );
		$showhideTitle =  $instance['show-title-text'] ;
		$btntxt = ! empty( $instance['button-text' ] ) ? $instance['button-text' ] : esc_html__( 'Subscribe', 'mohtron' );
		$ifsubbtntxt = ! empty( $instance['if-subscribed-button-text' ] ) ? $instance['if-subscribed-button-text' ] : esc_html__( 'You are already subscribed.', 'mohtron' );
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'mohtron' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'button-text' ) ); ?>"><?php esc_attr_e( 'Text for subscribe button :', 'mohtron' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'button-text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'button-text' ) ); ?>" type="text" value="<?php echo esc_attr( $btntxt); ?>">
		</p>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'if-subscribed-button-text' ) ); ?>"><?php esc_attr_e( 'Text to show if user is already subscribed:', 'mohtron' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'if-subscribed-button-text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'if-subscribed-button-text' ) ); ?>" type="text" value="<?php echo esc_attr( $ifsubbtntxt); ?>">
		</p>

		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'show-title-text' ) ); ?>"><?php esc_attr_e( 'Show title text above button:', 'mohtron' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'show-title-text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show-title-text' ) ); ?>" type="checkbox" <?php checked( $showhideTitle, 'on' ); ?>>
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) { 
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
		$instance['show-title-text'] = $new_instance['show-title-text'] ;
		$instance['button-text'] = ( ! empty( $new_instance['button-text'] ) ) ? sanitize_text_field( $new_instance['button-text'] ) : '';
		$instance['if-subscribed-button-text'] = ( ! empty( $new_instance['if-subscribed-button-text'] ) ) ? sanitize_text_field( $new_instance['if-subscribed-button-text'] ) : '';

		return $instance;
	}

} // class Foo_Widget