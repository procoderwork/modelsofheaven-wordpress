<?php
//------------------------------------------------------------
// Action to render post visibility DD
add_action('mohtron_post_visibility', 'mohtron_post_visibility_dd', 10, 1);
function mohtron_post_visibility_dd($activityid){
    return postVisibility($activityid);
}

//------------------------------------------------------------
// Hook to add visibility dropdown to newly added post.
add_action('bp_activity_entry_meta', 'mohtron_entry_meta_visibility_dd', 1);
function mohtron_entry_meta_visibility_dd(){
    
    if ( bp_is_my_profile() &&  (  checkUserRole('bbp_participant') || checkUserRole('bbp_keymaster'))  ){
        
        echo postVisibility(  bp_get_activity_id() );

    }


}

//------------------------------------------------------------
//Hook to add upload media button
add_action('bp_activity_post_form_options', 'mohtron_media_upload', 1000);
function mohtron_media_upload(){
    // echo mohtronUploadMedia(); 
    // echo postVisibility();

    
}

//------------------------------------------------------------
// Email content types

//Hook to after activties list
add_action('bp_after_member_activity_content', 'mohtron_subscribe_blog_after_activity', 1000);
function mohtron_subscribe_blog_after_activity(){

    //check if blogger has alteast one activty
    // if ( bp_has_activities()->activity_count > 0 ){
        
        // show subscribe blog button
            if (
                ( !is_user_logged_in() 	
                    && 
                    ((!checkUserRole("bbp_spectator",bp_displayed_user_id()) && !checkUserRole("bbp_supporter",bp_displayed_user_id()) ) 
                        ||
                        checkUserRole("bbp_keymaster",bp_displayed_user_id())
                    )
                )
                
                ||			 
                
                ( is_user_logged_in()
                    && 
                    ( checkUserRole("bbp_participant",bp_displayed_user_id()) || checkUserRole("bbp_keymaster",bp_displayed_user_id()) )
                        && 
                        !bp_is_my_profile()
                            && 
                            ( (checkUserSubscriptionStatus() == "inactive") || (checkUserSubscriptionStatus() == "") )
                                && 
                                ( checkUserRole("bbp_spectator") || checkUserRole("bbp_supporter") )
                                    &&
                                    ( !checkUserRole("bbp_keymaster") && !checkUserRole("bbp_participant")  )
                )
            
            )
            {
                // if ( dynamic_sidebar('subscribe-to-blog') ) :
                // else :
                // endif;
                echo do_shortcode('[subscribetoblog]');
            }
    // }
        
}




add_filter ("wp_mail_content_type", "mmohtron_mail_content_type");
function mmohtron_mail_content_type() {
    return "text/html";
}




add_filter( 'bp_after_has_members_parse_args', 'mohtron_filter_bp_members', 100 );
function mohtron_filter_bp_members( $retval ) {
    // $user_ids = get_transient( 'mohtron_blogger_users' );
    $user_ids = false;
    if( false === $user_ids ) {        
        // get all visitors and supporters
        $args = array(         
            'role__not_in' => array('bbp_spectator','bbp_supporter'),
            // 'role__in' => array('bbp_keymaster','administrator','bbp_participant'),
            'role__in' => array('bbp_participant'),
            'fields' => 'ID'
        );
        $displayed_users = "'";
        $user_ids = get_users( $args );
        set_transient( 'mohtron_blogger_users', $user_ids, 1 * HOUR_IN_SECONDS );
    }  
    
    if( $user_ids ) {
        $retval['include'] = $user_ids;
    } 
    return $retval;
}




// add_filter( 'bp_user_query_uid_clauses', 'filtering_memberpress', 10, 2 );
/**
* Filtering only users with an active memebership
* @param $sql array
* @param $query BP_User_Query
* @return array
*/
function filtering_memberpress( $sql, $query ) {
    
    // MemberPress is Active
    if( class_exists( 'MeprDb' ) ) {
        // Get the MemberPress members database object
        $mepr_db = new MeprDb();
        // Add a JOIN to the select
        $sql['select'] .= ' LEFT JOIN ' . $mepr_db->members . ' as members on members.user_id = u.' . $query->uid_name . ' ';
        // Add a WHERE clause
        $sql['where'][] = " members.memberships != ''";
    }

    // pr($sql);
    // pr($query);
    return $sql;
}

// add_action('wp_footer', function(){ global $wpdb;  pr($wpdb->queries); }, 100);