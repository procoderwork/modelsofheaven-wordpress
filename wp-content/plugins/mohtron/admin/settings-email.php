<?php

//----------------------------------------------------------------
//  Save default options
//----------------------------------------------------------------
add_action('wp_head', 'mohtron_reset_email_template', 100);
function mohtron_reset_email_template(){
    //Insert default Tempalete
    $tmpl = get_option('email_template_wrapper');
    if(trim($tmpl) === ''){
        update_option('email_template_wrapper', file_get_contents(MOHTRON_PLUGIN_DIR.'/templates/email/layout.php'));
    }

    //Insert default Contents
    $tmpl2 = get_option('email_template_subscribe');
    if(trim($tmpl2) === ''){
        update_option('email_template_subscribe', file_get_contents(MOHTRON_PLUGIN_DIR.'/templates/email/subscribe.php'));
    }
}

//----------------------------------------------------------------
//  Settings Fields
//----------------------------------------------------------------
add_action( 'admin_init', 'mohtron_email_template_settings' );
function mohtron_email_templates_section() {
    echo '<small>Vars that can be used within these email templates:</small>';
    echo '<ul>';
        echo '<li><strong>User Email</strong> -> {{useremail}}</li>';
        echo '<li><strong>User Name</strong> -> {{username}}</li>';
        echo '<li><strong>Blogger name</strong> -> {{blogger_name}}</li>';
        echo '<li><strong>BLogger email</strong> -> {{blogger_email}}</li>';
        echo '<li><strong>Website URL</strong> -> {{siteurl}}</li>';
        echo '<li><strong>Website Name</strong> -> {{sitename}}</li>';
        echo '<li><strong>Date</strong> -> {{date}}</li>';
        echo '<li><strong>Time</strong> -> {{time}}</li>';
        echo '<li><strong>Subscription Plan</strong> -> {{sub_plan}}</li>';
        echo '<li><strong>Expiry Date</strong> -> {{sub_expiry}}</li>';
        echo '<li><strong>Email Body</strong> -> {{email_body}}</li>';
        echo '<li><strong>Payout amount</strong> -> {{payout_amount}}</li>';
    echo '</ul>';
}

function email_template_wrapper_cb() {
    $content = get_option('email_template_wrapper');
    wp_editor( $content, 'email_template_wrapper' );
}

function email_template_subscribe_cb() {    
    $content = get_option('email_template_subscribe');
    wp_editor( $content, 'email_template_subscribe' );
}

function admin_email_template_subscribe_cb() {    
    $content = get_option('admin_email_template_subscribe');
    wp_editor( $content, 'admin_email_template_subscribe' );
}

function email_template_subscription_expired_cb() {    
    $content = get_option('email_template_subscription_expired');
    wp_editor( $content, 'email_template_subscription_expired' );
}

function admin_email_template_subscription_expired_cb() {    
    $content = get_option('admin_email_template_subscription_expired');
    wp_editor( $content, 'admin_email_template_subscription_expired' );
}


function payout_email_template_cb() {    
    $content = get_option('payout_email_template');
    wp_editor( $content, 'payout_email_template' );
}




// -----------------------------------------------------------
// Add Settings Fields to Settings page
function mohtron_email_template_settings() {
    // Section 
    add_settings_section(
       'email_templates_section',
       'You can edit Email Templates below:',
       'mohtron_email_templates_section',
       'email-templates'
    );
    // Email Header
    add_settings_field(
        'email_template_wrapper',    // Field Id
        'Email body wrapper',        // Field Title
        'email_template_wrapper_cb', // Field Callback to render field
        'email-templates',          // Page to show this field on
        'email_templates_section'   // Section to show this field in
    );
    

    //Subscription Email to subscriber
    add_settings_field(
       'email_template_subscribe',
       'Email body for successfull subscription (User)',
       'email_template_subscribe_cb',
       'email-templates',
       'email_templates_section'
   );

   // Subscription Email to Admin
   add_settings_field(
        'admin_email_template_subscribe',
        'Email Body for successfull subscription (Admin)',
        'admin_email_template_subscribe_cb',
        'email-templates',
        'email_templates_section'
    );

    // User subscription expiry Email to user
    add_settings_field(
       'email_template_subscription_expired',
       'Email body for subscription expiry (User)',
       'email_template_subscription_expired_cb',
       'email-templates',
       'email_templates_section'
   );    

    //User subscription expiry Email to admin
    add_settings_field(
        'admin_email_template_subscription_expired',
        'Email body for subscription expiry (Admin)',
        'admin_email_template_subscription_expired_cb',
        'email-templates',
        'email_templates_section'
    );

    // Payout status  Email
    add_settings_field(
        'payout_email_template',
        'Payout status email body',
        'payout_email_template_cb',
        'email-templates',
        'email_templates_section'
    );
    
    // Value under which we are saving the data by $_POST
    register_setting( 'email-templates', 'email_template_wrapper' );
    register_setting( 'email-templates', 'email_template_subscribe' );    
    register_setting( 'email-templates', 'admin_email_template_subscribe' );
    register_setting( 'email-templates', 'email_template_subscription_expired' );
    register_setting( 'email-templates', 'admin_email_template_subscription_expired' );
    register_setting( 'email-templates', 'payout_email_template' );
}

add_action( 'admin_init', 'mohtron_admin_menu', 99 );
function mohtron_admin_menu(){
    // pr($GLOBALS[ 'menu' ]);
}

// -----------------------------------------------------------
//  Add Admin Menu
function mohtron_email_template_submenu() {
    // pr('mohtron');
    // add_menu_page('MOH Settings', 'MOH Settings', 'manage_options', 'mohtron');
    // add_submenu_page( 'mohtron', 'MOH Settings', 'MOH Settings', 'manage_options', 'mohtron', 'mohtron_settings_page');
    // add_submenu_page( 'mohtron', 'Email Templates', 'Email Templates', 'manage_options', 'email-templates', 'mohtron_email_template_submenu_cb');
    // Add page as a child of previous MOHTRON
    add_submenu_page( 'mohtronsettings_edit', 'Email Templates', 'Email Templates', 'manage_options', 'admin.php?page=email-templates', 'mohtron_email_template_submenu_cb');
    

}
add_action( 'admin_menu', 'mohtron_email_template_submenu', 99 );

function mohtron_settings_page() {
	?>
	<div class="wrap">
		<h2><?php _e( 'MOH Settings', MOHTRON_TEXT); ?></h2>
	</div>
	<?php
}

function mohtron_email_template_submenu_cb() {
	?>
	<div class="wrap">
		<h2><?php _e( 'Email Templates', MOHTRON_TEXT); ?></h2>
		<form method="post" action="options.php">
			<?php 
			settings_fields( 'email-templates' );	//pass slug name of page, also referred
			do_settings_sections( 'email-templates' ); 	//pass slug name of page
			submit_button();
			?>
		</form>
	</div>
	<?php
}



function mohtron_send_subscribe_email( $emaildata ) {
    
    // Get Admin User Object
    // $admin_info = get_user_by( 1 );
    
    // Set Dynamic Data with placeholders and values
    $dynamicData = array(
    	'username' => $emaildata['user_object']->data->user_login,
        'useremail' =>$emaildata['user_object']->data->user_email,
        'siteurl' => get_option('siteurl'),
        'sitename' => get_option('blogname'),
        'date' => date_i18n(get_option('date_format')),
        'time' => date_i18n(get_option('time_format')),
        'sub_plan' => $emaildata['sub_plan'],
        'sub_expiry' => $emaildata['sub_expiry'],
    );
    
    // Get Admin Email
    $emails = get_option( 'admin_email' );
    
    // Create Email Subject
    $subjectAdmin = 'New User Subscribed:  ' . $dynamicData['username'];
    $subjectUser = 'You are now subscribed to :  ' . $emaildata['blogger_object']->data->user_nicename;  
    
    // Send Admin email
    new MohtronEmail( $emails, $subjectAdmin, $dynamicData, 'email_template_subscribe' );
    
    // Send User email
    new MohtronEmail( $dynamicData['useremail'], $subjectUser, $dynamicData, 'admin_email_template_subscribe' );

}

//Sending the Email on User Registration
add_action( 'user_register', 'mohtron_send_register_email', 10, 1 );
function mohtron_send_register_email( $user_id ) {
    
    // Get User Object
    $user_info = get_userdata( $user_id );
    
    // Set Dynamic Data with placeholders and values
    $dynamicData = array(
                        'username' => $user_info->user_login,
                        'useremail' => $user_info->user_email
                    );
    
    // Get Admin Email
    $emails = get_option( 'admin_email' );
    
    // Create Email Subject
    $subject = 'New User Registered:  ' . $user_info->user_login;
    
    // Name of our template (Settings Field)
    $template = 'email_template_subscribe';
    
    // Sending email with our Class
    new MohtronEmail( $emails, $subject, $dynamicData, $template );
}



//Sending the Email of payout status
function mohtron_send_payout_email( $payoutData ) {
    
    // Get User Object
    // $user_info = get_user_by( 'slug', $payoutData['user_slug'] );
    
    //send to this email
    $emails =  $payoutData['useremail'];
    
    // Create Email Subject
    $subject = 'MOH Payout status';
    
    // Name of our template (Settings Field)
    $template = 'payout_email_template';

    // pr($payoutData,1);

    // Sending email with our Class
    new MohtronEmail( $emails, $subject, $payoutData, $template );
}


//Subscription expired email
function mohtron_send_subscription_expired_email( $emaildata ) {   
    

    // Get Admin Email
    $emails = get_option( 'admin_email' );
    
    // Create Email Subject
    $subject = 'MOH Subscription expired';
    
    // Name of our template (Settings Field)
    $adminEmailTemplate = 'admin_email_template_subscription_expired';
    $userEmailTemplate = 'email_template_subscription_expired';
   
    
    // Sending email to admin and blogger
    new MohtronEmail( $emails, $subject, $emaildata, $adminEmailTemplate );
    new MohtronEmail(  $emaildata['blogger_email'], $subject, $emaildata, $adminEmailTemplate );
    
    // Sending email to supporter
    new MohtronEmail( $emaildata['useremail'], $subject, $emaildata, $userEmailTemplate );
}
