<?php
/**
 * Widget - Role Memberlist
 * 
 * @package Zourbuth
 * @subpackage Classes
 * http://zourbuth.com/role-memberlist
 * For another improvement, you can drop email to zourbuth@gmail.com or visit http://zourbuth.com
 */

/**
 * The widget was created to give users the ability to list all the role members in a list of their blog because
 * there was no equivalent WordPress widget that offered the functionality. This widget allows full
 * control over its output by giving access to the parameters of list_users().
 */
class Role_Memberlist_Widget extends WP_Widget {

	// @since 1.1
	var $prefix = '';

	// Textdomain for the widget.
	var $textdomain = 'role-memberlist';
	
	/**
	 * Set up the widget's unique name, ID, class, description, and other options.
	 * @since 0.6.0
	 */
	function __construct() {
		$this->prefix = 'role-memberlist';
		
		// load the widget stylesheet for the widgets screen
		add_action( 'load-widgets.php', array(&$this, 'load_widgets') );
		add_action( 'init', array(&$this, 'load_templates') );
		
		// Set up the widget options
		$widget_options = array(
			'classname' => $this->prefix,
			'description' => esc_html__( 'An advanced widget to display your role memberlist.', $this->textdomain )
		);

		// Set up the widget control options
		$control_options = array(
			'width' => 460,
			'height' => 350,
			'id_base' => $this->prefix
		);

		// Create the widget
		$this->WP_Widget( $this->prefix, esc_attr__( 'Role Memberlist', $this->textdomain ), $widget_options, $control_options );

		// Print plugin and the user costum scripts or styles
		if ( is_active_widget( false, false, $this->id_base, false ) && ! is_admin() ) {
			add_action( 'wp_enqueue_scripts', array( &$this, 'costum_stylescript' ) );
		}
	}

	
	// push the widget stylesheet widget.css into widget admin page
	function load_widgets() {
		if( function_exists( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		} else {
			wp_enqueue_style('thickbox');
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
		}
		wp_enqueue_style('total-dialog');
		wp_enqueue_script('total-dialog');
	}
	
	
	// Print the user stylesheet and script
	function load_templates() {
		$templates = role_memberlist_templates();
		$settings = $this->get_settings();
		foreach( $settings as $k => $setting )
			if( isset( $setting['template'] ) )
				if ( file_exists( $templates[ $setting['template'] ] ) )
					require_once( $templates[ $setting['template'] ] );
	}
	
	
	// Print the user stylesheet and script
	function costum_stylescript() {
		$settings = $this->get_settings();
		foreach( $settings as $k => $setting ){					
			if ( !empty( $setting['stylescript'] ) )
				echo $setting['stylescript'];
		}
	}
	
	
	/**
	 * Outputs the widget based on the arguments input through the widget controls.
	 * @since 0.1
	 */
	function widget( $args, $instance ) {
		extract( $args, EXTR_SKIP );

		// Set up the arguments
		$args = array(
			'blog_id' 		=> $GLOBALS['blog_id'],
			'role'			=> $instance['role'],
			'meta_key' 		=> $instance['meta_key'],
			'meta_value' 	=> $instance['meta_value'],
			'meta_compare' 	=> $instance['meta_compare'],
			'include' 		=> ! empty( $instance['include'] ) ? join( ', ', $instance['include'] ) : '',
			'exclude' 		=> ! empty( $instance['exclude'] ) ? join( ', ', $instance['exclude'] ) : '',			
			'search' 		=> $instance['search'],
			'orderby'		=> $instance['orderby'],
			'order'			=> $instance['order'],
			'offset' 		=> $instance['offset'],
			'number' 		=> ! empty( $instance['number'] ) ? intval( $instance['number'] ) : '',
			'count_total'	=> ! empty( $instance['count_total'] ) ? true : false,
			'fields' 		=> $instance['fields'],
			'who' 			=> $instance['who'],
			'template' 		=> isset( $instance['template'] ) ? $instance['template'] : 'default',
			'target_blank'	=> ! empty( $instance['target_blank'] ) ? true : false,

			'show_name' 	=> ! empty( $instance['show_name'] ) ? true : false,
			'use_fullname' 	=> ! empty( $instance['use_fullname'] ) ? true : false,
			'show_avatar' 	=> ! empty( $instance['show_avatar'] ) ? true : false,
			'avatar_size' 	=> ! empty( $instance['avatar_size'] ) ? intval( $instance['avatar_size'] ) : 60,
			'show_contact'	=> ! empty( $instance['show_contact'] ) ? true : false,
			'bio_info'		=> ! empty( $instance['bio_info'] ) ? true : false,
			'tab_active'	=> $instance['tab_active'],
			'intro_text' 	=> $instance['intro_text'],
			'outro_text' 	=> $instance['outro_text'],
			'stylescript'	=> $instance['stylescript'],
			'echo' 			=> false
		);
		
		// Output the theme's $before_widget wrapper
		echo $before_widget;

		// If a title was input by the user, display it
		if ( !empty( $instance['title'] ) )
			echo $before_title . apply_filters( 'widget_title',  $instance['title'], $instance, $this->id_base ) . $after_title;

		// Print intro text if exist
		if ( !empty( $instance['intro_text'] ) )
			echo '<p class="'. $this->id . '-intro-text intro-text">' . $instance['intro_text'] . '</p>';
			
		// Display the memberlist list
		echo role_memberlist( $args );		
		
		// Print outro text if exist
		if ( !empty( $instance['outro_text'] ) )
			echo '<p class="'. $this->id . '-outro-text outro-text">' . $instance['outro_text'] . '</p>';
			
		// Close the theme's widget wrapper
		echo $after_widget;
	}

	
	/**
	 * Updates the widget control options for the particular instance of the widget.
	 * @since 0.1
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance = $new_instance;
		
		// If new role is chosen, reset includes and excludes
		if ( $instance['role'] !== $old_instance['role'] && '' !== $old_instance['role'] ) {
			$instance['include'] = array();
			$instance['exclude'] = array();
		}

		$instance['meta_key'] 		= strip_tags( $new_instance['meta_key'] );
		$instance['meta_value'] 	= strip_tags( $new_instance['meta_value'] );
		$instance['meta_compare'] 	= strip_tags( $new_instance['meta_compare'] );
			
		$instance['search'] 		= $new_instance['search'];
		$instance['orderby']		= $new_instance['orderby'];
		$instance['order']			= $new_instance['order'];
		$instance['offset'] 		= $new_instance['offset'];
		$instance['number'] 		= ! empty( $instance['number'] ) ? intval( $instance['number'] ) : '';
		$instance['count_total']	= ! empty( $instance['count_total'] ) ? true : false;
		$instance['fields'] 		= $instance['fields'];
		$instance['who'] 			= $instance['who'];
		$instance['template']		= $instance['template'];
		$instance['target_blank']	= ! empty( $instance['target_blank'] ) ? true : false;

		$instance['show_name'] 		= isset( $new_instance['show_name'] ) ? 1 : 0;
		$instance['use_fullname'] 	= isset( $new_instance['use_fullname'] ) ? 1 : 0;
		$instance['show_avatar'] 	= isset( $new_instance['show_avatar'] ) ? 1 : 0;
		$instance['avatar_size'] 	= (int) $new_instance['avatar_size'];
		$instance['show_contact']	= isset( $new_instance['show_contact'] ) ? 1 : 0;
		$instance['bio_info']		= isset( $new_instance['bio_info'] ) ? 1 : 0;
		$instance['tab_active']		= $new_instance['tab_active'];
		$instance['intro_text'] 	= $new_instance['intro_text'];
		$instance['outro_text'] 	= $new_instance['outro_text'];
		$instance['stylescript']	= $new_instance['stylescript'];

		return $instance;
	}

	
	/**
	 * Displays the widget control options in the Widgets admin screen.
	 * @since 0.1
	 */
	function form( $instance ) {

		// Set up the default form values
		$defaults = array(
			'title' 		=> esc_attr__( 'Role Memberlist', $this->textdomain ),
			'blog_id' 		=> $GLOBALS['blog_id'],
			'role' 			=> 'subscriber',
			'meta_key' 		=> '',
			'meta_value' 	=> '',
			'meta_compare'	=> '',
			'include' 		=> array(),
			'exclude' 		=> array(),
			'search' 		=> '',
			'orderby' 		=> 'login',
			'order' 		=> 'ASC',
			'offset' 		=> '',
			'number' 		=> '',
			'count_total' 	=> true,
			'fields' 		=> 'all',
			'who' 			=> '',
			'template'		=> 'default',
			'target_blank'	=> false,

			'show_name' 	=> true,
			'use_fullname' 	=> true,
			'show_avatar' 	=> true,
			'avatar_size' 	=> 40,
			'show_contact'	=> false,
			'bio_info'		=> false,
			'tab_active'	=> array( 0 => true, 1 => false, 2 => false, 3 => false, 4 => false, 5 => false, 6 => false, 7 => false ),
			'intro_text' 	=> '',
			'outro_text' 	=> '',
			'stylescript'	=> ''
		);

		// Merge the user-selected arguments with the defaults. */
		$instance = wp_parse_args( (array) $instance, $defaults );

		$tabs = array( 
			__( 'General', $this->textdomain ),  
			__( 'Advanced', $this->textdomain ),
			__( 'Template', $this->textdomain ),
			__( 'Customs', $this->textdomain ),
			__( 'Supports', $this->textdomain )
		);		

		global $wp_roles;
		if ( ! isset( $wp_roles ) ) $wp_roles = new WP_Roles();
		$roles = $wp_roles->get_names();
		
		$members = get_users( array( 'role' => $instance['role'] ) );	
		
		// Load templates
		$templates = role_memberlist_templates(); 
		
		$order = array( 'ASC' => esc_attr__( 'Ascending', $this->textdomain ), 'DESC' => esc_attr__( 'Descending', $this->textdomain ) );
		$orderby = array( 
			'display_name' => esc_attr__( 'Display Name', $this->textdomain ), 
			'email' => esc_attr__( 'Email', $this->textdomain ), 
			'ID' => esc_attr__( 'ID', $this->textdomain ), 
			'nicename' => esc_attr__( 'Nice Name', $this->textdomain ), 
			'post_count' => esc_attr__( 'Post Count', $this->textdomain ), 
			'registered' => esc_attr__( 'Registered', $this->textdomain ), 
			'url' => esc_attr__( 'URL', $this->textdomain ), 
			'user_login' => esc_attr__( 'Login', $this->textdomain ),
			'meta_value' => esc_attr__( 'Meta Value', $this->textdomain ),
		);
		$intro_text = esc_textarea($instance['intro_text']);
		$outro_text = esc_textarea($instance['outro_text']);
		$stylescript = esc_textarea($instance['stylescript']);
		?>
		
		<div class="pluginName">Role Memberlist<span class="pluginVersion"><?php echo ROLE_MEMBERLIST_VERSION; ?></span></div>
		
		<div id="tupro-<?php echo $this->id ; ?>" class="total-options tabbable tabs-left">
		
			<ul class="nav nav-tabs">
				<?php foreach ($tabs as $key => $tab ) : ?>
					<li class="<?php echo $instance['tab_active'][$key] ? 'active' : '' ; ?>"><?php echo $tab; ?><input type="hidden" name="<?php echo $this->get_field_name( 'tab_active' ); ?>[]" value="<?php echo $instance['tab_active'][$key]; ?>" /></li>
				<?php endforeach; ?>							
			</ul>

			<ul class="tab-content">
				<li class="tab-pane <?php if ( $instance['tab_active'][0] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', $this->textdomain ); ?></label>
							<span class="description"><?php _e( 'Give the widget title or leave it empty for no title.', $this->textdomain ); ?></span>
							<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
						</li>						
						<li>
							<label for="<?php echo $this->get_field_id( 'role' ); ?>"><?php _e( 'Role', $this->textdomain ); ?></label>
							<span class="description"><?php _e( 'Limit the returned authors to the role specified.', $this->textdomain ); ?></span>
							<select class="smallfat" onchange="wpWidgets.save(jQuery(this).closest('div.widget'),0,1,0);" id="<?php echo $this->get_field_id( 'role' ); ?>" name="<?php echo $this->get_field_name( 'role' ); ?>">
								<?php foreach ( $roles as $role => $v ) { ?>
									<option value="<?php echo esc_attr( $role ); ?>" <?php selected( $instance['role'], $role ); ?>><?php echo esc_html( $v ); ?></option>
								<?php } ?>
							</select>							
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'orderby' ); ?>"><?php _e( 'Order & Order By', $this->textdomain ); ?></label> 
							<span class="description"><?php _e( 'Order by user meta data in ascending or descending.', $this->textdomain ); ?></span>
							<select class="smallfat" id="<?php echo $this->get_field_id( 'orderby' ); ?>" name="<?php echo $this->get_field_name( 'orderby' ); ?>">
								<?php foreach ( $orderby as $option_value => $option_label ) { ?>
									<option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $instance['orderby'], $option_value ); ?>><?php echo esc_html( $option_label ); ?></option>
								<?php } ?>
							</select>

							<select class="smallfat" id="<?php echo $this->get_field_id( 'order' ); ?>" name="<?php echo $this->get_field_name( 'order' ); ?>">
								<?php foreach ( $order as $option_value => $option_label ) { ?>
									<option value="<?php echo esc_attr( $option_value ); ?>" <?php selected( $instance['order'], $option_value ); ?>><?php echo esc_html( $option_label ); ?></option>
								<?php } ?>
							</select>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number', $this->textdomain ); ?></label>							
							<span class="description"><?php _e( 'Limit the total number of users returned.', $this->textdomain ); ?></span>
							<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" value="<?php echo esc_attr( $instance['number'] ); ?>" />
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'who' ); ?>"><?php _e('Who', $this->textdomain); ?></label>
							<span class="description"><?php _e( 'If set to \'authors\', only authors (user level greater than 0) will be returned', $this->textdomain ); ?></span>
							<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'who' ); ?>" name="<?php echo $this->get_field_name( 'who' ); ?>" value="<?php echo esc_attr( $instance['who'] ); ?>" />
						</li>
					</ul>
				</li>
				
				<li class="tab-pane <?php if ( $instance['tab_active'][1] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<label for="<?php echo $this->get_field_id( 'include' ); ?>"><?php _e( 'Include', $this->textdomain ); ?></label>							
							<span class="description"><?php _e( 'Only selected users below will be returned.', $this->textdomain ); ?></span>
							<select class="widefat" id="<?php echo $this->get_field_id( 'include' ); ?>" name="<?php echo $this->get_field_name( 'include' ); ?>[]" size="4" multiple="multiple">
								<?php foreach ( $members as $member ) { ?>
									<option value="<?php echo $member->ID; ?>" <?php echo ( in_array( $member->ID, (array) $instance['include'] ) ? 'selected="selected"' : '' ); ?>><?php echo esc_html( $member->display_name ); ?></option>
								<?php } ?>
							</select>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'exclude' ); ?>"><?php _e( 'Exclude', $this->textdomain ); ?></label>
							<span class="description"><?php _e( 'Only selected users below will <strong>not</strong> be returned.', $this->textdomain ); ?></span>
							<select class="widefat" id="<?php echo $this->get_field_id( 'exclude' ); ?>" name="<?php echo $this->get_field_name( 'exclude' ); ?>[]" size="4" multiple="multiple">
								<?php foreach ( $members as $member ) { ?>
									<option value="<?php echo $member->ID; ?>" <?php echo ( in_array( $member->ID, (array) $instance['exclude'] ) ? 'selected="selected"' : '' ); ?>><?php echo esc_html( $member->display_name ); ?></option>
								<?php } ?>
							</select>
						</li>					
						<li>
							<label for="<?php echo $this->get_field_id( 'meta_key' ); ?>"><?php _e('Meta Key', $this->textdomain); ?></label>							
							<span class="description"><?php _e( 'The meta_key in the wp_usermeta table for the meta_value to be returned.', $this->textdomain ); ?></span>
							<input type="text" class="mediumfat code" id="<?php echo $this->get_field_id( 'meta_key' ); ?>" name="<?php echo $this->get_field_name( 'meta_key' ); ?>" value="<?php echo esc_attr( $instance['meta_key'] ); ?>" />
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'meta_value' ); ?>"><?php _e('Meta Value', $this->textdomain); ?></label>
							<span class="description"><?php _e( 'The value of the meta key', $this->textdomain ); ?></span>
							<input type="text" class="mediumfat code" id="<?php echo $this->get_field_id( 'meta_value' ); ?>" name="<?php echo $this->get_field_name( 'meta_value' ); ?>" value="<?php echo esc_attr( $instance['meta_value'] ); ?>" />
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'meta_compare' ); ?>"><?php _e('Meta Compare', $this->textdomain); ?></label>
							<span class="description"><?php _e( 'Operator to test the \'meta_value\'. Possible values are \'!=\', \'>\', \'>=\', \'<\', or \'<=\'. Default value is \'=\'.', $this->textdomain ); ?></span>
							<input type="text" class="mediumfat code" id="<?php echo $this->get_field_id( 'meta_compare' ); ?>" name="<?php echo $this->get_field_name( 'meta_compare' ); ?>" value="<?php echo esc_attr( $instance['meta_compare'] ); ?>" />
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'fields' ); ?>"><?php _e('Fields', $this->textdomain); ?></label>
							<span class="description"><?php _e( 'Which fields to include in the returned array.', $this->textdomain ); ?></span>
							<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'fields' ); ?>" name="<?php echo $this->get_field_name( 'fields' ); ?>" value="<?php echo esc_attr( $instance['fields'] ); ?>" />
						</li>
					</ul>
				</li>				
				
				<li class="tab-pane <?php if ( $instance['tab_active'][2] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<label for="<?php echo $this->get_field_id( 'template' ); ?>-name"><?php _e( 'Templates', $this->textdomain ); ?></label>
							<span class="description"><?php _e( 'Select a tempate from the list. Template options may vary and may overwrite the settings below.', $this->textdomain ); ?></span>
							<select class="smallfat" onchange="wpWidgets.save(jQuery(this).closest('div.widget'),0,1,0);" id="<?php echo $this->get_field_id( 'template' ); ?>" name="<?php echo $this->get_field_name( 'template' ); ?>">
								<?php foreach( $templates as $dir => $index ) { ?>
									<option value="<?php echo $dir; ?>" <?php selected( $instance['template'], $dir ); ?>><?php echo esc_html( $dir ); ?></option>
								<?php } ?>
							</select>
							<span class="description" style="margin-top: 5px;">
								<strong style="color: #444444"><?php _e( 'Template Info', $this->textdomain ); ?></strong><br />
								<?php echo apply_filters( 'role_memberlist_template_info', __( 'No template info.', $this->textdomain ), $instance['template'] ); ?>
							</span>						
						</li>					
						<li>
							<label for="<?php echo $this->get_field_id( 'avatar_size' ); ?>"><?php _e('Avatar Size', $this->textdomain); ?></label>
							<span class="description"><?php _e( 'User avatar size in pixels.', $this->textdomain ); ?></span>
							<input type="text" class="smallfat code" id="<?php echo $this->get_field_id( 'avatar_size' ); ?>" name="<?php echo $this->get_field_name( 'avatar_size' ); ?>" value="<?php echo esc_attr( $instance['avatar_size'] ); ?>" />
						</li>					
						<li>
							<label for="<?php echo $this->get_field_id( 'show_avatar' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['show_avatar'], true ); ?> id="<?php echo $this->get_field_id( 'show_avatar' ); ?>" name="<?php echo $this->get_field_name( 'show_avatar' ); ?>" /> <?php _e( 'Show avatar', $this->textdomain ); ?></label>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'count_total' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['count_total'], true ); ?> id="<?php echo $this->get_field_id( 'count_total' ); ?>" name="<?php echo $this->get_field_name( 'count_total' ); ?>" /> <?php _e( 'Count total', $this->textdomain ); ?></label>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'show_name' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['show_name'], true ); ?> id="<?php echo $this->get_field_id( 'show_name' ); ?>" name="<?php echo $this->get_field_name( 'show_name' ); ?>" /> <?php _e( 'Show name', $this->textdomain ); ?></label>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id( 'use_fullname' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['use_fullname'], true ); ?> id="<?php echo $this->get_field_id( 'use_fullname' ); ?>" name="<?php echo $this->get_field_name( 'use_fullname' ); ?>" /> <?php _e( 'Use fullname', $this->textdomain ); ?></label>
						</li>		
						<li>
							<label for="<?php echo $this->get_field_id( 'show_contact' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['show_contact'], true ); ?> id="<?php echo $this->get_field_id( 'show_contact' ); ?>" name="<?php echo $this->get_field_name( 'show_contact' ); ?>" /> <?php _e( 'Show contact info', $this->textdomain ); ?></label>
						</li>			
						<li>
							<label for="<?php echo $this->get_field_id( 'bio_info' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['bio_info'], true ); ?> id="<?php echo $this->get_field_id( 'bio_info' ); ?>" name="<?php echo $this->get_field_name( 'bio_info' ); ?>" /> <?php _e( 'Show biographical info', $this->textdomain ); ?></label>
						</li>		
						<li>
							<label for="<?php echo $this->get_field_id( 'target_blank' ); ?>">
							<input class="checkbox" type="checkbox" <?php checked( $instance['target_blank'], true ); ?> id="<?php echo $this->get_field_id( 'target_blank' ); ?>" name="<?php echo $this->get_field_name( 'target_blank' ); ?>" /> <?php _e( 'Open user link in new window or tab.', $this->textdomain ); ?></label>
						</li>
						<?php do_action( 'role_memberlist_form', $instance, $this->get_field_id( 'tpl' ), $this->get_field_name( 'tpl' ) ); ?>
					</ul>
				</li>

				<li class="tab-pane <?php if ( $instance['tab_active'][3] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<label for="<?php echo $this->get_field_id('intro_text'); ?>"><?php _e( 'Intro Text', $this->textdomain ); ?>
							<span class="description"><?php _e( 'This option will display addtional text before the widget content and supports HTML.', $this->textdomain ); ?></span>
							<textarea name="<?php echo $this->get_field_name( 'intro_text' ); ?>" id="<?php echo $this->get_field_id( 'intro_text' ); ?>" rows="3" class="widefat"><?php echo $intro_text; ?></textarea>
							</label>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id('outro_text'); ?>"><?php _e( 'Outro Text', $this->textdomain ); ?>
							<span class="description"><?php _e( 'This option will display addtional text after widget and supports HTML.', $this->textdomain ); ?></span>
							<textarea name="<?php echo $this->get_field_name( 'outro_text' ); ?>" id="<?php echo $this->get_field_id( 'outro_text' ); ?>" rows="3" class="widefat"><?php echo $outro_text; ?></textarea>
							</label>
						</li>
						<li>
							<label for="<?php echo $this->get_field_id('stylescript'); ?>"><?php _e( 'Custom Script & Style', $this->textdomain ); ?></label>
							<span class="description"><?php _e( 'Use this box for additional widget CSS style of custom javascript. Current widget selector: ', $this->textdomain ); ?><?php echo '<tt>#' . $this->id . '</tt>'; ?></span>
							<textarea name="<?php echo $this->get_field_name( 'stylescript' ); ?>" id="<?php echo $this->get_field_id( 'stylescript' ); ?>" rows="4" class="widefat"><?php echo $stylescript; ?></textarea>							
						</li>
					</ul>
				</li>
				
				<li class="tab-pane <?php if ( $instance['tab_active'][4] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<h3>Support and Contribute</h3>
							<p>Please ask us for supports or discussing new features for the next updates.<p>
							<ul>
								<li>
									<a href="http://codecanyon.net/user/zourbuth#message?ref=zourbuth"><strong>CodeCanyon Author Profile Page</strong></a>
									<span class="description"><?php _e( 'Send private mail message via codecanyon.net', $this->textdomain ); ?></span>
								</li>
								<li>
									<a href="http://codecanyon.net/item/role-memberlist/discussion/1572944?ref=zourbuth"><strong>Plugin Discussion</strong></a>
									<span class="description"><?php _e( 'Discuss or post query in the item dicussion forum.', $this->textdomain ); ?></span>
								</li>
								<li>
									<p style="margin-bottom: 5px;"><a href="javascript: void(0)"><strong>Tweet to Get Supports</strong></a></p>
									<a href="https://twitter.com/intent/tweet?screen_name=zourbuth" class="twitter-mention-button" data-related="zourbuth">Tweet to @zourbuth</a>
									<a href="https://twitter.com/zourbuth" class="twitter-follow-button" data-show-count="false">Follow @zourbuth</a>									
								</li>
								<li>
									<span class="description"><?php _e( 'Help us to share this plugin.', $this->textdomain ); ?></span>
									<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://codecanyon.net/item/role-memberlist/1572944" data-text="Check out this WordPress #plugin item 'Role Memberlist'">Tweet</a>
									
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									<script>if( typeof(twttr) !== 'undefined' ) twttr.widgets.load()</script>								
								</li>
							</ul>
						</li>
					</ul>
				</li>
				<li class="tab-pane <?php if ( $instance['tab_active'][4] ) : ?>active<?php endif; ?>">
					<ul>
						<li>
							<h3>Support and Contribute</h3>
							<p>Please ask us for supports or discussing new features for the next updates.<p>
							<ul>
								<li>
									<a href="http://codecanyon.net/user/zourbuth#message?ref=zourbuth"><strong>CodeCanyon Author Profile Page</strong></a>
									<span class="description"><?php _e( 'Send private mail message via codecanyon.net', $this->textdomain ); ?></span>
								</li>
								<li>
									<a href="http://codecanyon.net/item/role-memberlist/discussion/1572944?ref=zourbuth"><strong>Plugin Discussion</strong></a>
									<span class="description"><?php _e( 'Discuss or post query in the item dicussion forum.', $this->textdomain ); ?></span>
								</li>
								<li>
									<p style="margin-bottom: 5px;"><a href="javascript: void(0)"><strong>Tweet to Get Supports</strong></a></p>
									<a href="https://twitter.com/intent/tweet?screen_name=zourbuth" class="twitter-mention-button" data-related="zourbuth">Tweet to @zourbuth</a>
									<a href="https://twitter.com/zourbuth" class="twitter-follow-button" data-show-count="false">Follow @zourbuth</a>									
								</li>
								<li>
									<span class="description"><?php _e( 'Help us to share this plugin.', $this->textdomain ); ?></span>
									<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://codecanyon.net/item/role-memberlist/1572944" data-text="Check out this WordPress #plugin item 'Role Memberlist'">Tweet</a>
									
									<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
									<script>if( typeof(twttr) !== 'undefined' ) twttr.widgets.load()</script>								
								</li>
							</ul>
						</li>
					</ul>
				</li>				
			</ul>
		</div>				
	<?php
	}
}

?>