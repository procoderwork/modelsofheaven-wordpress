<?php
 /*
	Description: A function to handle user query based on role selection, display it in with a shortcode or in a widget.
	Version: 0.1
	Author: zourbuth
	Author URI: http://zourbuth.com
	License: Under GPL2

	Copyright 2013 zourbuth (email : zourbuth@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! function_exists( 'role_memberlist' ) ):
	function role_memberlist($args = '') {
			
		$textdomain = 'role-memberlist'; // Textdomain for the widget.
		global $wpdb;
		
		$defaults = array(
			'blog_id' 		=> $GLOBALS['blog_id'],
			'role' 			=> 'subscriber',
			'meta_key' 		=> '',
			'meta_value'	=> '',
			'meta_compare' 	=> '',
			'include' 		=> array(),
			'exclude' 		=> array(),
			'search' 		=> '',
			'orderby' 		=> 'login',
			'order' 		=> 'ASC',
			'offset' 		=> '',
			'number' 		=> '',
			'count_total' 	=> true,
			'fields' 		=> 'all',
			'who' 			=> '',
			'template'		=> 'default',
			
			'show_name' 	=> true,
			'use_fullname' 	=> true,
			'show_avatar' 	=> true,
			'avatar_size' 	=> 80,
			'show_contact'	=> false,
			'bio_info'		=> false
		);

		$args = wp_parse_args( $args, $defaults );
		
		return apply_filters( 'role_memberlist', '', $args );
	}
endif;


/**
 * Get the templates list
 * @since 2.0.0
 */
function role_memberlist_templates() {
	$templates = array();
	$_dir = ROLE_MEMBERLIST_DIR . trailingslashit( 'tpl' );
	
	if ( $handle = opendir( $_dir ) ) {
		while (false !== ( $entry = readdir( $handle ) ) )
			if( $entry != '.' && $entry != '..' && $entry != 'index.php' ) {
			
				$default_headers = array(
					'Template Name' => 'Template Name',
					'Template URI' 	=> 'Template URI',
					'Author' 		=> 'Author',
					'Author URI' 	=> 'Author URI',
					'Description' 	=> 'Description',
					'Version' 		=> 'Version',
					'License' 		=> 'License'
				);				
			
				// Read list of files
				$files = scandir( $_dir . $entry );
				foreach( $files as $file )
					if ( $file != '.' && $file != '..' ) {
						//$templates[$entry]['files'][] = $file;
						$file_path = $_dir . trailingslashit( $entry ) . $file;
						$file_data = get_file_data( $file_path, $default_headers );
						if( $file_data['Template Name'] )
							$templates[$entry] = $file_path;
					}
			}
		
		closedir($handle);
	}

	return apply_filters( 'role_memberlist_templates', $templates );
}


/**
 * Get the user link based on user object
 * @since 2.0.0
 */
function rm_get_user_link( $user, $args = array() ) {
	if( get_author_posts_url( $user->ID ) )
		$user_link = get_author_posts_url( $user->ID );
	elseif( $user->user_url )
		$user_link = $user->user_url;
	else
		$user_link = false;

	return $user_link;
}


/**
 * Pass the template styles to the header
 * @since 2.0.0
 */
function role_memberlist_head() {
	do_action( 'role_memberlist_head' );
}
add_action( 'wp_head', 'role_memberlist_head' );
?>