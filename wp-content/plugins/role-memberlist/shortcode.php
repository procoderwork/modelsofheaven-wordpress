<?php
/*    
    http://zourbuth.com/
    Copyright 2013  zourbuth.com  (email : zourbuth@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

add_shortcode( 'role-memberlist', 'role_memberlist_shortcode' );
add_action( 'wp', 'role_memberlist_detect_shortcode' );

function role_memberlist_shortcode( $atts ) {
	$attr = shortcode_atts( array( 
		'blog_id' => $GLOBALS['blog_id'],
		'role' => 'subscriber',
		'meta_key' => '',
		'meta_value' => '',
		'meta_compare' => '',
		'include' => array(),
		'exclude' => array(),
		'search' => '',
		'orderby' => 'login',
		'order' => 'ASC',
		'offset' => '',
		'number' => '',
		'count_total' => true,
		'fields' => 'all',
		'who' => '',
		'template'	=> 'default',
		'target_blank'	=> false,		

		'show_name' 	=> true,
		'use_fullname' 	=> true,
		'show_avatar' 	=> true,
		'avatar_size' 	=> 80,
		'show_contact'	=> false,
		'bio_info'		=> false
	), $atts );
	
	return role_memberlist( $attr );
}

function role_memberlist_detect_shortcode() {
    global $post;
    $pattern = get_shortcode_regex();

    if ( preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
	&& array_key_exists( 2, $matches )
	&& in_array( 'role-memberlist', $matches[2] ) ) {
		if ($matches) {	
			$templates = role_memberlist_templates();
			foreach ( $matches[3] as $match ) {
				$attr = shortcode_parse_atts( $match );
				if( isset( $attr['template'] ) )
					require_once( $templates[ $attr['template'] ] );	
			}
		}
    }
}
?>