<?php
/*
Plugin Name: Hide subscribers in Buddypress
Description: Hides subscribers on site from showing up anywhere on site!
Version: 1.0
Author: SeventhQueen
Author URI: http://www.seventhqueen.com
*/


add_filter( 'bp_after_has_members_parse_args', 'models_exclude_users_by_role' );
 
function models_exclude_users_by_role( $args ) {
    //do not exclude in admin
    if( is_admin() && ! defined( 'DOING_AJAX' ) ) {
        return $args;
    }
    
    $excluded = isset( $args['exclude'] )? $args['exclude'] : array();
 
    if( !is_array( $excluded ) ) {
        $excluded = explode(',', $excluded );
    }
    
    $role = 'Subscriber';
    $user_ids =  get_users( array( 'role' => $role ,'fields'=>'ID') );
    
    
    $excluded = array_merge( $excluded, $user_ids );
    
    $args['exclude'] = $excluded;
    
    return $args;
}