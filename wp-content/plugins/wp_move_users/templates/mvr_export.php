<?php
global $wpdb, $current_user;


if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	if( isset( $_POST['export'] ))
	{
		// Check if we have to export by ID
		if( !empty( $_POST['export_limit_by'] ))
		{
			if( !empty( $_POST['export_ids'] ))
			{
				$ids = array();
				$is_range = explode('#', $_POST['export_ids']);
				
				if( !empty($is_range[1]))
				{
					for( $i = $is_range[0]; $i <= $is_range[1]; $i++)
					{
						$ids[] = $i;
					}
				}
				else
				{
					$ids = explode(',', $_POST['export_ids'] );
				}
				
				// Include
				if( $_POST['export_limit_by'] == 'include' )
				{
					$include = $ids;
				}
				else // exclude
				{
					$exclude = $ids;
				}
			}
			else
			{
				$err[] = 'If you choose to export by ID you need to add some IDs to the list.';
			}
			
		}
		
		if( !$err )
		{
			//$func = 'wp_mvr_get_' . $_POST['export_db'];
			$func = 'wp_mvr_get_users';
			
			$res = $func( $include, $exclude, $_POST['all_users'] );
			
			$err[] = 'You exported <b>' . count($res) . '</b> users successfully!';
		}
		
	}
}



?>
<div class="wrap">
	<div class="wpmvr">

        <div id="icon-mvr" class="icon32"></div>
        <h2>Export Users</h2>
        <div style="margin:0 0 20px 0;">
        
        	<?php
			if( $err )
			{
				?>
                <div class="updated err fade">
                <?php
				foreach( $err as $error )
				{
					?>
					<div><?php echo $error; ?></div>
					<?php
				}
				?>
                </div>
            	<?php
			}
			?>
        
            
            <!-- form -->
            <form action="" method="post" enctype="multipart/form-data">
                
                <ul>
                	<li>
                    	<label for="exportOPT"><h3>Export Options: <small>leave options empty to export all users.</small></h3></label><br />
                        <small><i>Optional:</i> Export users by ID. <i>Separate ID's by comma.</i></small><br />
                        <small>If you need to export large amount of users by ID you can create a group using <strong>#</strong></small><br/>
                        <small><i>For example: <strong>1#500</strong> This will export users with ID between 1 and 500</i></small><br />
                         <select id="exportOPT" name="export_limit_by">
                         	<option value="" <?php echo !$_POST['export_limit_by'] ? 'selected="selected"' : ''; ?>></option>
                            <option value="include" <?php echo $_POST['export_limit_by'] == 'include' ? 'selected="selected"' : ''; ?>>Include</option>
                            <option value="exclude" <?php echo $_POST['export_limit_by'] == 'exclude' ? 'selected="selected"' : ''; ?>>Exclude</option>
                        </select> 
                    	<input type="text" name="export_ids" style="width:300px;" placeholder="ID1, ID2, ID3 --OR-- 1#500" value="<?php echo $_POST['export_ids']; ?>" />
                    </li>
                    
                    <?php 
					// IF MULTISITE
					if ( is_multisite() )
					{
						?>
                        <li>
                            <label for="all_users"><small><i>Optional <u>If Multisite</u>:</i> load all users from the entire network?</small></label><br />
                          
                             <select id="all_users" name="all_users">
                                <option value="" <?php echo !$_POST['all_users'] ? 'selected="selected"' : ''; ?>></option>
                                <option value="0" <?php echo $_POST['all_users'] == '0' ? 'selected="selected"' : ''; ?>>No, - only users from current blog -</option>
                                <option value="1" <?php echo $_POST['all_users'] == '1' ? 'selected="selected"' : ''; ?>>Yes, - all users from entire network -</option>
                            </select> 
                        </li>
                        <?php
					}
					?>
                    <!--
                    <li>
                        <label for="exportDB">What to export?</label><br>
                        <select id="exportDB" name="export_db">
                            <option value="users">Users</option>
                            <option value="users">Posts</option>
                        </select> 
                    </li>
                    -->    
                         
                   
                    <li style="margin:20px 0 0 0;"><input class="button-primary" type="submit" name="export" value="export users"></li>
                </ul>
               
            </form>
            
            
            <!-- results -->
            <div class="grey_box">
            <?php
            if( $res )
            {
                ?>
                <div style="padding:5px;">
                	<b>Export code:</b><br />
                	<small>
                    	Copy this code and paste it in the import textfield on the website where you want to import the users.
                    </small>
                </div>
                <textarea readonly><?php echo base64_encode( serialize( $res ) ); ?></textarea>
                <?php
            }
            ?>
            </div>
        </div>
    
    
	</div>
	<!-- end wpmvr -->
</div>
<!-- end wrap -->