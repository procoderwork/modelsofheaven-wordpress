<?php
global $wpdb, $current_user;

$err = array();

if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
	if( isset( $_POST['import'] ))
	{
		if( !empty( $_POST['insert_type'] ))
		{
			if( !empty( $_POST['import_cd'] ))
			{
				$cd = unserialize( base64_decode( $_POST['import_cd'] ) );
				$err = wp_mvr_import_users( $cd, $_POST['insert_type'], $_POST['addto_mainsite'] );
			}
			else
			{
				$err[] = 'Please add your import code.';
			}
		}
		else
		{
			$err[] = 'Please select an insert type.';
		}
	}
}


?>
<div class="wrap">
    <div class="wpmvr">
    
        <div id="icon-mvr" class="icon32"></div>
        <h2>Import Users</h2>
        <div style="margin:0 0 20px 0;">
            
            <?php
			if( $err )
			{
				?>
                <div class="updated err fade">
                <?php
				foreach( $err as $error )
				{
					?>
					<div><?php echo $error; ?></div>
					<?php
				}
				?>
                </div>
            	<?php
			}
			?>
            
            <!-- form -->
            <form action="" method="post" enctype="multipart/form-data">
                
                <ul>
                    <li>
                        <label for="insert_type"><h3>Insert Type: <small>How do you want to insert the data?</small></h3></label><br>
                        
                        <select id="insert_type" name="insert_type" style="width:200px;">
                            <option value=""></option>
                            <option value="add">Add * - gives new user ID's -</option>
                            <option value="new">New * - keep same user ID's! -</option>
                        </select>  
                        
                        <div style="margin:0 0 20px 0;">
                            <small>
                            	<b>* Add</b> : Keep all current users and just add the new ones to the list. This option will give new user ids to the imported users starting from the current highest ID.<br>
                            	<b>* New</b> : Recommended for new sites only! This option will keep the user ids as they are on your current website. But if the same ID already exists in the database the user will not be added!
                            </small>
                        </div>
                    </li>
                    
                    <?php 
					// IF MULTISITE
					$blogID = get_current_blog_id();
					if ( is_multisite() && $blogID != BLOG_ID_CURRENT_SITE )
					{
						?>
                        <li>
                            <label for="addto_mainsite"><small><i>Optional <u>If Multisite</u>:</i> add users to <b><?php bloginfo('name'); ?></b> but also copy them to the main site of the network?</small></label><br>
                            <select id="addto_mainsite" name="addto_mainsite">
                                <option value=""></option>
                                <option value="0">No, - This blog only -</option>
                                <option value="1">Yes, - copy users to main site! -</option>
                            </select>  
                        </li>
                        <?php
					}
					?>
                    
                    <li>
                        <label for="importCD"><b>import code.</b></label><br>
                        <textarea id="importCD" name="import_cd" placeholder="Paste your import code here..."></textarea>
                    </li>
                    
                    <li><input class="button-primary" type="submit" name="import" value="import users"></li>
                </ul>
               
            </form>
        </div>
    
    </div>
	<!-- end wpmvr -->
</div>
<!-- end wrap -->