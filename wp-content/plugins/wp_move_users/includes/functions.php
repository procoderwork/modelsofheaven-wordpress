<?php
/*
 * EXPORT FUNCTIONS
*/

// Get users & usermeta
function wp_mvr_get_users( $include, $exclude, $multisite_all_users = 0 )
{
	global $wpdb;
	$results = array();
	$results_meta = array();
	$blogID = get_current_blog_id();
	
	
	// Get the users
	if ( is_multisite() && $multisite_all_users )
	{
		$users = $wpdb->get_results("SELECT * FROM ".$wpdb->get_blog_prefix( BLOG_ID_CURRENT_SITE )."users ".$limit);
	}
	else
	{
		$args = array(
			'include'      => $include,
			'exclude'      => $exclude,
			'fields'       => 'all'
		);
		$users = get_users( $args );
	}
	
	// Create code
	foreach( $users as $key => $user )
	{
		$results[$key]['dbprefix'] = $wpdb->get_blog_prefix( $blogID );

		// create query
		$results[$key]['id'] = $user->ID;
		$results[$key]['user_login'] = $user->user_login;
		$results[$key]['user_pass'] = $user->user_pass;
		$results[$key]['user_nicename'] = $user->user_nicename;
		$results[$key]['user_email'] = $user->user_email;
		$results[$key]['user_url'] = $user->user_url;
		$results[$key]['user_registered'] = $user->user_registered;
		$results[$key]['user_activation_key'] = $user->user_activation_key;
		$results[$key]['user_status'] = $user->user_status;
		$results[$key]['display_name'] = $user->display_name;
		
		// get the usermeta
		$usermeta = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."usermeta WHERE user_id = ".$user->ID );
		foreach( $usermeta as $mk => $meta )
		{
			// create query
			$results_meta[$mk]['umeta_id'] = $meta->umeta_id; 
			$results_meta[$mk]['user_id'] = $meta->user_id; 
			$results_meta[$mk]['meta_key'] = $meta->meta_key; 
			$results_meta[$mk]['meta_value'] = $meta->meta_value; 	
			
			$results[$key]['metadata'] = $results_meta;
		}
	}
	
	return $results;
}








/*
 * IMPORT FUNCTIONS
*/

function wp_mvr_import_users( $code, $insert_type, $addto_mainsite = 0 )
{
	global $wpdb;
	
	
	// IF MULTISITE
	if ( is_multisite() ) 
	{ 
		$db_prefix = $wpdb->get_blog_prefix( BLOG_ID_CURRENT_SITE ); 
	}
	else
	{
		$db_prefix = $wpdb->prefix;
	}
	
	
	if( $db_prefix )
	{
		$cnt = 0;
	
		foreach( $code as $key => $cd )
		{
			// Check if this user does not already exists
			$user_check = $wpdb->get_results( "SELECT * FROM ".$db_prefix."users WHERE user_email = '".$cd['user_email']."' LIMIT 1" );
			if( !$user_check[0]->user_email )
			{
				// first we check if we need to change the user ID
				if( $insert_type == 'add' )
				{
					$lastID = $wpdb->get_results( "SELECT ID FROM ".$db_prefix."users ORDER BY ID DESC LIMIT 1" );
					$userID = $lastID[0]->ID + 1;
				}
				else
				{
					$userID = $cd['id'];
				}
				
				
				// Check if this ID does not already exists
				$id_check = $wpdb->get_results( "SELECT ID FROM ".$db_prefix."users WHERE ID = '".$userID."' LIMIT 1" );
				if( !$id_check[0]->ID )
				{
	
					// Insert into user database
					$wpdb->query(
							
						'INSERT INTO `'.$db_prefix.'users` (ID, user_login, user_pass, user_nicename, user_email, user_url, user_registered, user_activation_key, user_status, display_name) VALUES(  
						'.$userID.', 
						"'.$cd['user_login'].'",
						"'.$cd['user_pass'].'",
						"'.$cd['user_nicename'].'",
						"'.$cd['user_email'].'",
						"'.$cd['user_url'].'",
						"'.$cd['user_registered'].'",
						"'.$cd['user_activation_key'].'",
						"'.$cd['user_status'].'",
						"'.$cd['display_name'].'"
						);'
						
					);
						
					// Insert into usermeta database
					if( $cd['metadata'] )
					{
						foreach( $cd['metadata'] as $metadata )
						{
							// if the db_prefix is used for a table name we need to change it to the new one first.
							$capabilities_key = explode('capabilities' , $metadata['meta_key']);
							$user_level_key = explode('user_level' , $metadata['meta_key']);
							$dashboard_quick_press_last_post_id_key = explode('dashboard_quick_press_last_post_id' , $metadata['meta_key']);
							$user_settings_key = explode('user-settings' , $metadata['meta_key']);
							$user_settings_time_key = explode('user-settings-time' , $metadata['meta_key']);
							$_bbp_last_posted_key = explode('_bbp_last_posted' , $metadata['meta_key']);
								
							if( count( $capabilities_key ) > 1 )
							{
								$meta_key = $wpdb->prefix . 'capabilities';
							}
							elseif( count( $user_level_key ) > 1 )
							{
								$meta_key = $wpdb->prefix . 'user_level';
							}
							elseif( count( $dashboard_quick_press_last_post_id_key  ) > 1 )
							{
								$meta_key = $wpdb->prefix . 'dashboard_quick_press_last_post_id';
							}
							elseif( count( $user_settings_key ) > 1 )
							{
								$meta_key = $wpdb->prefix . 'user-settings';
							}
							elseif( count( $user_settings_time_key ) > 1 )
							{
								$meta_key = $wpdb->prefix . 'user-settings-time';
							}
							elseif( count( $_bbp_last_posted_key ) > 1 )
							{
								$meta_key = $wpdb->prefix . '_bbp_last_posted';
							}
								
							else
							{
								$meta_key = $metadata['meta_key'];
							}
								
					
							
							$wpdb->query(
									
								'INSERT INTO `'.$db_prefix.'usermeta` (user_id, meta_key, meta_value) VALUES(  
								'.$userID.',
								"'.mysql_real_escape_string( $meta_key ).'",
								"'.mysql_real_escape_string( $metadata['meta_value'] ).'"
								);'
								
							);
							
							// IF MULTISITE 
							if( $addto_mainsite )
							{
								$user = new WP_User( $userID );
								if( add_user_to_blog( BLOG_ID_CURRENT_SITE, $userID, $user->roles[0] ) )
								{
									$info = ' And are added to the main site.';
								}
							} // end if addto_mainsite
								
						}
						
					} // end if usermeta
						
					$cnt++;
				
				}
				else
				{
					$err[] = 'USER WITH ID: <b><i>'.$userID.'</i></b> already exists in this database and was not added again.';
				} // end check if ID exists
			} 
			else
			{
				$err[] = 'USER: <b><i>'.$cd['user_email'].'</i></b> already exists in this database and was not added again.';
			} // end check if user exists
				
			
		}
		
		$err[] = $cnt . ' USERS ARE ADDED TO THE DATABASE!' .$info;
		
	}
	else
	{
		
		$err[] = 'ERROR: The database <b><i>`'.$db_prefix.'users`</i></b> does not exists.';
	} // end check if database exists
	
	return $err;
}
?>