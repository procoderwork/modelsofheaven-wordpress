<?php
/*
Plugin Name: WP Move Users
Plugin URI: http://www.wpplugins.be/wp-move-users
Description: Plugin to move wordpress users from one site to another.
Author: Tunafish
Version: 1.3
Author URI: http://www.tunasite.com
*/


//Plugin Path
$mvr_path = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
$mvr_abs_path = ABSPATH. 'wp-content/plugins/' .str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
$mvr_role_admin = 'add_users';

require_once('includes/functions.php');



// Load css into the ADMIN theme
function mvr_admin_init() 
{
	global $mvr_path;
	
	if( $_GET['page'] == 'wp-mvr-import' || $_GET['page'] == 'wp-export-users' )
	{
		wp_enqueue_style('mvr_admin_style', $mvr_path.'templates/css/style.css', false, "1.0", "all");
	}
}
add_action( 'admin_init', 'mvr_admin_init' );



// Button to admin page
function mvr_admin_actions() 
{
	global $mvr_role_admin, $mvr_path;
	
	// Create menu
	if (function_exists('add_object_page')) 
	{
		add_menu_page(
			__('WP Move Users','mvr'), 
			__('WP Move Users','mvr'), 
			$mvr_role_admin, 
			"wp-export-users" , 
			"mvr_export", 
			$mvr_path."templates/images/mvr16.png"
		);
	}
	
	// Create submenu
	add_submenu_page( "wp-export-users", __('Export Users','mvr'), __('Export Users','mvr'), $mvr_role_admin, "wp-export-users", "mvr_export" );
	add_submenu_page( "wp-export-users", __('Import Users','mvr'), __('Import Users','mvr'), $mvr_role_admin, "wp-mvr-import", "mvr_import" );
}
add_action('admin_menu', 'mvr_admin_actions');


// MENU FUNCTIONS
function mvr_export()
{
	include('templates/mvr_export.php');
}
function mvr_import()
{
	include('templates/mvr_import.php');
}
?>
