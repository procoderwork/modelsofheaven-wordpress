<?php
/*
Plugin Name: Gigabox
Plugin URI: http://codecanyon.net/user/wp_workshop
Description: Awesome image box to display your photos with style.
Version: 2.5
Author: WP Workshop
Author URI: http://codecanyon.net/user/wp_workshop
License: GNU
*/


$version = '2.5';

if (!class_exists("GigaboxAdmin")) {
	require_once dirname( __FILE__ ) . '/GigaboxAdmin.class.php';	
	$gb = new GigaboxAdmin (__FILE__, $version);
}

?>