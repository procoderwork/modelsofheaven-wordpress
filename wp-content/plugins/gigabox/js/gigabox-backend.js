jQuery(document).ready(function($) {



	var item_count = $('.gbboxitem').size();
    var deletedArray = Array();
    var dAC = 0;
	$("#gb-item-count").val(item_count);	

	//SHOW/HIDE SINGLE IMAGE PANEL
	if (item_count==1) {
		$("#gb-single-control").show();	
	}

	//SET ITEM ORDER
	determine_order();

    //BUTTON UPLOAD FOO
    $('.gbuploadb').click(function(e) {

        e.preventDefault();
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);

        var custom_uploader = wp.media({
            title: 'Gigabox Image',
            button: {
                text: 'Select Gigabox Image'
            },
            multiple: false  // Set this to true to allow multiple files to be selected
        })
                .on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            
            if (attachment.sizes.thumbnail)
               $(button).prev().prev().attr('src', attachment.sizes.thumbnail.url);
            else 
                $(button).prev().prev().attr('src', attachment.url);

            $(button).prev().val(attachment.id);

            wp.media.editor.send.attachment = send_attachment_bkp;
        }).open();

        return false;
    });    


	//BUTTON REMOVE FOO
    $('.gbremoveb').click(function(e) {
        e.preventDefault();
        var button = $(this);

        $(button).prev().prev().prev().attr('src', '');
        $(button).prev().prev().val('');            
    });		


    //BUTTON UPLOAD ICON TEMPLATE 
    $('.gbuploadIcon').click(function(e) {

        e.preventDefault();
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);

        var custom_uploader = wp.media({
            title: 'Gigabox Image',
            button: {
                text: 'Select Gigabox Image'
            },
            multiple: false  // Set this to true to allow multiple files to be selected
        })
                .on('select', function() {
            var attachment = custom_uploader.state().get('selection').first().toJSON();
            
            $('.gb-icon-template-preview').attr('src', attachment.url);

            $(button).prev().val(attachment.id);

            wp.media.editor.send.attachment = send_attachment_bkp;
        }).open();

        return false;
    });    


    //BUTTON DELETE ICON TEMPLATE
    $('.gbremoveIcon').click(function(e) {
    	e.preventDefault();
    	var defic = $(this).data('deficon');

		$('.gb-icon-template-preview').attr('src', defic);
		$(this).prev().prev().val('');  	
    });


	$("#delete-pgrid").click(function(e) {
		e.preventDefault();
		$("#pg-deletebox").show();

	});

	$(".pg-dontdelete").click(function(e) {
		e.preventDefault();
		$("#pg-deletebox").fadeOut();
	});


	//TITLE NAME CHANGE
    $('.gb-imgtitle').change(function() {
        var ovaj = $(this);
        var cale = $(this).parent().parent().parent().parent().parent().parent();
        if (!ovaj.val() || ovaj.val() == ' ')
            return;
        //console.log(cale);
        $('.hndler span', cale).html(ovaj.val());
    });		

    //DELETE IMAGE ITEM
    $(".gb-delete-item").click(function(e) {
        e.preventDefault();

        deletedArray[dAC] = $(this).parent().data("order"); 
        dAC++; 

        $("#gb-item-count").val(--item_count);
        if (item_count==1) {
        	$("#gb-single-control").slideDown();	
        } else {
        	$("#gb-single-control").slideUp();	
        }
        $(this).parent().slideUp(300, function() {$(this).remove(); determine_order(); });   
    }); 

    //ADD SORTABLE
	$('#gb-sortables').sortable({ 
		axis: "y",
        delay: 150, 
		deactivate: function( event, ui ) {
				determine_order();
			}
	});

	$('.gb-addnewitem').click(function(e) { 
		e.preventDefault();

        var count;
        if (dAC > 0 ) {
            dAC--;
            count = deletedArray[dAC];
        } else {
            count = item_count;
        }


		var template = ''+
'		<div class="postbox gbboxitem gbboxcnt'+count+'" data-order="'+count+'">'+
'		    <h3 class="hndle"><span>Untitled</span></h3><div class="gb-delete-item" id="gb-delete-item'+count+'">Delete this item</div>'+
'		    <div class="inside">'+
'		        <table class="gbimageitem">'+
'		            <thead>'+
'		                <tr>'+
'		                    <th width="20%"></th>'+
'		                    <th width="50%"></th>'+
'		                    <th width="30%"></th>'+
'		                </tr>'+
'		            </thead>'+
'		            <tbody>'+
'		                <tr>'+
'		                    <td>Image Title</td>'+
'		                    <td><input id="gb-item-title'+count+'" type="text" name="gb-item-title'+count+'" value="" class="gb-fullwidth" /></td>'+
'		                    <td><p class="howto">Enter the title of this image.</p></td>'+
'		                </tr>'+
'		                <tr>'+
'		                    <td>Image Description</td>'+
'		                    <td><textarea id="gb-item-desc'+count+'" name="gb-item-desc'+count+'" class="gb-description"></textarea></td>'+
'		                    <td><p class="howto">Enter the description of this image.</p></td>'+
'		                </tr>'+
'		                <tr>'+
'		                    <td>Image</td>'+
'		                    <td class="gb-upload-td">'+
'		                        <img class="gb-custom_media_image" src="" />'+
''+		                        
'		                        <input class="" id="gb-image-id'+count+'" type="hidden" name="gb-image-id'+count+'" value="">'+
'		                        <a href="#" class="button gbuploadb'+count+'">Upload</a>'+
'		                        <a href="#" class="button gbremoveb'+count+'">Remove</a>'+
'		                    </td>'+
'		                    <td><p class="howto">Select the image.</p></td>'+
'		                </tr>'+
'		                <tr>'+
'		                    <td>Read More Button Text</td>'+
'		                    <td><input id="gb-image-readmore'+count+'" type="text" name="gb-image-readmore'+count+'" value="" class="gb-fullwidth" /></td>'+
'		                    <td><p class="howto">Enter the text of the read more button here. Leave blank if none.</p></td>'+
'		                </tr>'+
'		                <tr>'+
'		                    <td>Read More Button Link</td>'+
'		                    <td><input id="gb-image-readmorehref'+count+'" type="text" name="gb-image-readmorehref'+count+'" value="" class="gb-fullwidth" /></td>'+
'		                    <td><p class="howto">Enter the link of the read more button here. Leave blank if none.</p></td>'+
'		                </tr>'+                                                                                    
'		            </tbody>'+    
'		        </table>'+
'		    </div>'+
'		</div>';

		$('#gb-sortables').append(template);

		//TITLE NAME CHANGE
	    $('#gb-item-title'+count).change(function() {
	        var ovaj = $(this);
	        var cale = $(this).parent().parent().parent().parent().parent().parent();
	        if (!ovaj.val() || ovaj.val() == ' ')
	            return;
	        //console.log(cale);
	        $('.hndle span', cale).html(ovaj.val());
	    });		


	    //BUTTON UPLOAD FOO
	    $('.gbuploadb'+count).click(function(e) {

	        e.preventDefault();
	        var send_attachment_bkp = wp.media.editor.send.attachment;
	        var button = $(this);

	        var custom_uploader = wp.media({
	            title: 'Gigabox Image',
	            button: {
	                text: 'Select Gigabox Image'
	            },
	            multiple: false  // Set this to true to allow multiple files to be selected
	        })
	                .on('select', function() {
	            var attachment = custom_uploader.state().get('selection').first().toJSON();
	            
	            if (attachment.sizes.thumbnail)
	               $(button).prev().prev().attr('src', attachment.sizes.thumbnail.url);
	            else 
	                $(button).prev().prev().attr('src', attachment.url);

	            $(button).prev().val(attachment.id);

	            wp.media.editor.send.attachment = send_attachment_bkp;
	        }).open();

	        return false;
	    });    


		//BUTTON REMOVE FOO
	    $('.gbremoveb'+count).click(function(e) {
	        e.preventDefault();
	        var button = $(this);

	        $(button).prev().prev().prev().attr('src', '');
	        $(button).prev().prev().val('');            
	    });	

	    //DELETE IMAGE ITEM
	    $("#gb-delete-item"+count).click(function(e) {
	        e.preventDefault();

	        deletedArray[dAC] = $(this).parent().data("order"); 
	        dAC++; 

	        $("#gb-item-count").val(--item_count);
	        $(this).parent().slideUp(300, function() {$(this).remove(); determine_order(); });   
	    }); 


	    determine_order();

		//increment for next
		item_count++;
		$("#gb-item-count").val(item_count);
		if (item_count==1) {
			$("#gb-single-control").slideDown();	
		} else {
			$("#gb-single-control").slideUp();	
		}

	});

	//call spectrum
	$("#gb-hovcolor-sel").spectrum({
	    showButtons: false,
	    showInput: true,
	    preferredFormat: "hex",
	    color: $("#gb-hovcolor-sel").prev().val(),
	    move: function(color) {
	        $("#gb-hovcolor-sel").prev().val(color.toHexString());
	    },
	    change: function(color) {
		    $("#gb-hovcolor-sel").prev().val(color.toHexString());
		}	    
	});
	$("#gb-bgcolor-sel").spectrum({
	    showButtons: false,
	    showInput: true,
	    preferredFormat: "hex",
	    color: $("#gb-bgcolor-sel").prev().val(),
	    move: function(color) {
	        $("#gb-bgcolor-sel").prev().val(color.toHexString());
	    },
	    change: function(color) {
		    $("#gb-bgcolor-sel").prev().val(color.toHexString());
		}
	});
	$("#gb-txtcolor-sel").spectrum({
	    showButtons: false,
	    showInput: true,
	    preferredFormat: "hex",
	    color: $("#gb-txtcolor-sel").prev().val(),
	    move: function(color) {
	        $("#gb-txtcolor-sel").prev().val(color.toHexString());
	    },
	    change: function(color) {
		    $("#gb-txtcolor-sel").prev().val(color.toHexString());
		}
	});	
	
	function determine_order() {
		var order = '';

		$('.gbboxitem').each(function() {
			order += $(this).data('order') + ', ';
		});

		order = order.slice(0, - 2);
		$("#gb-item-order").val(order);

	}
	var nextStr = 'Maximize All Panels';
	var prevStr = 'Minimize All Panels';

	$("#gbHideAllBoxes").click(function() {
		$(this).html(nextStr);
		$(".gbboxitem .inside").stop().slideToggle(300);
		var swapStr = nextStr;
		nextStr = prevStr;
		prevStr = swapStr;
	})

});