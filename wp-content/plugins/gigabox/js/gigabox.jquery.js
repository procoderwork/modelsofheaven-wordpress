//version 2.5
(function($) {
    $(document).ready(function() {


        var curLoc = window.location,
                curTitle = document.title;
                var openLink=true;
                var socMedia=true;
                var opnlink='';
                if(openLink) opnlink="target='_blank'";
                socmed = "<a href='http://www.facebook.com/sharer.php?u=" + curLoc + "&t=" + curTitle + "' class='gbox-fb'  target='_blank'></a>\n\
                                <a target='_blank' href='http://twitter.com/share?text=Currently reading " + curLoc + "' class='gbox-tw' title='Click to share this post on Twitter'></a>\n\
                                <a href='javascript:void((function()%7Bvar%20e=document.createElement(&apos;script&apos;);e.setAttribute(&apos;type&apos;,&apos;text/javascript&apos;);e.setAttribute(&apos;charset&apos;,&apos;UTF-8&apos;);e.setAttribute(&apos;src&apos;,&apos;http://assets.pinterest.com/js/pinmarklet.js?r=&apos;+Math.random()*99999999);document.body.appendChild(e)%7D)());' class='gbox-pin'></a>\n\
                                <a href='https://plus.google.com/share?url=" + curLoc + "' class='gbox-gg'></a>";
        //init gigabox
        $("body").append("<div class='gbox-bg' data-curgal='' data-actimg=''>\n\
                            <div class='loader'></div><div class='gbox-bg-wrap'><div class='gbox-navi'><div class='gbox-navi-left gbox-navi-arrow'></div><div class='gbox-navi-right gbox-navi-arrow'></div><div class='gbox-navi-close'></div><div class='gbox-increase-img gbox-increase-img-nonclicked'></div><div class='gbox-clear'></div></div>\n\
                                <div class='gbox-bg-imgwrap gbox-bg-imgwrap-imgfw'>\n\
                                </div>\n\
                                <div class='gbox-bg-txtwrap'>\n\
                                </div>\n\
                                <div class='gbox-clear'></div>\n\
                                <div class='gbox-socnet'>"+socmed+" <a href='#' "+opnlink+" class='gbox-readmore'></a></div>\n\
                            </div>\n\
                         </div>");

        var winWidth = $(window).width();
        var winHeight = $(window).height();
        var docHeight = $(document).height();
        var docWidth = $(document).width();
        var fWidth = 0;

        $(".gbox-bg").width(winWidth).height(docHeight);
        var galOpened = false;
        var isMasonry = false;

        $('.gbox-gallery-wrap').each(function() {
            $(this).attr('data-galid', $(this).index());
        });

        $(".gbox-img-hover-wrap").each(function() {
            var hoverColor = $(this).data('hovcolor');
            var icoTmpl = $(this).data('imgtempl');
            var opac = $(this).data('hovopac');
            var titlD = $(".gbox-img-thumb", $(this)).data("title");
            var txtCr = $(".gbox-img-thumb", $(this)).attr('data-txtcolor');
            //$(this).css({"width": $('.gbox-img-thumb', this).width(), "height" : $('.gbox-img-thumb', this).height()});
            if ($(this).parent().data('hovdisp') == 'zoom' || $(this).parent().parent().data('hovdisp') == 'zoom'  || $(this).data('hovdisp') == 'zoom' ) 
                $("<div class='gbox-img-hover' style='background-color:"+hexToRgb(hoverColor, opac)+";'> <div class='gbox-img-hover-sign' style='background-image: url("+icoTmpl+");'></div></div>").appendTo(this);
            else 
                $("<div class='gbox-img-hover' style='background-color:"+hexToRgb(hoverColor, opac)+";'><p class='gboxHoverText' style='color: "+txtCr+";'><span style='border-color: "+txtCr+";'></span>"+titlD+"</p></div>").appendTo(this);
        });

        //$('.gbox-navi-close, .gbox-socnet a').fadeTo(0, 0.5);

        $('.gbox-navi-close, .gbox-navi-arrow, .gbox-socnet a, .gbox-increase-img').hover(function() {
            if ($(this).hasClass('gbox-nonactive'))
                return;
            $(this).stop().addClass("gbox-navi-HoverTrans");
        }, function() {
            if ($(this).hasClass('gbox-nonactive'))
                return;
            $(this).stop().removeClass("gbox-navi-HoverTrans");
        })

        var curWrapWidth = $(".gbox-bg-wrap").width();

        //resize functionality 
        $(window).resize(function() {
            winWidth = $(window).width();
            winHeight = $(window).height();
            docHeight = $(document).height();
            docWidth = $(document).width();
            $(".gbox-bg").width(winWidth).height(docHeight);
            curWrapWidth = $(".gbox-bg-wrap").width();

                if (fWidth > $(".gbox-bg-imgwrap").width())
                    $(".gbox-increase-img").show();
                else 
                    $(".gbox-increase-img").hide();
        });



        //ubaci divove za hover animaciju
        /*$('.gbox-img-thumb').each(function() {
         $(this).wrap('<span class="gbox-single-thumb-wrap" style="width:'+$(this).width()+'px; height:'+$(this).height()+'px;" />')    
         });*/
        //$('.gbox-img-thumb').after('</div>');

        $('.gbox-img-hover-wrap').hover(function() {
            //var cale = $('.gbox-img-thumb').parent();
            //var opac = $(this).data('hovopac') / 100;
            $('.gbox-img-hover', this).css("display", "block").stop().transition({"opacity": 1}, 300);
        }, function() {
            //var cale = $('.gbox-img-thumb').parent();
            $('.gbox-img-hover', this).stop().transition({"opacity": 0}, 300, function() {
                //$(this).css({"display": "none"});
            });
        });

        //upali gigabox, ovde je sva magija
        $(".gbox-img-hover").click(function() {

            var cale = $(this).parent();
            var clickedThumb = $(".gbox-img-thumb", cale);
            var fullImgSrc = clickedThumb.attr('data-fullimg');
            var title = clickedThumb.attr('data-title');
            var desc = clickedThumb.attr('data-desc');
            fWidth = clickedThumb.attr('data-f-width');
            var fHeight = clickedThumb.attr('data-f-height');
            var rmLink = clickedThumb.attr('data-rmlink');
            var rmText = clickedThumb.attr('data-rmtext');
            var bgColr = clickedThumb.attr('data-bgcolor');
            var bgOpc = clickedThumb.attr('data-bgopacity');
            var txtColr = clickedThumb.attr('data-txtcolor');
            var icoTmpl = cale.attr('data-imgtempl');
            var socNetShow = cale.attr('data-socnets');
            
            var topscroll = $(window).scrollTop() + 100;

            if (cale.parent().parent().hasClass("gbox-gallery-wrap")) {
                var wrapdiv = cale.parent().parent();
                var activeImg = clickedThumb.parent().data("msnr");
                isMasonry = true;
            }
            else {
                var wrapdiv = cale.parent();
                var activeImg = $('img', wrapdiv).index(clickedThumb);
                isMasonry = false;
            }
           
            var wrapdivID = wrapdiv.attr('data-galid');

            galOpened = true;

            //OVDE CE TI DODJE ONO GOVNO
            initGallery(activeImg, wrapdivID, $(".gbox-img-thumb", cale));

            //changebg
            $('.gbox-bg').css({'background-color' : hexToRgb(bgColr, bgOpc)});


            /*if ($(this).hasClass('gigabox-gallery-thumb'))
             {
             
             $('.gbox-navi-arrow').fadeTo(0, 0.5)
             $('.gbox-bg').attr('data-curgal', wrapdivID);
             $('.gbox-bg').attr('data-actimg', activeImg);
             
             }*/
            //;


           
            $(".gbox-bg").css({"display": 'block', "top" : "100%"}).transition({top: 0, opacity: 1}, 400, function() {
                $('.loader').show();
                //$(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                //$(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");

                if (title && desc) {
                    $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                    $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                }
                else if (!title && !desc) {
                    //alert('dejo')
                    $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                    $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');
                    if ($(".gbox-bg-wrap").width() < fWidth) {
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    }
                    else {
                        $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    }
                } else if (title && !desc) {
                    //alert('dejo')
                    $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                    $(".gbox-bg-txtwrap").addClass('gbox-bg-txtwrap-fw').append("<h1>" + title + "</h1>");
                    $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');

                    if ($(".gbox-bg-wrap").width() < fWidth) {
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    }
                    else {
                        $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    }
                    // $(".gbox-bg-wrap").css({"width" : 0});

                } else {
                    $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                    $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                }
                
                //TOGGLE DISPLAY OF READ MORE
                if (rmText && rmText !=="") {
                    $(".gbox-readmore").attr('href', rmLink).html(rmText).css({'visibility' : 'visible'}); 
                } 
                else {
                    $(".gbox-readmore").css({'visibility' : 'hidden'});   
                }

                //TOGGLE DISPLAY OF SOCIAL NETWORKS
                if (socNetShow =="y") {
                    $(".gbox-socnet").css({'visibility' : 'visible'}); 
                } 
                else {
                    $(".gbox-socnet").css({'visibility' : 'hidden'});   
                }


                //CHANGE FONT TEXT AND ICONS
                $('.gbox-bg-txtwrap h1, .gbox-bg-txtwrap p').css({'color' : txtColr});
                $('.gbox-readmore').css({'color' : txtColr, 'border-color' : txtColr});
                $('.gbox-navi-close, .gbox-navi-left, .gbox-navi-left-inactive, .gbox-navi-right, .gbox-navi-right-inactive, .gbox-fb, .gbox-tw, .gbox-pin, .gbox-gg, .gbox-increase-img').css({'background-image' : 'url('+icoTmpl+')'});

                $('.gbox-fullimg').load(function() {

                    $('.loader').hide();
                    $(".gbox-bg-wrap").css({"display": 'block', opacity: 0, top: topscroll});

                    //show resizy
                    if (fWidth > $(".gbox-bg-imgwrap").width())
                        $(".gbox-increase-img").show();

                    $(".gbox-bg-wrap").transition({top: (topscroll-50), opacity: 1}, 600, function() {

                        var winWidth = $(window).width();
                        var winHeight = $(window).height();
                        var docHeight = $(document).height();

                        $(".gbox-bg").width(winWidth).height(docHeight);
                    });
                });
            });
        });

        function gbCloseOverlay() {
            if (galOpened == false)
                return;

            var topscroll = $(window).scrollTop() + 100;

            $(".gbox-bg-wrap").transition({top: (topscroll), opacity: 0}, 400, function() {
                $(".gbox-bg").transition({"opacity" : "0"}, 200, function() {

                    $(".gbox-bg-wrap").css({"max-width": ""});
                    $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    $(".gbox-bg").css({"display": 'none', opacity: 0});
                    $(this).css({"display": 'none', opacity: 0})
                    $(".gbox-bg-imgwrap").html('');
                    $(".gbox-bg-txtwrap").html('');
                    $(".gbox-readmore").attr('href', '').css({'visibility' : 'hidden'}); 
                    $('.gbox-navi-arrow, .gbox-navi-left-inactive, .gbox-navi-right-inactive').hide();
                    $('.gbox-bg').attr('data-curgal', '');
                    $('.gbox-bg').attr('data-actimg', '');
                    $('.gbox-bg-imgwrap-fw').removeClass('gbox-bg-imgwrap-fw');
                    $(".gbox-bg-txtwrap-fw").removeClass('gbox-bg-txtwrap-fw');
                    $(".gbox-increase-img").hide();
                    fWidth = 0;
                    galOpened = false;
                });
            });
        };

        function gbMoveRight() {
            var currentGal = parseInt($('.gbox-bg').attr('data-curgal'));
            var currentImg = parseInt($('.gbox-bg').attr('data-actimg'));
            var curgalSel = $('[data-galid="' + currentGal + '"] .gigabox-gallery-thumb');
            
            //var topscroll = $(window).scrollTop() + 150;

            var imageCount = curgalSel.length;
            //var tatkoCountMason =;
            // console.log(tatkoCountMason);

            currentImg++;

            if (currentImg == imageCount || galOpened == false)
                return;
            else {
                if (isMasonry) {
                    var disGalbro =  $('[data-galid="' + currentGal + '"]');
                    var CURRENT_PROCESSED_IMG = $('[data-msnr="' + currentImg + '"]', disGalbro).data("indx"); //double wat :C
                    //console.log(CURRENT_PROCESSED_IMG);
                }
                else 
                    CURRENT_PROCESSED_IMG = currentImg;

                 //console.log(isMasonry);
                $(".gbox-bg-wrap").transition({"left" : "-200px", opacity: 0}, 300, function() {
                    $(".gbox-bg-imgwrap").html('');
                    $(".gbox-bg-txtwrap").html('');
                    $('.gbox-bg-imgwrap-fw').removeClass('gbox-bg-imgwrap-fw');
                    $(".gbox-bg-txtwrap-fw").removeClass('gbox-bg-txtwrap-fw');
                    $('.gbox-navi-arrow');
                    $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    $(".gbox-bg-wrap").css({"max-width": ""})
                    $(".gbox-readmore").attr('href', '').css({'visibility' : 'hidden'}); 
                    $(".gbox-increase-img").hide();

                    var fullImgSrc = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-fullimg');
                    var title = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-title');
                    var desc = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-desc');
                    var fWidth = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-f-width');
                    var fHeight = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-f-height');
                    var rmLink = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-rmlink');
                    var rmText = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-rmtext');
                    var txtColr = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-txtcolor');


                    //alert(.attr('data-desc'));
                    $('.loader').show();
                    if (title && desc) {
                        $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                    }
                    else if (!title && !desc) {
                        //alert('dejo')
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');
                        if ($(".gbox-bg-wrap").width() < fWidth) {
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw')
                        }
                        else {
                            $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                        }
                    } else if (title && !desc) {
                        //alert('dejo')
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").addClass('gbox-bg-txtwrap-fw').append("<h1>" + title + "</h1>");
                        $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');

                        if ($(".gbox-bg-wrap").width() < fWidth) {
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw')
                        }
                        else {
                            $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                        }
                        // $(".gbox-bg-wrap").css({"width" : 0});

                    } else {
                        $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                    }
                    
                    // read more text
                    if (rmText && rmText !=="") {
                        $(".gbox-readmore").attr('href', rmLink).html(rmText).css({'visibility' : 'visible'}); 
                    } 
                    else {
                        $(".gbox-readmore").css({'visibility' : 'hidden'});   
                    }
                    
                    //CHANGE FONT TEXT AND ICONS
                    $('.gbox-bg-txtwrap h1, .gbox-bg-txtwrap p').css({'color' : txtColr});
                    $('.gbox-readmore').css({'color' : txtColr, 'border-color' : txtColr});

                    $('.gbox-fullimg').load(function() {
                        $('.loader').hide();
                        initGallery(currentImg, currentGal, curgalSel.eq(currentImg));

                        if (fWidth > $(".gbox-bg-imgwrap").width())
                            $(".gbox-increase-img").show();


                        $(".gbox-bg-wrap").transition({left: 200}, 50, function() {
                            $(".gbox-bg-wrap").transition({left: 0, opacity: 1}, 550, function() {
                                var winWidth = $(window).width();
                                var winHeight = $(window).height();
                                var docHeight = $(document).height();

                                $(".gbox-bg").width(winWidth).height(docHeight);
                            });
                        });
                    });

                });
            }

        };

        function gbMoveLeft() {
            var currentGal = parseInt($('.gbox-bg').attr('data-curgal'));
            var currentImg = parseInt($('.gbox-bg').attr('data-actimg'));
            var curgalSel = $('[data-galid="' + currentGal + '"] .gigabox-gallery-thumb');

            //var topscroll = $(window).scrollTop() + 150;

            var imageCount = curgalSel.length;

            //currentImg++;

            if (currentImg == 0 || galOpened == false)
                return;
            else {
                currentImg--;
                if (isMasonry) {
                    var disGalbro =  $('[data-galid="' + currentGal + '"]');
                    var CURRENT_PROCESSED_IMG = $('[data-msnr="' + currentImg + '"]', disGalbro).data("indx"); //double wat :C
                    //console.log(CURRENT_PROCESSED_IMG);
                }
                else 
                    CURRENT_PROCESSED_IMG = currentImg;

                $(".gbox-bg-wrap").transition({left: 200, opacity: 0}, 300, function() {
                    $(".gbox-bg-imgwrap").html('');
                    $(".gbox-bg-txtwrap").html('');
                    $('.gbox-bg-imgwrap-fw').removeClass('gbox-bg-imgwrap-fw');
                    $(".gbox-bg-txtwrap-fw").removeClass('gbox-bg-txtwrap-fw');
                    $('.gbox-navi-arrow');
                    $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                    $(".gbox-bg-wrap").css({"max-width": ""})
                    $(".gbox-readmore").attr('href', '').css({'visibility' : 'hidden'}); 
                    $(".gbox-increase-img").hide();

                    var fullImgSrc = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-fullimg');
                    var title = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-title');
                    var desc = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-desc');
                    var fWidth = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-f-width');
                    var fHeight = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-f-height');
                    var rmLink = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-rmlink');
                    var rmText = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-rmtext');
                    var txtColr = curgalSel.eq(CURRENT_PROCESSED_IMG).attr('data-txtcolor');

                    //alert(.attr('data-desc'));
                    $('.loader').show();
                    if (title && desc) {
                        $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                    }
                    else if (!title && !desc) {
                        //alert('dejo')
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');
                        if ($(".gbox-bg-wrap").width() < fWidth) {
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw')
                        }
                        else {
                            $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                        }
                    } else if (title && !desc) {
                        //alert('dejo')
                        $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-fw').append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").addClass('gbox-bg-txtwrap-fw').append("<h1>" + title + "</h1>");
                        $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');

                        if ($(".gbox-bg-wrap").width() < fWidth) {
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw')
                        }
                        else {
                            $(".gbox-bg-wrap").css({"max-width": fWidth + "px"});
                            $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                        }
                        // $(".gbox-bg-wrap").css({"width" : 0});

                    } else {
                        $(".gbox-bg-imgwrap").append("<img src='" + fullImgSrc + "' alt='" + title + "' class='gbox-fullimg' />");
                        $(".gbox-bg-txtwrap").append("<h1>" + title + "</h1><br/><br/><br/><p>" + desc + "</p>");
                    }
                    
                    if (rmText && rmText !=="") {
                        $(".gbox-readmore").attr('href', rmLink).html(rmText).css({'visibility' : 'visible'}); 
                    } 
                    else {
                        $(".gbox-readmore").css({'visibility' : 'hidden'});   
                    }


                    //CHANGE FONT TEXT AND ICONS
                    $('.gbox-bg-txtwrap h1, .gbox-bg-txtwrap p').css({'color' : txtColr});
                    $('.gbox-readmore').css({'color' : txtColr, 'border-color' : txtColr});


                    $('.gbox-fullimg').load(function() {
                        $('.loader').hide();
                        initGallery(currentImg, currentGal, curgalSel.eq(currentImg));

                        if (fWidth > $(".gbox-bg-imgwrap").width())
                            $(".gbox-increase-img").show();

                        //console.log("trenutni indes:"+currentImg+" ukupno:");
                        $(".gbox-bg-wrap").transition({left: -200}, 50, function() {
                            $(".gbox-bg-wrap").transition({left: 0, opacity: 1}, 550, function() {
                                var winWidth = $(window).width();
                                var winHeight = $(window).height();
                                var docHeight = $(document).height();

                                $(".gbox-bg").width(winWidth).height(docHeight);
                            });
                        });
                    });

                });
            }

        };


        var increaseClicked = false;
        var isSingle = false;
        function gbIncreaseImg() { 

            if (increaseClicked) {
                increaseClicked = false;
                $(".gbox-bg-wrap").transition({"opacity" : 0}, 300, function() {
                     $(".gbox-bg-imgwrap").addClass('gbox-bg-imgwrap-imgfw');
                     $(".gbox-bg-wrap").css("margin", "0 auto");
                     $(".gbox-increase-img").css("float", "right");
                     //$(".gbox-bg-imgwrap, .gbox-fullimg, .gbox-increase-img").transition({"opacity" : 1}, 300);
                     $(".gbox-increase-img").removeClass("gbox-increase-img-clicked").addClass("gbox-increase-img-nonclicked");
                     if (isSingle)
                        $(".gbox-navi-close, .gbox-bg-txtwrap, .gbox-socnet").css({"display" : "block", "opacity" : 1});
                     else
                        $(".gbox-navi-arrow, .gbox-navi-close, .gbox-bg-txtwrap, .gbox-socnet").css({"display" : "block", "opacity" : 1});

                     $(".gbox-bg").width(winWidth).height(docHeight);

                     $(".gbox-bg-wrap").transition({"opacity" : 1}, 300);
                });
            } else {
                increaseClicked = true;
                if ($(".gbox-navi-left").css("display") == 'none') 
                    isSingle = true;
                else 
                    isSingle = false; 
                
                $(".gbox-bg-imgwrap, .gbox-fullimg, .gbox-increase-img").transition({"opacity" : 0}, 300);
                $(".gbox-navi-arrow, .gbox-navi-close, .gbox-bg-txtwrap, .gbox-socnet").transition({"opacity" : 0}, 300, function() {
                    $(this).css("display", "none");
                    $(".gbox-bg-imgwrap-imgfw").removeClass('gbox-bg-imgwrap-imgfw');
                    $(".gbox-bg-wrap").css("margin", "0");
                    $(".gbox-increase-img").css("float", "left");
                    $(".gbox-increase-img").removeClass("gbox-increase-img-nonclicked").addClass("gbox-increase-img-clicked");
                    $(".gbox-bg-imgwrap, .gbox-fullimg, .gbox-increase-img").transition({"opacity" : 1}, 300);
                    $(".gbox-bg").width(docWidth).height(docHeight);
                    
                });
            }
        };

        $(".gbox-navi-left").click(gbMoveLeft);
        $(".gbox-navi-right").click(gbMoveRight);
        $(".gbox-navi-close").click(gbCloseOverlay);
        $(".gbox-increase-img").click(gbIncreaseImg)

        function initGallery(curIndex, curGal, dis) {
            if (dis.hasClass('gigabox-gallery-thumb'))
            {
                /*var wrapdiv = $(this).parent();
                 var wrapdivID = wrapdiv.attr('data-galid');
                 var activeImg = $('img', wrapdiv).index(this);*/
                //$('.gbox-navi-arrow').fadeTo(0, 0.5)
                $('.gbox-bg').attr('data-curgal', curGal);
                $('.gbox-bg').attr('data-actimg', curIndex);

                // $('.gbox-navi-left').fadeTo(0, 0.5);

                var curgalSel = $('[data-galid="' + curGal + '"] .gigabox-gallery-thumb');

                var imageCount = curgalSel.length;

                if (curIndex == 0)
                {
                    $('.gbox-navi-left').addClass('gbox-nonactive').fadeTo(0, 0.2);
                    //$('.gbox-navi-left-inactive').fadeTo(0, 0.2);
                }
                else
                {
                    $('.gbox-navi-left').removeClass('gbox-nonactive').fadeTo(0, 1);
                    //$('.gbox-navi-left-inactive').hide();
                }

                imageCount--;
                if (curIndex >= imageCount)
                {
                    $('.gbox-navi-right').addClass('gbox-nonactive').fadeTo(0, 0.2);
                    //$('.gbox-navi-right-inactive').fadeTo(0, 0.2);
                }
                else
                {
                    $('.gbox-navi-right').removeClass('gbox-nonactive').fadeTo(0, 1);
                    //$('.gbox-navi-right-inactive').hide();
                }
            }
        }

        function hexToRgb(hex, opacity) {
            // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function(m, r, g, b) {
                return r + r + g + g + b + b;
            });

            opacity = opacity/100;

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

            var retrn = 'rgba('+parseInt(result[1], 16)+', '+parseInt(result[2], 16)+', '+parseInt(result[3], 16)+', '+opacity+')';
            return retrn;
           /* return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;*/
        }

        $(document).keyup(function(ev) {
            if (ev.which == 39) 
                gbMoveRight();
            else if (ev.which == 37) 
                gbMoveLeft();
            else if (ev.which == 27 && increaseClicked == false) 
                gbCloseOverlay();
            else if (ev.which == 27 && increaseClicked == true) 
                gbIncreaseImg();
        });


    });
})(jQuery);