jQuery(document).ready(function($) {

    $(".delete-pgallery").click(function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        var oldlink = $('.pggodelete').attr('href');
        
        var newlink = oldlink.slice(0, - 1) + id;
        $('.pggodelete').attr('href', newlink);
        $("#pg-deletebox").show();

    });

    $(".pg-dontdelete").click(function(e) {
        e.preventDefault();
        $("#pg-deletebox").fadeOut();
    });

});