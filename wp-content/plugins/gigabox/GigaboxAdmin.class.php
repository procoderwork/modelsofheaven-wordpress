<?php 

class GigaboxAdmin
{

	public $path, $version, $file, $table_name_suffix, $includePath;
	
	function __construct($file, $version) 
	{
		$this->version = $version;
		$this->file = $file;
		$this->path = plugins_url('gigabox');
		$this->includePath = dirname(__FILE__);
		$this->table_name_suffix = 'gigabox';

		$this->init();
	}


	function init() {
	    register_activation_hook( $this->file, array($this, 'gigaboxInstall') );

	    add_image_size('gigabox-gal-thumb', 300, 300, true); //(cropped)
	    add_image_size('gigabox-gal-mason', 300, 999, false); //(cropped)

	    if (is_admin())
	        add_action( 'admin_menu', array($this, 'adminMenuInit') );   

	    require_once $this->includePath  . '/GigaboxFrontend.class.php';	
	    $gbfr = new GigaboxFrontend ($this->path, $this->includePath, $this->table_name_suffix); 
	}


	function gigaboxInstall() {
        global $wpdb;
        $table_name = $wpdb->prefix . $this->table_name_suffix; 

        $sql = "CREATE TABLE $table_name (
        id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
        name TINYTEXT NOT NULL,
        items TEXT DEFAULT '',
        PRIMARY KEY  (id)
        ) COLLATE utf8_general_ci;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
        
        add_option( "gigabox_db_version", $this->version );
	}


	function adminMenuInit() {

	    $mainmenu = add_menu_page( 'Gigabox', 'Gigabox', 'manage_options', 'gigabox', array( $this, 'gigaboxListInterface' ) , '', '100.72' );
	    $submenu = add_submenu_page( 'gigabox', 'Gigabox', 'Add New', 'manage_options', 'gigabox-edit', array(&$this, 'gigaboxEditInterface'));

	    add_action('load-'.$mainmenu, array(&$this, 'adminScriptsList'));
	    add_action('load-'.$submenu, array(&$this, 'adminScriptsEdit'));  

	}	


	function gigaboxListInterface() {
		include_once($this->includePath . '/templates/gigabox-list.php');
	}


	function gigaboxEditInterface() {
		include_once($this->includePath . '/templates/gigabox-edit.php');
	}


	function adminScriptsList() {

	    wp_enqueue_script('jquery');

	    wp_enqueue_style( 'gigabox-backend', $this->path . '/css/gigabox-backend-list.css');
	    wp_enqueue_script('gigabox-backend', $this->path . '/js/gigabox-backend-list.js');

	}


	function adminScriptsEdit() {

	    wp_enqueue_script('jquery');
	    wp_enqueue_script('post');
	    wp_enqueue_media();

	    wp_enqueue_style( 'gigabox-backend', $this->path . '/css/gigabox-backend-edit.css');
	    wp_enqueue_script('gigabox-backend', $this->path . '/js/gigabox-backend.js');

	    wp_enqueue_style( 'spectrum', $this->path . '/css/spectrum.css');
	    wp_enqueue_script('spectrum', $this->path . '/js/spectrum.js');
	}

	function saveToDb($post, $id) {
    //var_dump($_POST);

	    global $wpdb;
	    $table_name = $wpdb->prefix . $this->table_name_suffix; 

	    $settings = array();
	    $settings["title"] = sanitize_text_field($post['post_title']);
	    $settings["options"]["item_count"] = (int) $post['gb-item-count'];
	    $settings["options"]["item_order"] = explode(', ', $post['gb-item-order']);
	    $settings["options"]["first-save"] = $post['gb-first-save'];

	    $settings["options"]["columns"] = sanitize_text_field($post['gb-columns']);
	    $settings["options"]["hovcolor"] = $post['gb-hovcolor'];
	    $settings["options"]["hovopacity"] = sanitize_text_field($post['gb-hovopacity']);
	    $settings["options"]["bgcolor"] = $post['gb-bgcolor'];
	    $settings["options"]["bgopacity"] = sanitize_text_field($post['gb-bgopacity']);
	    $settings["options"]["txtcolor"] = $post['gb-txtcolor'];
	    $settings["options"]["iconsid"] = $post['gb-icontmplt'];
	    $settings["options"]["socnet"] = $post['gb-socnet-disp'];
	    $settings["options"]["hovdisp"] = $post['gb-hov-disp'];
	    $settings["options"]["gblayoutdisp"] = $post['gb-layout-disp'];

	    $settings["options"]["singlewidth"] = $post['gb-single-width'];
	    $settings["options"]["singleheight"] = $post['gb-single-height'];

	    for ($i = 0; $i < $settings["options"]["item_count"]; $i++) {

	        //get current item (stored in item_order, created from js)
	        $current = $settings["options"]["item_order"][$i];

	        $settings["items"][$i]["title"] = stripslashes_deep($post["gb-item-title" . $current]);
	        $settings["items"][$i]["description"] = stripslashes_deep($post["gb-item-desc" . $current]);
	        $settings["items"][$i]["image_id"] = stripslashes_deep($post["gb-image-id" . $current]);
	        $settings["items"][$i]["readmore"] = stripslashes_deep($post["gb-image-readmore" . $current]);
	        $settings["items"][$i]["readmorehref"] = stripslashes_deep($post["gb-image-readmorehref" . $current]);
	    }
	    //var_dump($settings);
	    //var_dump($post);
	    $serialized_sets = serialize($settings);


	    if ($settings["options"]["first-save"] == "N") {
	        $rows_affected = $wpdb->update($table_name, array('name' => $settings["title"], 'items' => $serialized_sets), array('id' => $id));
	        //$message = $this->message(2);
	    }
	    else {
	        $rows_affected = $wpdb->insert($table_name, array('name' => $settings["title"], 'items' => $serialized_sets));
	        //$message = $this->message(0);
	    }

    	return $settings;	
	}


	function loadFromDb($id) {

	    global $wpdb;
	    $table_name = $wpdb->prefix . $this->table_name_suffix; 

		$currentid = $id;
		$results = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $currentid");
		$settings = unserialize($results->items);

		return $settings;

	}

    function message($nr) {
        switch ($nr) {
            case 0:
                echo '<div id="message" class="updated below-h2"><p>New Gigabox Gallery Created</p></div>';
                break;
            case 1:
                echo '<div id="message" class="updated below-h2"><p>Gigabox Gallery Deleted</p></div>';
                break;
            case 2:
                echo '<div id="message" class="updated below-h2"><p>Gigabox Gallery Updated</p></div>';
                break;  
            case 3:  
            	echo '<div id="message" class="updated below-h2"><p>Thumbnails Recreated.</p></div>';
           		break;              
        }
    }

	function getCurrentId() {

	    global $wpdb;
	    $table_name = $wpdb->prefix . $this->table_name_suffix;
	    $result = $wpdb->get_results("SHOW TABLE STATUS WHERE name = '$table_name'");

	    return $result[0]->Auto_increment;
	}	
}

?>