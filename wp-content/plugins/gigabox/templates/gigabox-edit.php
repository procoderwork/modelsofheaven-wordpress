<?php
if (isset($_GET['id'])) {
    $messageTitle = '<h2>Edit Gigabox Gallery</h2>';
    $ID = $_GET['id'];
}
else {
    $messageTitle = '<h2>Add Gigabox Gallery</h2>';
    $ID = $this->getCurrentId();
}
    //if (isset($message)) echo $message;
?>


<?php

//handle SAVE and LOAD
if (isset($_POST['gb-form-submitted']) && $_POST['gb-form-submitted'] == 'Y') {

    //SAVE TO DATABASE
    $settings = $this->saveToDb($_POST, $ID);

}
else if (isset($_GET['id']) && !isset($_POST['gb-form-submitted'])) {

    //LOAD FROM DATABASE
    $settings = $this->loadFromDb($_GET['id']);

}
?>


<div class="wrap">
<?php
    echo $messageTitle;

    if (isset($settings) && $settings["options"]["first-save"] == "N" && isset($_POST['gb-form-submitted']) ) {
        echo $this->message(2);
    }
    elseif (isset($settings) && $settings["options"]["first-save"] == "Y"  && isset($_POST['gb-form-submitted']) ) {
        echo $this->message(0);
    }
?>

    <form id="post" method="post" action="<?php echo admin_url('admin.php?page=gigabox-edit&id=' . $ID); ?>" name="post">
        <div id="poststuff">
            <div id="post-body" class="metabox-holder columns-2">

                <div id="post-body-content">
                    <div id="titlediv">
                        <div id="titlewrap">
                            <label id="title-prompt-text" class="" for="title"></label>
                            <input id="title" type="text" autocomplete="off" value="<?php if (isset($settings['title'])) echo $settings['title']; ?>" size="30" name="post_title">
                        </div>
                    </div>
                    <input type="hidden" id="gb-form-submitted" name="gb-form-submitted"  value="Y" />
                    <input type="hidden" id="gb-item-order" name="gb-item-order"  value="0" />
                    <input type="hidden" id="gb-item-count" name="gb-item-count"  value="0" />
                    <input type="hidden" id="gb-first-save" name="gb-first-save"  value="<?php if (isset($_GET['id']) || isset($settings["options"]["first-save"] ) ) echo "N"; else echo "Y" ?>" />
                    <br/><a href="#" class="button button-primary" id="gbHideAllBoxes">Minimize All Panels</a>
                    <div id="gb-sortables" class="">

                        <?php
                        if (isset($settings["items"])) {
                            //var_dump($settings);
                            $count = 0;
                            foreach ($settings["items"] as $item) {
                        ?>

                        <div class="postbox gbboxitem gbboxcnt<?php echo $count; ?>" data-order="<?php echo $count; ?>">
                           <h4 class="hndler"><span><?php if (isset($item["title"]) && $item["title"] != '') echo $item["title"]; else echo 'Untitled'; ?></span></h4><div class="gb-delete-item" id="gb-delete-item<?php echo $count; ?>">Delete this item</div>
                           <div class="inside">
                               <table class="gbimageitem">
                                   <thead>
                                       <tr>
                                           <th width="20%"></th>
                                           <th width="50%"></th>
                                           <th width="30%"></th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                       <tr>
                                           <td>Image Title</td>
                                           <td><input id="gb-item-title<?php echo $count; ?>" type="text" name="gb-item-title<?php echo $count; ?>" value="<?php if (isset($item["title"])) echo $item["title"];?>" class="gb-fullwidth gb-imgtitle" /></td>
                                           <td><p class="howto">Enter the title of this image.</p></td>
                                       </tr>
                                       <tr>
                                           <td>Image Description</td>
                                           <td><textarea id="gb-item-desc<?php echo $count; ?>" name="gb-item-desc<?php echo $count; ?>" class="gb-description"><?php if (isset($item["description"])) echo $item["description"];?></textarea></td>
                                           <td><p class="howto">Enter the description of this image.</p></td>
                                       </tr>
                                       <tr>
                                           <td>Image</td>
                                            <?php 
                                                   $imageSrc = '';
                                                if ($item["image_id"])  {
                                                    $imageSrc = wp_get_attachment_image_src( $item["image_id"], 'thumbnail' );
                                                    $imageSrc = $imageSrc[0]; 
                                                  }
                                            ?>                                           
                                           <td class="gb-upload-td">
                                               <img class="gb-custom_media_image" src="<?php echo $imageSrc; ?>" />
                                             
                                               <input class="" id="gb-image-id<?php echo $count; ?>" type="hidden" name="gb-image-id<?php echo $count; ?>" value="<?php if(isset($item["image_id"])) echo $item["image_id"]; ?>">
                                               <a href="#" class="button gbuploadb">Upload</a>
                                               <a href="#" class="button gbremoveb">Remove</a>
                                           </td>
                                           <td><p class="howto">Select the image.</p></td>
                                       </tr>
                                       <tr>
                                           <td>Read More Button Text</td>
                                           <td><input id="gb-image-readmore<?php echo $count; ?>" type="text" name="gb-image-readmore<?php echo $count; ?>" value="<?php if(isset($item["readmore"])) echo $item["readmore"]; ?>" class="gb-fullwidth" /></td>
                                           <td><p class="howto">Enter the text of the read more button here. Leave blank if none.</p></td>
                                       </tr>
                                       <tr>
                                           <td>Read More Button Link</td>
                                           <td><input id="gb-image-readmorehref<?php echo $count; ?>" type="text" name="gb-image-readmorehref<?php echo $count; ?>" value="<?php if(isset($item["readmorehref"])) echo $item["readmorehref"]; ?>" class="gb-fullwidth" /></td>
                                           <td><p class="howto">Enter the link of the read more button here. Leave blank if none.</p></td>
                                       </tr>                                                                                    
                                   </tbody>    
                               </table>
                           </div>
                        </div>

                        <?php 
                                $count++;
                            }
                        }
                        ?>

                    </div>

                    <div class="gbclear"></div>
                    <a class="button button-primary gb-addnewitem" href="#">Add New Item</a>

                </div>

                <div id="postbox-container-1" class="postbox-container">
                    <div id="" class="">
                        <div id="submitdiv" class="postbox pgsubmitbox">
                            <div class="handlediv" title="Click to toggle">
                                <br>
                            </div>
                            <h3 class="hndle"><span>Publish</span></h3>
                            <div class="inside <?php if(!isset($_GET["id"])) echo 'pg-no-delete'; ?>" >
                                <input name="save-pgrid" id="save-pgrid" value="<?php if(isset($_GET["id"])) echo 'Update Gallery'; else echo "Save Gallery"; ?>" class="button button-primary" type="submit" />
                                <?php if(isset($_GET["id"])) { ?><a href="#" class="button" id="delete-pgrid">Delete</a><?php } ?>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div id="" class="postbox pgsubmitbox">
                            <div class="handlediv" title="Click to toggle">
                                <br>
                            </div>
                            <h3 class="hndle"><span>Shortcode</span></h3>
                            <div class="inside" >
                                <p style="font-size: 16px; text-align: center;">[gigabox id="<?php echo $ID; ?>"]</p>
                                <?php if(isset($_GET["id"])) { ?>
                                <p class="howto">This is the shortcode of this Gigabox Gallery. You can paste it into your posts and pages and this Gigabox Gallery will appear.</p>
                                <?php } else {  ?>
                                <p class="howto">This will be the shortcode for this Gigabox Gallery after you save it. You can then paste it into your posts and pages and this Gigabox Gallery will appear.</p>
                                <?php } ?>
                            </div>
                        </div>

                        <div id="gb-single-control" class="postbox pgsubmitbox">
                            <div class="handlediv" title="Click to toggle">
                                <br>
                            </div>
                            <h3 class="hndle"><span>Single Image Settings</span></h3>
                            <div class="inside" >
                                <table class="pg-side-table"> 
                                    <thead>
                                        <tr>
                                            <th width="50%"></th>
                                            <th width="50%"></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    <tr>
                                        <td>Image Width</td>
                                        <td><input id="gb-single-width" class="" type="text" value="<?php if (isset($settings["options"]["singlewidth"])) echo $settings["options"]["singlewidth"]; ?>" autocomplete="off" name="gb-single-width"></td>

                                    </tr>
                                    <tr>
                                        <td>Image Height</td>
                                        <td><input id="gb-single-height" class="" type="text" value="<?php if (isset($settings["options"]["singleheight"])) echo $settings["options"]["singleheight"]; ?>" autocomplete="off" name="gb-single-height"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">If there is only one image in this gallery, you can control the size of the thumbnail here. Values are entered in pixels. Enter numerical value (WIHTOUT px at the end).</p></td></tr>
                                    </tbody>
                                </table>                              
                            </div>
                        </div>

                        <div class="postbox ">
                            <div class="handlediv" title="Click to toggle">
                                <br>
                            </div>
                            <h3 class="hndle"><span>General Options</span></h3>
                            <div class="inside">
                                <table class="pg-side-table"> 
                                    <thead>
                                        <tr>
                                            <th width="50%"></th>
                                            <th width="50%"></th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                    <tr>
                                        <td>Number of Columns:</td>
                                        <td><input id="gb-columns" class="" type="text" value="<?php if (isset($settings["options"]["columns"])) echo $settings["options"]["columns"]; else echo '4'; ?>" autocomplete="off" name="gb-columns"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Enter how many images should be in each row (i.e. number of columns).</p></td></tr>
                                    <tr>
                                        <td>Hover Color:</td>
                                        <td><input id="gb-hovcolor" type="hidden" value="<?php if (isset($settings["options"]["hovcolor"])) echo $settings["options"]["hovcolor"]; else echo '#000000'; ?>" name="gb-hovcolor"><input id="gb-hovcolor-sel" name="gb-hovcolor-sel"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select the color of hover effect which is displayed when user hovers over an image.</p></td></tr>
                                    <tr>
                                        <td>Hover Opacity</td>
                                        <td><input id="gb-hovopacity" class="" type="text" value="<?php if (isset($settings["options"]["hovopacity"])) echo $settings["options"]["hovopacity"]; else echo '80'; ?>" autocomplete="off" name="gb-hovopacity"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select the opacity of hover effect which is displayed when user hovers over an image. Values should be in in range 0 - 100.</p></td></tr>   
                                    <tr>
                                        <td>Background Color:</td>
                                        <td><input id="gb-bgcolor" type="hidden" value="<?php if (isset($settings["options"]["bgcolor"])) echo $settings["options"]["bgcolor"]; else echo '#000000'; ?>"  name="gb-bgcolor"><input id="gb-bgcolor-sel" class="" name="gb-bgcolor-sel"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select the color of background overlay that is shown when user clicks on an image.</p></td></tr>
                                    <tr>
                                        <td>Background Opacity</td>
                                        <td><input id="gb-bgopacity" type="text" value="<?php if (isset($settings["options"]["bgopacity"])) echo $settings["options"]["bgopacity"]; else echo '80'; ?>" autocomplete="off" name="gb-bgopacity"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select the opacity of background overlay that is shown when user clicks on an image. Values should be in in range 0 - 100.</p></td></tr>           
                                    <tr>
                                        <td>Text Color:</td>
                                        <td><input id="gb-txtcolor" type="hidden" value="<?php if (isset($settings["options"]["txtcolor"])) echo $settings["options"]["txtcolor"]; else echo '#FFFFFF'; ?>"  name="gb-txtcolor"><input id="gb-txtcolor-sel" class="" name="gb-txtcolor-sel"></td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select the color of title and description for each image.</p></td></tr>
                                    <tr>
                                        <td>Select Gallery Layout</td>
                                        <td>
                                          <select id="gb-layout-disp" name="gb-layout-disp">
                                            <option value="sq" <?php if (isset($settings["options"]["gblayoutdisp"]) && $settings["options"]["gblayoutdisp"] == 'sq') echo 'selected'; ?> >Square</option>
                                            <option value="sqm" <?php if (isset($settings["options"]["gblayoutdisp"]) && $settings["options"]["gblayoutdisp"] == 'sqm') echo 'selected'; ?> >Square Margin</option>
                                            <option value="ms" <?php if (isset($settings["options"]["gblayoutdisp"]) && $settings["options"]["gblayoutdisp"] == 'ms') echo 'selected'; ?> >Masonry</option>
                                            <option value="msm" <?php if (isset($settings["options"]["gblayoutdisp"]) && $settings["options"]["gblayoutdisp"] == 'msm') echo 'selected'; ?> >Masonry Margin</option>
                                          </select>
                                        </td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select what will be shown when a user hovers over a thubmnail in this gallery.</p></td></tr>
                                    <tr>
                                        <td>Thumbnail hover event:</td>
                                        <td>
                                          <select id="gb-hov-disp" name="gb-hov-disp">
                                            <option value="title" <?php if (isset($settings["options"]["hovdisp"]) && $settings["options"]["hovdisp"] == 'title') echo 'selected'; ?> >Display Image Title</option>
                                            <option value="zoom" <?php if (isset($settings["options"]["hovdisp"]) && $settings["options"]["hovdisp"] == 'zoom') echo 'selected'; ?> >Display Zoom icon</option>
                                          </select>
                                        </td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Select what will be shown when a user hovers over a thubmnail in this gallery.</p></td></tr>
                                    <tr>
                                        <td>Display Social Network Icons:</td>
                                        <td>
                                          <select id="gb-socnet-disp" name="gb-socnet-disp">
                                            <option value="y" <?php if (isset($settings["options"]["socnet"]) && $settings["options"]["socnet"] == 'y') echo 'selected'; ?> >Yes</option>
                                            <option value="n" <?php if (isset($settings["options"]["socnet"]) && $settings["options"]["socnet"] == 'n') echo 'selected'; ?> >No</option>
                                          </select>
                                        </td>

                                    </tr>
                                    <tr><td colspan="2"><p class="howto">Choose wheter you want to hide or show the social network sharing buttons.</p></td></tr>
                                    <tr>
                                        <td>Icons Template:</td>
                                        <?php 
                                          $defaultIcons =  $this->path .'/images/all_icons.png'; 
                                          $echoIcons = $defaultIcons;
                                          if (isset($settings["options"]["iconsid"]) && $settings["options"]["iconsid"] != '') {
                                            $echoIcons = wp_get_attachment_image_src( $settings["options"]["iconsid"], 'full' ); 
                                            $echoIcons = $echoIcons[0];
                                          } 
                                        ?>
                                        <td><input id="gb-icontmplt" type="hidden" value="<?php if (isset($settings["options"]["iconsid"]) && $settings["options"]["iconsid"] != '') echo $settings["options"]["iconsid"]; ?>"  name="gb-icontmplt">
                                          <a href="#" class="button gbuploadIcon" style="margin-right: 5px;">Upload</a><a href="#" class="button gbremoveIcon" data-deficon="<?php echo $defaultIcons; ?>">Default</a></td>
                                    </tr>
                                    <tr><td colspan="2"><img class="gb-icon-template-preview" src="<?php echo $echoIcons; ?>"  style="width: 100%;" /></td></tr>  
                                    <tr><td colspan="2"><p class="howto">If you want to change the color or customize icons you can do that here. <a href="<?php echo $defaultIcons; ?>" target="_BLANK">First download this image</a>. Then modify it in any image editing software. Once you are done you can add your new icon template here by clicking on the "Upload" button above.</p></td></tr>                                                                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </form>



</div>

<div id="pg-deletebox" class="pg-aligncenter">
	<h3>Warning!</h3>
	<p>This action can't be undone. Are you sure that you want to delete this Gigabox Gallery?</p>
	<a href="<?php echo admin_url('admin.php?page=gigabox&deleteid='. $_GET['id']); ?>" class="button">Yes, delete.</a> <a class="button pg-dontdelete" href="#">No, don't delete.</a>
</div>
