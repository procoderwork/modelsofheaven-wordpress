<div class="wrap">

	<h2>Gigabox v<?php echo $this->version; ?>
			<a href="<?php echo admin_url( "admin.php?page=gigabox-edit" ); ?>" class="add-new-h2">Add New</a>
	</h2>
	<?php 
		global $wpdb;
		$prefix = $wpdb->base_prefix;
		$table_name = $wpdb->prefix . $this->table_name_suffix; 
		if (isset($_GET["deleteid"])) {
		
			$wpdb->delete(  $table_name, array( 'id' => $_GET["deleteid"] ) );
			$this->message(1);

		} 

		if (isset($_GET["recreate"])) {
			$args = array( 'post_type' => 'attachment', 'numberposts' => -1, 'post_parent' => null, 'post_mime_type' => 'image' );
			$attachments = get_posts( $args );
			if ($attachments) {
			    foreach ( $attachments as $post ) {
			        $file = get_attached_file( $post->ID );
			        wp_update_attachment_metadata( $post->ID, wp_generate_attachment_metadata( $post->ID, $file ) );
			    }
			}   
			$this->message(3);  
		}

		if (isset($_GET["cloneid"])) {
					$tabl_nm = $wpdb->base_prefix . $this->table_name_suffix;
					$clnid = $_GET["cloneid"];
					$results = $wpdb->get_row("SELECT * FROM $tabl_nm WHERE id = $clnid");
					$settings = unserialize($results->items);
					$settings["title"] = $settings["title"] . " CLONED";

					$serialized_sets = serialize($settings);
					$rows_affected = $wpdb->insert($tabl_nm , array('name' => $settings["title"], 'items' => $serialized_sets));
					//var_dump($settings);
		}
	?>
<div class="form_result" style="height: 35px;">&nbsp;</div>

<table class="wp-list-table widefat fixed">
	<thead>
		<tr>
			<th width="10%">ID</th>
			<th width="30%">Name</th>
			<th width="60%">Shortcode</th>
		</tr>
	</thead>
	
	<tbody>
		<?php 


			$gals = $wpdb->get_results("SELECT * FROM " . $table_name . " ORDER BY id");
			if (count($gals) == 0) {
				echo '<tr>'.
						 '<td colspan="100%">No Gigabox Galleries found.</td>'.
					 '</tr>';
			} else {
				$name;
				foreach ($gals as $gallery) {
					$name = $gallery->name;
					if(!$name) {
						$name = 'No Name #' . $gallery->id;
					}
					echo '<tr>'.
							'<td>' . $gallery->id . '</td>'.						
							'<td>' . '<strong><a class="row-title" href="' . admin_url('admin.php?page=gigabox-edit&id=' . $gallery->id) . '" title="Edit">'.$name.'</a></strong>' . '<div class="row-actions"><span class="edit"><a href="' . admin_url('admin.php?page=gigabox-edit&id=' . $gallery->id) . '">Edit</a> |</span> <span class="trash"><a class="delete-pgallery" href="#" data-id="'.$gallery->id.'">Delete</a> |</span> <span class="clonepg"><a class="clone-pgrid" href="'.admin_url('admin.php?page=gigabox&cloneid='. $gallery->id ) .'" data-id="'.$gallery->id.'">Clone</a></span></div></td>'.
							'<td> [gigabox id="' . $gallery->id . '"]</td>' .															
						'</tr>';
				}
			}
		?>
		
	</tbody>

	<tfoot>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Shortcode</th>				
		</tr>
	</tfoot>	

</table>
</div> 
<br/><br/>
<a href="<?php echo admin_url('admin.php?page=gigabox&recreate=1'); ?>" class="button button-primary">Regenerate Image Sizes</a>
<p>If you want to use images that you have uploaded BEFORE installing Gigabox Gallery, click the button above to create new sizes required for gigabox to function.<br/>
   Any image that you upload AFTER installation of Gigabox will be automatically resized and you don't have to do anything.<br/>
   WARNING: this operation can take long if you have lots of images, so if that's the case, it's better to just manually reupload only the images that you want to use in Gigabox. 
</p>
<p></p> 

<div id="pg-deletebox" class="pg-aligncenter">
	<h3>Warning!</h3>
	<p>This action can't be undone. Are you sure that you want to delete this Gigabox Gallery?</p>
	<a href="<?php echo admin_url('admin.php?page=gigabox&deleteid=0'); ?>" class="button pggodelete">Yes, delete.</a> <a class="button pg-dontdelete" href="#">No, don't delete.</a>
</div>