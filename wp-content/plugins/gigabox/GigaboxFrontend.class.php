<?php 
	class GigaboxFrontend
	{
		public $path, $includePath, $table_suffix;


		function __construct($pth, $incPth, $tbl)
		{
			$this->path = $pth;
			$this->includePath = $incPth;
			$this->table_suffix = $tbl;

			add_shortcode('gigabox', array($this, 'gigaboxShortcode'));
			add_shortcode('gigabox-gallery', array($this, 'gigaboxGalleryShortcodeLegacy'));
			add_shortcode('gigabox-image', array($this, 'gigaboxImageShortcodeLegacy'));
			add_action( 'wp_enqueue_scripts', array($this, 'enqueueFrontendStyle') );
		}


		function gigaboxShortcode($attr) {
			extract( shortcode_atts( array(
				'id' => ''
			), $attr ) );

			$settings = $this->getData($id);

			$itemsCount = count($settings["items"]);

			if ($itemsCount == 0) {
				return "Gigabox gallery with ID = ".$id." is empty! Please insert at least one image.";
			}
			elseif ($itemsCount == 1) {
				return $this->gigaboxImage($settings);
			}
			elseif ($itemsCount > 1) {
				return $this->gigaboxGallery($settings);
			}			
		}


		function gigaboxImage($settings) {
		    add_action('wp_footer', array($this,'enqueueFrontend'));
		    //var_dump($settings);

		    $imageData = $settings["items"][0];

		    //$image = wp_get_attachment_image_src($imageData['image_id'], 'full');
		    $fullimage = wp_get_attachment_image_src($imageData['image_id'], 'full');

		    $websitelink = $imageData['readmorehref'];

		    $hovDisp = 'zoom'; 
		    if (isset($settings["options"]["hovdisp"]))
		    	$hovDisp = $settings["options"]["hovdisp"];

		    if ($imageData['readmore']) 
		    	$websitelink = esc_url($imageData['readmorehref']);

		    if ($settings["options"]["iconsid"] == '') {
		    	$defIcons = $this->path .'/images/all_icons.png';
		    } else {
		    	$defIcons = wp_get_attachment_image_src( $settings["options"]["iconsid"], 'full' ); 
		    	$defIcons = $defIcons[0];
		    }

		    $output = '<div style="width: '.$settings["options"]["singlewidth"].'px; height: '.$settings["options"]["singleheight"].'px; background-image: url('.$fullimage[0].');" class="gbox-img-hover-wrap"  data-hovdisp="'.$hovDisp.'" data-socnets="'.$settings["options"]["socnet"].'" data-imgtempl="'.$defIcons.'" data-hovopac="'.$settings["options"]["hovopacity"].'" data-hovcolor="'.$settings["options"]["hovcolor"].'">'.
		    '<img src="" class="gbox-img-thumb" data-fullimg="' . $fullimage[0] . '" data-title="' . $imageData['title'] . '" data-desc="' .  esc_html(do_shortcode($imageData['description'])) . '" data-f-width="' . $fullimage[1] . '" data-f-height="' . $fullimage[2] . '"  data-rmtext="'.$imageData['readmore'].'" data-rmlink="' . $websitelink . '"  data-bgcolor="'.$settings["options"]["bgcolor"].'" data-bgopacity="'.$settings["options"]["bgopacity"].'" data-txtcolor="'.$settings["options"]["txtcolor"].'" />'.
		    '</div>';

		    return $output;
		}


		function gigaboxGallery($settings) {
			add_action('wp_footer', array($this,'enqueueFrontend'));
			//var_dump($settings);
			$hovDisp = 'zoom'; 
			$layDisp = 'sq';
			if (isset($settings["options"]["hovdisp"]))
				$hovDisp = $settings["options"]["hovdisp"];
			if (isset($settings["options"]["gblayoutdisp"]))
				$layDisp = $settings["options"]["gblayoutdisp"];

		    $output = '<div class="gbox-gallery-wrap" data-galid="" data-hovdisp="'.$hovDisp.'" data-laydisp="'.$layDisp.'">';
		    $i = 1;
		    $imgwidth = round(100 / $settings["options"]["columns"], PHP_ROUND_HALF_DOWN);
		    $marginatedClas='';
		    $mgWdth = 100;
		    //if ($settings["options"]["gblayoutdisp"] == 'msm' || $settings["options"]["gblayoutdisp"] == 'sqm') {
	    	if ($settings["options"]["gblayoutdisp"] == 'sqm') { 
	    		$imgwidth -=2;
	    		$marginatedClas = 'gbox-img-hover-wrmargin';
	    	}
	    	if ($settings["options"]["gblayoutdisp"] == 'msm') { 
	    		$mgWdth = 96;
	    		$marginatedClas = 'gbox-img-hover-wrmarginmsm';
	    	}
		    //}

		    //MASONRY LAYOUT
		    if ($settings["options"]["gblayoutdisp"] == 'ms' || $settings["options"]["gblayoutdisp"] == 'msm') {

		    	for ($j=0; $j<$settings["options"]["columns"]; $j++) {
		    		//key stuff here - column here
		    		$output .= '<div class="gboxMsonryColumn" style="width:' . $imgwidth . '%;">';

					for ($k=$j; $k < $settings["options"]["item_count"]; $k=$k+$settings["options"]["columns"]) {

						$imageItem = $settings["items"][$k];
				        $image = wp_get_attachment_image_src($imageItem["image_id"], 'gigabox-gal-mason');
				        $fullimage = wp_get_attachment_image_src($imageItem["image_id"], 'full');

				        if ($settings["options"]["iconsid"] == '') {
				        	$defIcons = $this->path .'/images/all_icons.png';
				        } else {
				        	$defIcons = wp_get_attachment_image_src( $settings["options"]["iconsid"], 'full' ); 
				        	$defIcons = $defIcons[0];
				        }

					    $websitelink = $imageItem['readmorehref'];

					    if ($imageItem['readmore']) 
					    	$websitelink = esc_url($imageItem['readmorehref']);


				        $output .= '<div class="gbox-img-hover-wrap gbox-img-hoverwrapgal '.$marginatedClas.'" style="width:'.$mgWdth.'%;" data-msnr="'.$k.'" data-indx="'.($i-1).'" data-socnets="'.$settings["options"]["socnet"].'" data-imgtempl="'.$defIcons.'" data-hovopac="'.$settings["options"]["hovopacity"].'" data-hovcolor="'.$settings["options"]["hovcolor"].'">'.
				        	'<img src="' . $image[0] . '"  class="gigabox-gallery-thumb gbox-img-thumb" data-fullimg="' . $fullimage[0] . '" data-f-width="' . $fullimage[1] . '" data-f-height="' . $fullimage[2] . '" data-title="' . $imageItem['title'] . '" data-desc="' . esc_html(do_shortcode($imageItem['description'])) . '"  data-rmtext="'.$imageItem['readmore'].'" data-rmlink="' . $websitelink . '"  data-bgcolor="'.$settings["options"]["bgcolor"].'" data-bgopacity="'.$settings["options"]["bgopacity"].'" data-txtcolor="'.$settings["options"]["txtcolor"].'"  /></div>';
				        if ($i % $settings["options"]["columns"]== 0)
				            $output .= '<div class="gbox-clear"></div>';
				        $i++;
				    }	

				    $output .="</div>";
				}

			}
			//SQUARE LAYOUT
			else {
			    foreach ($settings["items"] as $imageItem ) {
			        $image = wp_get_attachment_image_src($imageItem["image_id"], 'gigabox-gal-thumb');
			        $fullimage = wp_get_attachment_image_src($imageItem["image_id"], 'full');

			        if ($settings["options"]["iconsid"] == '') {
			        	$defIcons = $this->path .'/images/all_icons.png';
			        } else {
			        	$defIcons = wp_get_attachment_image_src( $settings["options"]["iconsid"], 'full' ); 
			        	$defIcons = $defIcons[0];
			        }

				    $websitelink = $imageItem['readmorehref'];

				    if ($imageItem['readmore']) 
				    	$websitelink = esc_url($imageItem['readmorehref']);


			        $output .= '<div class="gbox-img-hover-wrap gbox-img-hoverwrapgal '.$marginatedClas.'" style="width:' . $imgwidth . '%;" data-socnets="'.$settings["options"]["socnet"].'" data-imgtempl="'.$defIcons.'" data-hovopac="'.$settings["options"]["hovopacity"].'" data-hovcolor="'.$settings["options"]["hovcolor"].'">'.
			        	'<img src="' . $image[0] . '"  class="gigabox-gallery-thumb gbox-img-thumb" data-fullimg="' . $fullimage[0] . '" data-f-width="' . $fullimage[1] . '" data-f-height="' . $fullimage[2] . '" data-title="' . $imageItem['title'] . '" data-desc="' . esc_html(do_shortcode($imageItem['description'])) . '"  data-rmtext="'.$imageItem['readmore'].'" data-rmlink="' . $websitelink . '"  data-bgcolor="'.$settings["options"]["bgcolor"].'" data-bgopacity="'.$settings["options"]["bgopacity"].'" data-txtcolor="'.$settings["options"]["txtcolor"].'"  /></div>';
			        if ($i % $settings["options"]["columns"]== 0)
			            $output .= '<div class="gbox-clear"></div>';
			        $i++;
			    }				
			}

		    $output .= '</div><div class="gbox-clear"></div>';
		    //$output = preg_replace('/\s+/', ' ', trim($output));
		    return $output;
		}


		function gigaboxGalleryShortcodeLegacy($attr) {
		    $post = get_post();

		    add_action('wp_footer', array($this,'enqueueFrontendLegacy'));
		    add_action('wp_footer', array($this,'gboxheaderjsLegacy'), 100);

		    static $instance = 0;
		    $instance++;

		    if (!empty($attr['ids'])) {
		        // 'ids' is explicitly ordered, unless you specify otherwise.
		        if (empty($attr['orderby']))
		            $attr['orderby'] = 'post__in';
		        $attr['include'] = $attr['ids'];
		    }


		    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
		    if (isset($attr['orderby'])) {
		        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
		        if (!$attr['orderby'])
		            unset($attr['orderby']);
		    }

		    extract(shortcode_atts(array(
		        'order' => 'ASC',
		        'orderby' => 'menu_order ID',
		        'id' => $post->ID,
		        'itemtag' => 'dl',
		        'icontag' => 'dt',
		        'captiontag' => 'dd',
		        'columns' => 4,
		        'size' => 'thumbnail',
		        'include' => '',
		        'exclude' => ''
		                    ), $attr));

		    $id = intval($id);
		    if ('RAND' == $order)
		        $orderby = 'none';

		    if (!empty($include)) {
		        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

		        $attachments = array();
		        foreach ($_attachments as $key => $val) {
		            $attachments[$val->ID] = $_attachments[$key];
		        }
		    } elseif (!empty($exclude)) {
		        $attachments = get_children(array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
		    } else {
		        $attachments = get_children(array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));
		    }

		    if (empty($attachments))
		        return '';

		    $output = '';

		    if (is_feed()) {
		        $output = "\n";
		        foreach ($attachments as $att_id => $attachment)
		            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		        return $output;
		    }

		    //$galid = 1;

		    $output .= '<div class="gbox-gallery-wrap" data-galid="">';
		    $i = 1;
		    $imgwidth = round(100 / $columns, PHP_ROUND_HALF_DOWN);
		    foreach ($attachments as $id => $attachment) {
		        $image = wp_get_attachment_image_src($attachment->ID, 'gigabox-gal-thumb');
		        $fullimage = wp_get_attachment_image_src($attachment->ID, 'full');
		        //var_dump( $fullimage);
		        $websitelink = $attachment->post_excerpt;
		        if (preg_match("#https?://#", $websitelink) === 0 && $websitelink) {
		            $websitelink = 'http://' . $websitelink;
		        };
		        $dectValid = str_replace('"', '&quot;', $attachment->post_content);
		        //$dectValid = str_replace("'", '&quot;', $attachment->post_content);
		        $output .= '<div class="gbox-img-hover-wrap gbox-img-hoverwrapgal" style="width:' . $imgwidth . '%;"><img src="' . $image[0] . '"  class="gigabox-gallery-thumb gbox-img-thumb" data-fullimg="' . $fullimage[0] . '" data-f-width="' . $fullimage[1] . '" data-f-height="' . $fullimage[2] . '" data-title="' . $attachment->post_title . '" data-desc="' . $dectValid . '" data-rmlink="' . $websitelink . '"  /></div>';
		        if ($i % $columns == 0)
		            $output .= '<div class="gbox-clear"></div>';
		        $i++;
		    }
		    $output .= '</div><div class="gbox-clear"></div>';
		    $output = preg_replace('/\s+/', ' ', trim($output));
		    return $output;
		}


		function gigaboxImageShortcodeLegacy($attr) {
		    add_action('wp_footer', array($this,'enqueueFrontendLegacy'));
		    add_action('wp_footer', array($this,'gboxheaderjsLegacy'), 100);
		    
		    extract(shortcode_atts(array(
		        'id' => '',
		        'align' => 'alignnone',
		        'size' => 'medium'
		                    ), $attr));

		    if (!$id)
		        return; //in case there is no id

		    $image = wp_get_attachment_image_src($id, $size);
		    $fullimage = wp_get_attachment_image_src($id, 'full');

		    $gbAtt = $this->gb_get_attachment($id);
		    $websitelink = $gbAtt['caption'];
		    if (preg_match("#https?://#", $websitelink) === 0 && $websitelink) {
		        $websitelink = 'http://' . $websitelink;
		    };
		    $dectValid = str_replace('"', '&quot;', $gbAtt['description']);
		    $output = '<div class="gbox-img-hover-wrap ' . $align . '"><img src="' . $image[0] . '" class="gbox-img-thumb" data-fullimg="' . $fullimage[0] . '" data-title="' . $gbAtt['title'] . '" data-desc="' .  $dectValid . '" data-f-width="' . $fullimage[1] . '" data-f-height="' . $fullimage[2] . '" data-rmlink="' . $websitelink . '"   /></div>';
		    //$output = preg_replace('/\s+/', ' ', trim($output));
		    return $output;
		}


		function enqueueFrontend() {
		    wp_enqueue_script('jquery');

		    wp_enqueue_script(
		            'jquery-transit', plugins_url('/js/jquery.transit.min.js', __FILE__), array('jquery'), '2.0', true
		    );

		    wp_enqueue_script(
		            'gigabox', plugins_url('/js/gigabox.jquery.min.js', __FILE__), array('jquery'), '2.0', true
		    );

		}

		function enqueueFrontendStyle() {
		    wp_enqueue_style(
		            'gigabox', plugins_url('/css/gigabox.css', __FILE__)
		    );		
		}


		function gb_get_attachment($attachment_id) {

		    $attachment = get_post($attachment_id);
		    return array(
		        'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
		        'caption' => $attachment->post_excerpt,
		        'description' => $attachment->post_content,
		        'href' => get_permalink($attachment->ID),
		        'src' => $attachment->guid,
		        'title' => $attachment->post_title
		    );
		}

		function enqueueFrontendLegacy() {
		    wp_enqueue_script('jquery');

		    wp_enqueue_script(
		            'gigaboxLegacy', plugins_url('/js/gigabox.old.jquery.min.js', __FILE__), array('jquery'), '1.3.1', true
		    );

		    wp_enqueue_style(
		            'gigaboxLegacy', plugins_url('/css/gigaboxLegacy.css', __FILE__)
		    );
		}

		function gboxheaderjsLegacy() {
		    ?>
		    <script>
		        jQuery.gigabox('<?php echo get_option('gbox-readmore'); ?>', '<?php echo get_option('gbox-color'); ?>', '<?php echo get_option('gbox-openlink'); ?>', '<?php echo get_option('gbox-socmed'); ?>');
		    </script>
		    <?php
		}


		function getData($id) {

	        global $wpdb;
	        $table_name = $wpdb->prefix . $this->table_suffix; 	

		    $results = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $id");

		    if (!$results)
		    	return false;

		    $sets = unserialize($results->items);

		    return $sets;

		}		

	}
?>