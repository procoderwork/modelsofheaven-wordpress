��          �      �       H     I     _  4   w     �  
   �     �     �     �          (  :   7     r  ;   �     �  "   �     �  ,        5     E     [  	   y  !   �     �     �     �     �  ;   �     
                            	                                Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ PO-Revision-Date: 2015-09-10 21:36:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1100
Project-Id-Version: Stable (latest release)
 Kunne ikke skifte til egen bruger. Kunne ikke skifte bruger. Skift hurtigt mellem brugerkonti i WordPress John Blackbourn Skift til egen bruger Skift tilbage til %1$s (%2$s) Skift til Skiftede tilbage til %1$s (%2$s). Skiftede til %1$s (%2$s). Brugerskift Brugerskift https://johnblackbourn.com/ https://johnblackbourn.com/wordpress-plugin-user-switching/ 