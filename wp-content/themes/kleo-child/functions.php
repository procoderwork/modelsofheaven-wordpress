<?php
/**
 * @package WordPress
 * @subpackage Kleo
 * @author SeventhQueen <themesupport@seventhqueen.com>
 * @since Kleo 1.0
 */

/**
 * Kleo Child Theme Functions
 * Add custom code below
*/ 
if(!function_exists('pr')){
    function pr($arr, $die = 0){
        echo "<pre>";
            print_r($arr);
        echo ($die)?die():"</pre>";
    }
}

ini_set('max_execution_time', 1000);

add_action( 'after_setup_theme', 'kleo_fixer', 13 );

function kleo_fixer(){
	// remove_action( 'pre_set_site_transient_update_themes', array( 'Kleo', 'themeforest_themes_update' ) );
}


/**
 * Proper way to enqueue scripts and styles.
 */
function moh_scripts() {
    // wp_enqueue_style( 'style-name', get_stylesheet_uri() );
    wp_enqueue_script( 'moh', get_stylesheet_directory_uri() . '/js/moh.js', array(), '', true );
}
add_action( 'wp_enqueue_scripts', 'moh_scripts', 33 );



/***********************************************************************************************/
/***********************************************************************************************/

add_action( 'init', 'redirect_to_custom_register', 20 );

function redirect_to_custom_register(){
	
	if( strtolower( basename( $_SERVER['SCRIPT_FILENAME'] ) ) === 'wp-signup.php'){
		header("location: /models-of-heaven-register/");
		die();
		// wp_die();
	}
}


add_action( 'wp_head', 'custom_login_page_security', 20 );

function custom_login_page_security(){
	global $post;
	$blog_id = get_current_blog_id();

	$post_slug=$post->post_name;
	if( $blog_id == 1 && $post_slug == 'models-of-heaven-register' && !isset($_GET['94e510ecc1b1d7a405c0e7aa18db792b']) ){
		// if(  ){
			header("location: /");
			echo '<script>window.location="/";</script>';
			die();
		// }
	}
}



$blog_id = get_current_blog_id();


if( $blog_id == 1){

	add_action( 'signup_extra_fields' , 'custom_signup_extra_fields' , 10 , 2 );
	 
	function custom_signup_extra_fields( $errors ) {
		
		echo '<input type="hidden" name="94e510ecc1b1d7a405c0e7aa18db792b" value="1" />';
		
	}

}

add_action( 'wpmu_validate_blog_signup' , 'custom_wpmu_validate_blog_signup' );

function custom_wpmu_validate_blog_signup( $result ) {

	$hashKey = '';

	if( !empty( $_POST['94e510ecc1b1d7a405c0e7aa18db792b'] ) ) {

		$hashKey = sanitize_text_field( $_POST['94e510ecc1b1d7a405c0e7aa18db792b'] );

	}

	if ( empty( $hashKey ) ) {

		$result['errors']->add( 'invalid_access' , __( 'Invalid Submmission!' ) );

	}

}


/*
$debug_tags = array();
add_action( 'all', function ( $tag ) {
    global $debug_tags;
    if ( in_array( $tag, $debug_tags ) ) {
        return;
    }
    echo "<pre>" . $tag . "</pre>";
    $debug_tags[] = $tag;
} );
*/



// buddypress Fix for SignUp Issue

/*First remove the buddypress default Hook*/
remove_action( 'bp_screens', 'bp_core_screen_signup' );

/*Now Hook the new custom function*/
add_action( 'bp_screens', 'dm_core_screen_signup' );

function dm_core_screen_signup() {
	$bp = buddypress();

	if ( ! bp_is_current_component( 'register' ) || bp_current_action() )
		return;

	// Not a directory.
	bp_update_is_directory( false, 'register' );

	// If the user is logged in, redirect away from here.
	if ( is_user_logged_in() ) {

		$redirect_to = bp_is_component_front_page( 'register' )
			? bp_get_members_directory_permalink()
			: bp_get_root_domain();

		/**
		 * Filters the URL to redirect logged in users to when visiting registration page.
		 *
		 * @since 1.5.1
		 *
		 * @param string $redirect_to URL to redirect user to.
		 */
		bp_core_redirect( apply_filters( 'bp_loggedin_register_page_redirect_to', $redirect_to ) );

		return;
	}

	$bp->signup->step = 'request-details';

	if ( !bp_get_signup_allowed() ) {
		$bp->signup->step = 'registration-disabled';

		// If the signup page is submitted, validate and save.
	} elseif ( isset( $_POST['signup_submit'] ) && bp_verify_nonce_request( 'bp_new_signup' ) ) {

		/**
		 * Fires before the validation of a new signup.
		 *
		 * @since 2.0.0
		 */
		do_action( 'bp_signup_pre_validate' );

		// Check the base account details for problems.
		$account_details = bp_core_validate_user_signup( $_POST['signup_username'], $_POST['signup_email'] );

		// If there are errors with account details, set them for display.
		if ( !empty( $account_details['errors']->errors['user_name'] ) )
			$bp->signup->errors['signup_username'] = $account_details['errors']->errors['user_name'][0];

		if ( !empty( $account_details['errors']->errors['user_email'] ) )
			$bp->signup->errors['signup_email'] = $account_details['errors']->errors['user_email'][0];

		// Check that both password fields are filled in.
		if ( empty( $_POST['signup_password'] ) || empty( $_POST['signup_password_confirm'] ) )
			$bp->signup->errors['signup_password'] = __( 'Please make sure you enter your password twice', 'buddypress' );

		// Check that the passwords match.
		if ( ( !empty( $_POST['signup_password'] ) && !empty( $_POST['signup_password_confirm'] ) ) && $_POST['signup_password'] != $_POST['signup_password_confirm'] )
			$bp->signup->errors['signup_password'] = __( 'The passwords you entered do not match.', 'buddypress' );

		$bp->signup->username = $_POST['signup_username'];
		$bp->signup->email = $_POST['signup_email'];

		// Now we've checked account details, we can check profile information.
		if ( bp_is_active( 'xprofile' ) ) {

			// Make sure hidden field is passed and populated.
			if ( isset( $_POST['signup_profile_field_ids'] ) && !empty( $_POST['signup_profile_field_ids'] ) ) {

				// Let's compact any profile field info into an array.
				$profile_field_ids = explode( ',', $_POST['signup_profile_field_ids'] );

				// Loop through the posted fields formatting any datebox values then validate the field.
				foreach ( (array) $profile_field_ids as $field_id ) {
					bp_xprofile_maybe_format_datebox_post_data( $field_id );

					// Create errors for required fields without values.
					if ( xprofile_check_is_required_field( $field_id ) && empty( $_POST[ 'field_' . $field_id ] ) && ! bp_current_user_can( 'bp_moderate' ) )
						$bp->signup->errors['field_' . $field_id] = __( 'This is a required field', 'buddypress' );
				}

				// This situation doesn't naturally occur so bounce to website root.
			} else {
				bp_core_redirect( bp_get_root_domain() );
			}
		}

		// Finally, let's check the blog details, if the user wants a blog and blog creation is enabled.
		if ( isset( $_POST['signup_with_blog'] ) ) {
			$active_signup = bp_core_get_root_option( 'registration' );

			if ( 'blog' == $active_signup || 'all' == $active_signup ) {
				$blog_details = bp_core_validate_blog_signup( $_POST['signup_blog_url'], $_POST['signup_blog_title'] );

				// If there are errors with blog details, set them for display.
				if ( !empty( $blog_details['errors']->errors['blogname'] ) )
					$bp->signup->errors['signup_blog_url'] = $blog_details['errors']->errors['blogname'][0];

				if ( !empty( $blog_details['errors']->errors['blog_title'] ) )
					$bp->signup->errors['signup_blog_title'] = $blog_details['errors']->errors['blog_title'][0];
			}
		}

		/**
		 * Fires after the validation of a new signup.
		 *
		 * @since 1.1.0
		 */
		do_action( 'bp_signup_validate' );

		// Add any errors to the action for the field in the template for display.
		if ( !empty( $bp->signup->errors ) ) {
			foreach ( (array) $bp->signup->errors as $fieldname => $error_message ) {
				/*
				 * The addslashes() and stripslashes() used to avoid create_function()
				 * syntax errors when the $error_message contains quotes.
				 */

				/**
				 * Filters the error message in the loop.
				 *
				 * @since 1.5.0
				 *
				 * @param string $value Error message wrapped in html.
				 */
				add_action( 'bp_' . $fieldname . '_errors', create_function( '', 'echo apply_filters(\'bp_members_signup_error_message\', "<div class=\"error\">" . stripslashes( \'' . addslashes( $error_message ) . '\' ) . "</div>" );' ) );
			}
		} else {
			$bp->signup->step = 'save-details';

			// No errors! Let's register those deets.
			$active_signup = bp_core_get_root_option( 'registration' );

			if ( 'none' != $active_signup ) {

				// Make sure the extended profiles module is enabled.
				if ( bp_is_active( 'xprofile' ) ) {
					// Let's compact any profile field info into usermeta.
					$profile_field_ids = explode( ',', $_POST['signup_profile_field_ids'] );

					/*
					 * Loop through the posted fields, formatting any
					 * datebox values, then add to usermeta.
					 */
					foreach ( (array) $profile_field_ids as $field_id ) {

						bp_xprofile_maybe_format_datebox_post_data( $field_id );

						if ( !empty( $_POST['field_' . $field_id] ) )
							$usermeta['field_' . $field_id] = $_POST['field_' . $field_id];

						if ( !empty( $_POST['field_' . $field_id . '_visibility'] ) )
							$usermeta['field_' . $field_id . '_visibility'] = $_POST['field_' . $field_id . '_visibility'];
						
					}

					// Store the profile field ID's in usermeta.
					$usermeta['profile_field_ids'] = $_POST['signup_profile_field_ids'];
				}

				// Hash and store the password.
				$usermeta['password'] = wp_hash_password( $_POST['signup_password'] );

				// If the user decided to create a blog, save those details to usermeta.
				if ( 'blog' == $active_signup || 'all' == $active_signup )
					$usermeta['public'] = ( isset( $_POST['signup_blog_privacy'] ) && 'public' == $_POST['signup_blog_privacy'] ) ? true : false;

				/**
				 * Filters the user meta used for signup.
				 *
				 * @since 1.1.0
				 *
				 * @param array $usermeta Array of user meta to add to signup.
				 */
				$usermeta = apply_filters( 'bp_signup_usermeta', $usermeta );

				$signup_blog_domain = str_replace('/', '', $_POST['signup_blog_url'].'.'.bp_signup_get_subdomain_base());
				
				// pr($signup_blog_domain, 1);

				// Finally, sign up the user and/or blog.
				if ( isset( $_POST['signup_with_blog'] ) && is_multisite() )
					$wp_user_id = bp_core_signup_blog( $signup_blog_domain, '/', $_POST['signup_blog_title'], $_POST['signup_username'], $_POST['signup_email'], $usermeta );
				else
					$wp_user_id = bp_core_signup_user( $_POST['signup_username'], $_POST['signup_password'], $_POST['signup_email'], $usermeta );


				/*pr('_POST');
				pr($_POST);
				pr('usermeta');
				pr($usermeta);
				pr('wp_user_id');
				pr($wp_user_id, 1);*/

				if ( is_wp_error( $wp_user_id ) ) {
					$bp->signup->step = 'request-details';
					bp_core_add_message( $wp_user_id->get_error_message(), 'error' );
				} else {
					$bp->signup->step = 'completed-confirmation';
				}
			}

			/**
			 * Fires after the completion of a new signup.
			 *
			 * @since 1.1.0
			 */
			do_action( 'bp_complete_signup' );
		}

	}

	/**
	 * Fires right before the loading of the Member registration screen template file.
	 *
	 * @since 1.5.0
	 */
	do_action( 'bp_core_screen_signup' );

	/**
	 * Filters the template to load for the Member registration page screen.
	 *
	 * @since 1.5.0
	 *
	 * @param string $value Path to the Member registration template to load.
	 */
	bp_core_load_template( apply_filters( 'bp_core_template_register', array( 'register', 'registration/register' ) ) );
}