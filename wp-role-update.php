<?php
/**
 * Loads the WordPress environment and template.
 *
 * @package WordPress
 */

 
//pretty array printer
if ( !function_exists('pr') ){
    function pr( $arr , $dye = 0){
        $root = debug_backtrace();
        $file = str_replace($_SERVER['DOCUMENT_ROOT'],'',$root[0]['file']);
        $string = '</br>Line: '.$root[0]['line'].'</br>File : '.$file;

        echo "<pre>";        
        print_r($arr);        
        echo "</pre>";    

        if($dye == 1){            
            die($string);
        }
    }
}



$wp_did_header = true;

// Load the WordPress library.
require_once( dirname(__FILE__) . '/wp-load.php' );

// get all subscribers
$args = array( 
    // 'role__in' => array('author'),
    'role__in' => array('author'),
    'role__not_in' => array('bbp_keymaster','administrator','bbp_participant'),
    'number' => 1000
 );

$users = get_users( $args );
 pr($users,1);
//  pr(count($users),1);
foreach($users as $key=> $value){
       
    // pr($value);
     $value->remove_role( 'author' );
     $value->add_role( 'bbp_spectator' );     
}
